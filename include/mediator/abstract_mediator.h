#ifndef ABSTRACT_MEDIATOR_
#define ABSTRACT_MEDIATOR_

#include <ros/ros.h>
#include <ros/package.h>
#include <yaml-cpp/yaml.h>
#include <octomap/octomap.h>
#include <pcl/point_cloud.h>
#include <pcl/octree/octree.h>
#include <filesystem>

#include <fstream>

#include "robot/decorators/abstract_robot_decorator.h"
#include "robot/abstract_robot.h"
#include "robot/ceti_robot.h"
#include "robot_element/abstract_robot_element.h"
#include "reader/wing_reader.h"
#include "reader/robot_reader.h"
#include "reader/ts_reader.h"
#include "reader/cuboid_reader.h"



//! Blueprint of panel data
struct wing_BP {
    std::string name_;
    tf2::Transform pos_;
    tf2::Vector3 size_;
};

//! Protobuf entry
struct protobuf_entry{
    std::string name_; 
    tf2::Transform tf_; 
    int wing_config_;
};

//!  AbstractMediator
/*!
  Abstraction of a Mediator which registers Robots.
*/
class AbstractMediator {
    protected:
    std::shared_ptr<ros::NodeHandle> nh_; //!< Nodehandle for access to the Rosparam server
    std::unique_ptr<TSReader> task_space_reader_; //!< Task_space reader which provides drop off positions
    std::map<std::string, std::unique_ptr<AbstractRobotDecorator>> robots_; //!< Robots agents
    std::vector<std::vector<std::vector<tf2::Transform>>> relative_bounds_; //!< total bound a  workspace

    std::map<const std::string, std::vector<pcl::PointXYZ>> result_vector_; //!< Result_vector of base positions linked to robot
    std::map<const std::string, std::vector<std::unique_ptr<AbstractRobotElement>>> wings_;
    std::string dirname_; //!< Dirname of the reference protobuff

    std::unique_ptr<WingReader> wing_reader_; //!< Wing_reader which collects panel information of robots
    std::unique_ptr<RobotReader> robot_reader_; //!< Robot_reader which collects robot poses 
    std::unique_ptr<CuboidReader> cuboid_reader_; //!< coboidReader instance that distinguishes between scene objects of type bin and box


    public:
    //! AbstractMediator constructor
    /*!
        initializes task_space reader
        \param d Ros nodehandle
    */
    AbstractMediator(std::shared_ptr<ros::NodeHandle> const& d);

    //! Set result vector
    /*!
        \param res result vector
    */
    inline void setResultVector(std::map<const std::string, std::vector<pcl::PointXYZ>>& res) {result_vector_ = res;}

    //! Get wings
    /*!
        \return map of panals linked to their robots
    */
    inline std::map<const std::string, std::vector<std::unique_ptr<AbstractRobotElement>>>& wings() {return wings_;}

    //! Get result_vector
    /*!
        \return result_vector
    */
    inline std::map<const std::string, std::vector<pcl::PointXYZ>>& resultVector() {return result_vector_;}

    //! Get robots
    /*!
        \return robots as decorators
    */
    inline std::map<std::string, std::unique_ptr<AbstractRobotDecorator>>& robots(){return robots_;}

    //! Set dirname
    /*!
        \param dirn new dirname 
    */
    inline void setDirname(std::string& dirn) {dirname_ = dirn;}

    //! Get dirname
    /*!
        \return dirname
    */
    inline std::string& dirname() {return dirname_;}


    //! Cloud converter
    /*!
        Converts vector of pcl::PointXYZ to a pcl::Pointcloud
        \param vector Vector of XYZPoints
        \return Pointcloud
    */
    pcl::PointCloud< pcl::PointXYZ >::Ptr vector2cloud(std::vector<pcl::PointXYZ>& vector);
    
    //! pure virtual robot connecting methode
    /*!
        \param robot robot decorator
    */
    virtual void connectRobots(std::unique_ptr<AbstractRobotDecorator> robot)=0; 

    //! pure virtual mediate methode
    virtual void mediate()=0;

    //! pure virtual Sets panels for robots
    virtual void setPanel()=0;

    //!  Get Wing_reader
    /*!
            \return wing reader
    */
    inline WingReader* wingReader() {return wing_reader_.get();}

    //!  Get Robot_reader
    /*!
            \return robot reader
    */
    inline RobotReader* robotReader() {return robot_reader_.get();}

    //!  Get Cuboid_reader
    /*!
        \return cuboid reader
    */
    inline CuboidReader* cuboidReader() {return cuboid_reader_.get();}


};


#endif