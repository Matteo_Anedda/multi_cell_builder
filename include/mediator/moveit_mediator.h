#ifndef MOVEIT_MEDIATOR_
#define MOVEIT_MEDIATOR_

#include <ros/ros.h>
#include <moveit/planning_scene_interface/planning_scene_interface.h>
#include <moveit/move_group_interface/move_group_interface.h>
#include <moveit_msgs/ApplyPlanningScene.h>
#include <moveit/planning_scene/planning_scene.h>
#include <moveit/robot_model_loader/robot_model_loader.h>
#include <moveit/kinematic_constraints/utils.h>
#include <moveit_visual_tools/moveit_visual_tools.h>
#include <moveit/task_constructor/task.h>
#include <moveit/task_constructor/stages/compute_ik.h>
#include <moveit/task_constructor/stages/connect.h>
#include <moveit/task_constructor/stages/current_state.h>
#include <moveit/task_constructor/stages/generate_grasp_pose.h>
#include <moveit/task_constructor/stages/generate_place_pose.h>
#include <moveit/task_constructor/stages/generate_pose.h>
#include <moveit/task_constructor/stages/modify_planning_scene.h>
#include <moveit/task_constructor/stages/move_relative.h>
#include <moveit/task_constructor/stages/move_to.h>
#include <moveit/task_constructor/stages/predicate_filter.h>
#include <moveit/task_constructor/solvers/cartesian_path.h>
#include <moveit/task_constructor/solvers/pipeline_planner.h>
#include <moveit_task_constructor_msgs/ExecuteTaskSolutionAction.h>
#include <moveit/task_constructor/properties.h>
#include <moveit/task_constructor/solvers/joint_interpolation.h>
#include <moveit/task_constructor/solvers/planner_interface.h>
#include <moveit/task_constructor/stage.h>
#include <moveit/task_constructor/stages/fixed_state.h>
#include <eigen_conversions/eigen_msg.h>
#include <moveit/trajectory_execution_manager/trajectory_execution_manager.h>
#include <moveit/planning_scene_monitor/current_state_monitor.h>
#include <moveit/planning_scene_monitor/planning_scene_monitor.h>
#include <moveit/moveit_cpp/moveit_cpp.h>
#include <behaviortree_cpp_v3/behavior_tree.h>
#include <behaviortree_cpp_v3/tree_node.h>
#include <behaviortree_cpp_v3/bt_factory.h>
#include <behaviortree_cpp_v3/loggers/bt_zmq_publisher.h>
#include <stdint.h>


#include "robot_element/observers/moveit_panel.h"
#include "robot_element/decorators/log_decorator.h"
#include "robot/decorators/abstract_robot_decorator.h"
#include "reader/job_reader.h"
#include "reader/cuboid_reader.h"
#include "reader/wing_reader.h"
#include "bt/execution.h"
#include "bt/position_condition.h"
#include "bt/parallel_robot.h"


//!  Refined MoveitMediator 
/*!
    Build Multi-cells from specifications and apply MoveIt library, as well as Groot library functionallity to display the state in a task.
*/
class MoveitMediator : public AbstractMediator{
    protected:
    std::shared_ptr<moveit::core::RobotModel> robot_model_; //!< Moveit Robot-Model as specified in SDF
    std::shared_ptr<moveit::planning_interface::MoveGroupInterface> mgi_; //!< Move Group Interface of the whole multi-cell
    std::unique_ptr<moveit::planning_interface::PlanningSceneInterface> psi_; //!< PlanningSceneInteface to manage Scene Objects
    std::shared_ptr<planning_scene::PlanningScene> ps_; //!< Shared Planning Scene
    std::shared_ptr<ros::Publisher> planning_scene_diff_publisher_; //!< Publisher to manage PlanningScene diffs
    std::shared_ptr<moveit::task_constructor::solvers::PipelinePlanner> sampling_planner_; //!< Moveit task Constructior simple planner
    std::shared_ptr<moveit::task_constructor::solvers::CartesianPath> cartesian_planner_; //!< Moveit task Constructior cartesian planner
    std::map<std::string, std::vector<moveit::task_constructor::Task>> task_map_; //!< Tasks mapped to Robot
    std::unique_ptr<JobReader> job_reader_; //!< jobReader instancde which reads task information
    std::map<std::string, std::vector<uint8_t>> acm_; //!< shared allowed collision matrix between robots
    std::map<std::string, std::vector<uint8_t>> rs_; //!< shared robot state between all robots
    std::multimap<std::string, std::pair<tf2::Vector3, std::queue<moveit_task_constructor_msgs::ExecuteTaskSolutionGoal>>> tasks_; //!< tasks multimap
    std::map<std::string, std::pair<moveit_msgs::RobotTrajectory, moveit_msgs::PlanningScene>> executions_; //!< Shared execution map containing groot node information




    public:
    //! AbstractMediator constructor
    /*!
        initializes task_space reader
        \param nh Ros nodehandle
    */
    MoveitMediator(std::shared_ptr<ros::NodeHandle> const& nh);

    inline std::shared_ptr<moveit::planning_interface::MoveGroupInterface> mgi() {return mgi_;};

    //! mediator function
    /*!
    Construction of robots and planning scenes. Calculate plans in preor to chain them so that they can be executed in parallel.
    */
    void mediate() override;

    //! connect robot and initialize Moveit components
    /*!
    Set up acm_ and rs_ members to track, merge, and publish changes during execution.
    \param robot RobotDecorator of Robot
    */
    void connectRobots(std::unique_ptr<AbstractRobotDecorator> robot) override; 


    //! Merge Planning scene 
    /*!
    \param out merged Planning Scene
    \param in planning scene to merge
    \param mr Robot to determine parts to merge in out planning scene
    */
    void mergePS(moveit_msgs::PlanningScene& out, moveit_msgs::PlanningScene in, AbstractRobotDecorator* mr);

    //! Threaded function which calls executaion on a Robot
    /*!
    \param mr RobotDecorator of Robot
    \param rt Robot trajetory
    */
    void parallelExec(AbstractRobotDecorator & mr, moveit_msgs::RobotTrajectory rt);

    //! Threaded function which calls executaion on a Robot
    /*!
    \param in merges acm into shared acm
    */
    void mergeACM(moveit_msgs::PlanningScene& in);

    //! Threaded function which calls executaion on a Robot
    /*!
    \param in merges acm into shared acm
    */
    void taskPlanner();

    //! publish all panels in the planning scene
    void publishTables();

    //! Manipulate ACM by extracting inforamtion of spicified robot
    /*!
    \param mr Robot
    \param ps Planning Scene which contains ACM information
    */
    void manipulateACM(AbstractRobotDecorator* mr, moveit_msgs::PlanningScene& ps);

    //! Create Task with Moveit Task Constructor
    /*!
    \param r robot
    \param source initaial position of object
    \param target pose of object after execution 
    */
    moveit::task_constructor::Task createTask(AbstractRobotDecorator* r, moveit_msgs::CollisionObject& source, tf2::Transform& target);
    inline std::map<std::string, std::pair<moveit_msgs::RobotTrajectory, moveit_msgs::PlanningScene>>& executions(){return executions_;};
    inline std::map<std::string, std::vector<moveit::task_constructor::Task>>& taskMap(){return task_map_;};

    inline void setTasks(std::multimap<std::string, std::pair<tf2::Vector3, std::queue<moveit_task_constructor_msgs::ExecuteTaskSolutionGoal>>>& tasks) {tasks_ = tasks;};


    //!  Sets panels for robots
    void setPanel() override;

};

#endif