#ifndef GRASP_MEDIATOR_
#define GRASP_MEDIATOR_

#include "abstract_mediator.h"
#include "moveit_mediator.h"
#include "gb_grasp/MapConfigLoader.h"
#include "gb_grasp/GraspMap.h"
#include "gb_grasp/Cuboid.h"
#include "gb_grasp/MapGenerator.h"
#include "gb_grasp/VoxelManager.h"
#include "gb_grasp/GraspPipelineDemo.h"



class GraspMediator : public AbstractMediator{
    protected:
    std::shared_ptr<moveit_visual_tools::MoveItVisualTools> visual_tools_; //!< MoveItVisualTools
    std::shared_ptr<planning_scene_monitor::PlanningSceneMonitor> planning_scene_monitor_; //!< Planningscene monitor
    std::unique_ptr<moveit_grasps_demo::GraspPipelineDemo> grasp_pipeline_demo_; //!< Auerswald grasp pipeline
    std::string referenceRobot_; //!< Reference Robot
    std::string resultPath_;//!< path to the result
    std::map<const std::string, Cuboid> relevant_boxes_;


    public:
    GraspMediator(std::shared_ptr<ros::NodeHandle> const& nh);
    void rewriteResult();
    void rewriteJob();

    void mediate() override;
    void connectRobots(std::unique_ptr<AbstractRobotDecorator> robot) override;
    void setPanel() override;

};

#endif