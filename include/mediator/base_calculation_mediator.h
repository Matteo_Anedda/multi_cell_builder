#ifndef BASE_CALCULATION_MEDIATOR_
#define BASE_CALCULATION_MEDIATOR_

#include <ros/ros.h>
#include <ros/package.h>
#include <yaml-cpp/yaml.h>
#include <fstream>
#include <filesystem>


#include "mediator/abstract_mediator.h"
#include "robot/abstract_robot.h"
#include "robot/ceti_robot.h"
#include "robot_element/observers/rviz_panel.h"
#include "robot_element/decorators/log_decorator.h"
#include "reader/wing_reader.h"
#include "reader/robot_reader.h"


struct protocol {
    std::vector<std::tuple<std::string, tf2::Transform, std::bitset<3>>> robots;
};

//!  Concrete Mediator
/*!
    Base calculation mediator, which mediates agents to form a multi-cell by placing/rotating and perform collision checks
*/
class BaseCalculationMediator : public AbstractMediator{
    protected:
    std::unique_ptr<ros::Publisher> pub_; //!< Publisher sharing visualization messages of the scene
    std::map<const std::string, std::vector<pcl::PointXYZ>> grounds_; //!< Possible ground positions per robots
    std::vector<std::vector<std::unique_ptr<AbstractRobotElement>>> wings_; //!< Possible panels per robot
    std::vector<std::vector<std::vector<protobuf_entry>>> protobuf_; //!< Datastructure as product of all baseposition -> robot combinations
    std::string filename_; 
    std::map<std::string, std::vector<protocol>> protocol_map_;


    public:
    //!  BaseCalculationMediator constructor
    /*!
        initializes readers und publisher
            \param d Ros nodehandle
    */
    BaseCalculationMediator(std::shared_ptr<ros::NodeHandle> const& d);

    //!  Ground generator
    /*!
        initialize descrete ground 
            \param origin Ros nodehandle
            \param diameter Ros nodehandle
            \param resolution Ros nodehandle

    */
    void generateGrounds(const tf2::Vector3 origin, const float diameter, float resolution);

    //!  Approximates other robots to fit in the workspace
    /*!
        Places and rotates robot to fit in a previous robots surrounding
            \param robot next robot to fit in
    */
    void approximation(std::map<const std::string, std::vector<tf2::Transform>>& workcell, std::vector<protobuf_entry>& wc_solution);

    //!  Writes result file 
    /*!
        Writes Protobuff file containing all robots, panels, Drop off locations and actual box positions
    */
    void writeFile(std::vector<std::vector<protocol>>& combinations);

    //!  Marker publishing methode
    /*!
        Publishes all markers of an robot
            \param r Robot
    */
    void publish(CetiRobot* r);

    //!  Rviz setup methode
    void setupRviz();

    //!  Workspace calculator
    /*!
        calculates for given robots and corresponding base position, all possible combinations.
            \param workcell map of robots with corresponding base position
    */
    void calculate(std::map<const std::string, std::vector<tf2::Transform>>& workcell);
    
    void setPanel() override;

    //!  check_collision
    /*!
        Checks task_space of robot for collision. Also provides information about workload.
            \param robot[IN] std::string providing robot name       
            \param panel_mask[IN] bitmap providing which robot_elements to check 
            \param[OUT] workload desribing if robot/all robots_elements were used 
            \return true if task_space collides with robot/robot_elements

    */
    bool checkCollision(const std::string& robot, std::bitset<3>& panel_mask, bool& workload, std::vector<object_data>& ts);

    bool checkWorkcellCollision(const std::vector<std::string>& robot, std::vector<std::bitset<3>>& panel_mask, bool& workload);

    //!  Mediator implementation
    /*!
        Places and rotates robots till no collisions exists, all objects are somewhere on the tables
    */
    void mediate() override;

    //!  Robot Connector implementation 
    /*!
        registers robot
            \param robot 
    */
    void connectRobots(std::unique_ptr<AbstractRobotDecorator> robot) override;

    bool sceneCollision(std::vector<protobuf_entry>& wc_solution);

    void new_approximate(std::map<const std::string, std::vector<tf2::Transform>>& workcell, std::map<const std::string, std::vector<tf2::Transform>>::iterator& it, std::vector<std::string>& state, std::vector<std::bitset<3>>& panel_state);


    static void generateCombinations(std::map<std::string, std::vector<protocol>>& solutionMap,
                          std::vector<std::vector<protocol>>& combinations,
                          std::vector<protocol>& currentCombination,
                          const std::vector<std::string>& keys,
                          int currentIndex) {
        if (currentIndex == keys.size()) {
            // Base case: reached the end of keys, add current combination to combinations
            combinations.push_back(currentCombination);
            return;
        }

        const auto& key = keys[currentIndex];
        const auto& protocols = solutionMap[key];

        for (const auto& protocol : protocols) {
            // Add current protocol to current combination
            currentCombination.push_back(protocol);
            // Recursively generate combinations for the remaining keys
            generateCombinations(solutionMap, combinations, currentCombination, keys, currentIndex + 1);
            // Remove last added protocol to backtrack and explore other combinations
            currentCombination.pop_back();
        }
    }

static bool hasRobot(const protocol& proto, const std::string& robotName) {
    for (const auto& robot : proto.robots) {
        if (std::get<0>(robot) == robotName) {
            return true;
        }
    }
    return false;
}

static std::vector<std::vector<std::string>> compareMapEntries(std::map<const std::string, std::vector<object_data>>& inputMap) {
       std::vector<std::vector<std::string>> outputVec;

    std::map<std::string, bool> matchedMap;  // Map to keep track of matched keys

    // Loop through each map entry
    for (const auto& entry1 : inputMap) {
        // Skip if the key has already been matched
        if (matchedMap[entry1.first]) continue;

        std::vector<std::string> keysVec;
        keysVec.push_back(entry1.first);  // Add the current key to the vector
        matchedMap[entry1.first] = true;  // Mark the key as matched

        // Loop through remaining map entries
        for (const auto& entry2 : inputMap) {
            // Skip comparing the same map entry or if the key has already been matched
            if (entry1.first == entry2.first || matchedMap[entry2.first]) continue;

            bool matched = false;
            // Loop through each transform in entry1's value
            for (const auto& tf1 : entry1.second) {
                // Loop through each transform in entry2's value
                for (const auto& tf2 : entry2.second) {
                    // Compare the transform values
                    if (tf1.pose_ == tf2.pose_) {
                        matched = true;
                        break;
                    }
                }
                if (matched) break;
            }
            if (matched) {
                keysVec.push_back(entry2.first);  // Add the matching key to the vector
                matchedMap[entry2.first] = true;  // Mark the key as matched
            }
        }
        outputVec.push_back(keysVec);  // Add the vector of keys to the output vector
    }
    return outputVec;
}

};

#endif