#ifndef RVIZ_PANEL_
#define RVIZ_PANEL_

#include "ros/ros.h"
#include "robot_element/observers/panel.h"
#include "visualization_msgs/MarkerArray.h"



class RvizPanel  : public Panel{
    protected:
    visualization_msgs::Marker marker_;

    public:
    RvizPanel(std::string name, tf2::Transform tf, tf2::Vector3 size);
    void update(tf2::Transform& tf) override;

    inline visualization_msgs::Marker& marker() {return marker_;}

};

#endif