#ifndef PANEL_
#define PANEL_

#include "ros/ros.h"
#include "robot_element/abstract_robot_element.h"

class Panel  : public AbstractRobotElement{
    public:
    Panel(std::string name, tf2::Transform tf, tf2::Vector3 size);
        
    inline void setName(std::string str) {name_ = str;}
    inline void setSize(tf2::Vector3& vec) {size_ = vec;}

    void update(tf2::Transform& tf) override {this->calcWorldTf(tf);}
    
    std::vector<tf2::Transform>& bounds() override {return bounds_;}
    tf2::Transform& worldTf() override {return world_tf_;};
    std::string& name() override {return name_;}
    tf2::Vector3& size() override {return size_;}

};

#endif