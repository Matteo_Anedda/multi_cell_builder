#ifndef MOVEIT_PANEL_
#define MOVEIT_PANEL_

#include "ros/ros.h"
#include "robot_element/abstract_robot_element.h"
#include "robot_element/observers/panel.h"

#include <moveit/planning_scene_interface/planning_scene_interface.h>
#include <moveit/move_group_interface/move_group_interface.h>
#include <moveit_msgs/ApplyPlanningScene.h>
#include <moveit/planning_scene/planning_scene.h>

class MoveitPanel  : public Panel{
    protected:
    moveit_msgs::CollisionObject marker_;

    public:
    MoveitPanel(std::string name, tf2::Transform tf, tf2::Vector3 size);
    void update(tf2::Transform& tf) override;

    inline moveit_msgs::CollisionObject& marker() {return marker_;}
};

#endif