#ifndef LOG_DECORATOR_
#define LOG_DECORATOR_

#include "ros/ros.h"
#include "visualization_msgs/Marker.h"
#include "robot_element/decorators/abstract_robot_element_decorator.h"

class LogDecorator  : public AbstractRobotElementDecorator{

    public:
    LogDecorator(std::unique_ptr<AbstractRobotElement> next);
    void inputFilter() override;
    void update(tf2::Transform& tf) override;
    void outputFilter() override;
};


#endif