#ifndef ABSTRACT_ROBOT_ELEMENT_DECORATOR_
#define ABSTRACT_ROBOT_ELEMENT_DECORATOR_

#include "ros/ros.h"
#include "robot_element/abstract_robot_element.h"

class AbstractRobotElementDecorator  : public AbstractRobotElement{
    protected:
    std::shared_ptr<ros::NodeHandle> nh_;
    std::unique_ptr<AbstractRobotElement> next_;

    public:
    AbstractRobotElementDecorator(std::unique_ptr<AbstractRobotElement> next) 
    : AbstractRobotElement(tf2::Transform(tf2::Quaternion(0,0,0,1), tf2::Vector3(0,0,0)), "Blanc", tf2::Vector3(0,0,0))
    , next_(std::move(next)){};
    
    inline AbstractRobotElement* next(){return next_.get();}

    std::vector<tf2::Transform>& bounds() override {return next_->bounds();}
    tf2::Transform& worldTf() override {return next_->worldTf();}
    std::string& name() override {return next_->name();}
    tf2::Vector3& size() override {return next_->size();}


    virtual void inputFilter()=0;
    void update(tf2::Transform& tf) override {next_->update(tf);}
    virtual void outputFilter()=0;
};


#endif