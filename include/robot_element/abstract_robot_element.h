#ifndef ABSTRACT_ROBOT_ELEMENT_
#define ABSTRACT_ROBOT_ELEMENT_

#include "ros/ros.h"
#include <tf2/LinearMath/Transform.h>

class AbstractRobotElement {
    protected:
    std::string name_;
    tf2::Transform relative_tf_;
    tf2::Transform world_tf_;
    tf2::Vector3 size_;

    std::vector<tf2::Transform> bounds_;

    public:
    AbstractRobotElement(tf2::Transform tf, std::string name, tf2::Vector3 size) 
    : relative_tf_(tf)
    , name_(name)
    , size_(size)
    , world_tf_(tf2::Transform(tf2::Quaternion(0,0,0,1), tf2::Vector3(0,0,0))) {}

    
    inline tf2::Transform& relativeTf(){ return relative_tf_;}
    inline void setRelativeTf(tf2::Transform tf) { relative_tf_= tf;}

    inline void calcWorldTf(tf2::Transform& tf) {world_tf_= tf * relative_tf_;}
    inline void setWorldTf(tf2::Transform& tf) { world_tf_ = tf;}


    virtual std::vector<tf2::Transform>& bounds()=0;
    virtual tf2::Transform& worldTf()=0;
    virtual tf2::Vector3& size()=0;
    virtual std::string& name()=0;


    virtual void update(tf2::Transform& tf)= 0;
};


#endif