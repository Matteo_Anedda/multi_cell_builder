#ifndef SIMPLE_BASE_
#define SIMPLE_BASE_

#include <ros/ros.h>
#include <xmlrpcpp/XmlRpc.h>
#include <tf2/LinearMath/Transform.h>
#include <octomap/octomap.h>
#include <pcl/point_cloud.h>
#include <pcl/octree/octree.h>
#include <ros/package.h>
#include <yaml-cpp/yaml.h>

#include <fstream>

#include "bridge/abstract_base.h"
#include "reader/map_reader.h"
#include "reader/ts_reader.h"


//! SimpleBase as refinement of AbstractBase
/*!
A simple refinement of AbstractBase where concept variables are set by reader instances instead of being computed.
*/
class SimpleBase : public AbstractBase {
    protected:
    std::unique_ptr<TSReader> task_space_reader_;  /*!<  Task space reader instance */
    std::unique_ptr<MapReader> map_reader_; /*!<  Reachability map reader instance */

    public:
    //! Simple base constructor 
    /*!
        \param d ros::NodeHandle to interact with paramserver
    */
    SimpleBase(std::shared_ptr<ros::NodeHandle> const& d);

    //! refined Template methode
    /*!
      Overriden methide which calls necessary functions from implementation
      \param var store inversed reachability map data
      \return map of base positions linked to robots
    */
    void baseCalculation() override;
};
#endif