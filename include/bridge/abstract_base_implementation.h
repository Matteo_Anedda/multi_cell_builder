#ifndef ABSTRACT_BASE_IMPLEMENTATION_
#define ABSTRACT_BASE_IMPLEMENTATION_

#include <ros/ros.h>

#include "bridge/abstract_base.h"

//! Abstract base implementation
/*!
Abstract concept implementation for calculating base position by inverse maps. The template allows to set custom handle orientations for specific objects, inverse map and cloud construction.
*/
class AbstractBaseImplementation {
    public:
    AbstractBaseImplementation()= default;
    ~AbstractBaseImplementation()= default;

    //!  pure virtual methode to set grasp orientations
    /*!
    Manipulates the map of grasp orientations of a graspable object, mapped to a robot
    \param var caller reference to manipulate member
    */
    virtual void setGraspOrientations(std::map<const std::string, std::vector<std::pair<object_data, std::vector<tf2::Quaternion>>>>& var)=0;

    //!  pure virtual methode to calculate the inverse map
    /*!
    Manipulates the inv_map vector
    \param map reference to manipulate member
    \param inv_map reference to manipulate member
    \param task_space reference to manipulate member
    */
    virtual void invMapCreation(std::vector<tf2::Transform>& map, std::vector<tf2::Transform>& inv_map, std::map<const std::string, std::vector<std::pair<object_data,std::vector<tf2::Quaternion>>>>& task_space)=0;

    //!  pure virtual methode to calculate pcl clouds
    /*!
    Manipulates the vector of pcl::cloud of a graspable object
    \param inv_map reference to manipulate member
    \param task_space reference to manipulate member
    \param target_cloud reference to manipulate member
    */
    virtual void cloudCalculation(std::vector<tf2::Transform>& inv_map, std::map<const std::string, std::vector<std::pair<object_data,std::vector<tf2::Quaternion>>>>& task_space, std::map<const std::string, std::vector<pcl::PointCloud< pcl::PointXYZ >::Ptr>>& target_cloud)=0;


};

#endif