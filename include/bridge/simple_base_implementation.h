#ifndef SIMPLE_BASE_IMPLEMENTATION_
#define SIMPLE_BASE_IMPLEMENTATION_

#include <ros/ros.h>

#include "bridge/abstract_base_implementation.h"
#include "bridge/abstract_base.h"

//!  SimpleBaseImplementation as refinement of AbstractBaseImplementation
/*!
general approach to the calculation of the base, in which the orientations of the objects are considered to be equivalent
*/
class SimpleBaseImplementation : public AbstractBaseImplementation {
    public:
    SimpleBaseImplementation()= default;
    ~SimpleBaseImplementation()= default;

    //! Store grasp orientations as endeffector transforms
    /*!
    The 5 most basic Orientatations (x, -x, y, -y, z) for all graspable objects
        \param var manipulate grasp orienattions mapped to object and robot
    */
    void setGraspOrientations(std::map<const std::string, std::vector<std::pair<object_data, std::vector<tf2::Quaternion>>>>& var) override;

    //! Create inversed reachability map
    /*!
    Creating inversed reachability map by inverting reachability map if grasp orientations match reachability map entries
        \param map store inversed reachability map data
        \param inv_map store inversed reachability map data
        \param task_space store inversed reachability map data
    */
    void invMapCreation(std::vector<tf2::Transform>& map, std::vector<tf2::Transform>& inv_map, std::map<const std::string, std::vector<std::pair<object_data,std::vector<tf2::Quaternion>>>>& task_space) override;

    //! PCL Cloud calculation for every Object
    /*!
        \param var store cload data in proper map
    */
    void cloudCalculation(std::vector<tf2::Transform>& inv_map, std::map<const std::string, std::vector<std::pair<object_data,std::vector<tf2::Quaternion>>>>& task_space, std::map<const std::string, std::vector<pcl::PointCloud< pcl::PointXYZ >::Ptr>>& target_cloud) override;

    
};

#endif