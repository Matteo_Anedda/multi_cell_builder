#ifndef ABSTRACT_BASE_
#define ABSTRACT_BASE_

#include <ros/ros.h>
#include <xmlrpcpp/XmlRpc.h>
#include <tf2/LinearMath/Transform.h>
#include <octomap/octomap.h>
#include <pcl/point_cloud.h>
#include <pcl/octree/octree.h>
#include <ros/package.h>
#include <yaml-cpp/yaml.h>

#include <fstream>

#include "reader/abstract_param_reader.h"
#include "robot/abstract_robot.h"
#include "bridge/abstract_base_implementation.h"

class AbstractBaseImplementation;
class AbstractRobot;


//!  AbstractBaseClass
/*!
Abstract base class that calls functions of the implementation class members. Its members represent the data structures of the Zacharias concept and enable costume approaches through the design of bridge patterns.
*/
class AbstractBase{
    protected:
    std::shared_ptr<ros::NodeHandle> nh_; //!< Nodehandle for access to the Rosparam server
    std::vector<tf2::Transform> map_; //!< ReachabilityMap structure (Zacharias)
    std::vector<tf2::Transform> inv_map_; //!< InverseReachabilityMap structure (Zacharias)
    std::map<const std::string, std::vector<std::pair<object_data,std::vector<tf2::Quaternion>>>> task_space_; //!< Drop-off locations with their grasp orientations, mapped to a robot
    std::map<const std::string, std::vector<pcl::PointCloud< pcl::PointXYZ >::Ptr>> target_cloud_; //!< Pointcloud structure, mapped to robot 
    std::map<const std::string, std::vector<pcl::PointXYZ>> result_; //!< Result basepositions, mapped to robot


    std::shared_ptr<AbstractBaseImplementation> implementation_; //!< refined implementation 

    public:
    //! constructor
    /*!
      \param nh ros::NodeHandle to interact with paramserver
    */
    AbstractBase(std::shared_ptr<ros::NodeHandle> const& nh) : nh_(nh){};


    inline AbstractBaseImplementation* implementation() {return implementation_.get();}
    inline void setImplementation(std::shared_ptr<AbstractBaseImplementation> implementation) {implementation_ = implementation;}
    inline std::map<const std::string, std::vector<pcl::PointXYZ>>& result() { return result_;}
    inline void setResult(std::map<const std::string, std::vector<pcl::PointXYZ>>& result) { result_ = result;}
    inline std::vector<tf2::Transform>& invMap() {return inv_map_;}
    inline void setInvMap(std::vector<tf2::Transform>& inv_map) {inv_map_ = inv_map;}
    inline std::vector<tf2::Transform>& map() {return map_;}
    inline void setMap(std::vector<tf2::Transform>& map) {map_ = map;}
    inline std::map<const std::string, std::vector<std::pair<object_data,std::vector<tf2::Quaternion>>>>& taskSpace() {return task_space_;}
    inline void setTaskSpace(std::map<const std::string, std::vector<std::pair<object_data,std::vector<tf2::Quaternion>>>>& s_trans) {task_space_ = s_trans;}
    inline std::map<const std::string, std::vector<pcl::PointCloud< pcl::PointXYZ >::Ptr>>& targetCloud () { return target_cloud_;}
    inline void setTargetCloud(std::map<const std::string, std::vector<pcl::PointCloud< pcl::PointXYZ >::Ptr>>& cloud) {target_cloud_ = cloud;}

    //! createPCLBox box 
    /*!
      creates box shaped discretization of space, centered at origin (0,0,0)
      \return vector of pcl::PointXYZ data
    */
    static std::vector<pcl::PointXYZ> createPCLBox();

    //! pure virtual template methode 
    /*! 
      bridge template methode, in which implementation is called to calculate base positions
    */
    virtual void baseCalculation()=0;
};
#endif
