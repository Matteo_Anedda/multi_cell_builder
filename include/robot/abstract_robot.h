#ifndef ABSTRACT_ROBOT_
#define ABSTRACT_ROBOT_

#include "ros/ros.h"
#include <tf2/LinearMath/Transform.h>
#include <bitset>

#include "robot_element/abstract_robot_element.h"

enum wing_config{
    RML = 7,
    RM_ = 6,
    R_L = 5,
    R__ = 4,
    _ML = 3,
    _M_ = 2,
    __L = 1,
    ___ = 0
};

//! Abstract Robot implementation
/*!
Defines a Robot as table plus Manipulator on top.
*/
class AbstractRobot {
    protected:
    std::string name_; //!< Name of robot
    tf2::Vector3 size_; //!< Size of table 
    tf2::Transform tf_; //!< Pose of table 
    tf2::Transform root_tf_; //!< Robot root on table top
    std::vector<tf2::Transform> bounds_; //!< Bounds of table top surface
    std::vector<tf2::Transform> robot_root_bounds_; //!< Bounds of robot arm as sub-region of table top
    
    public:
    AbstractRobot(std::string name, tf2::Transform tf, tf2::Vector3 size);

    virtual std::string& name()=0; 
    virtual tf2::Transform& tf()=0; 
    virtual tf2::Vector3& size()=0; 
    virtual tf2::Transform& rootTf()=0; 
    virtual std::vector<tf2::Transform>& bounds()=0; 
    virtual std::vector<tf2::Transform>& robotRootBounds()=0; 
    virtual bool checkSingleObjectCollision(tf2::Transform& obj, std::string& robot_element, std::bitset<3>& panel_mask) =0;



    inline void size(tf2::Vector3& s) { size_ = s;}
    inline void setTf(tf2::Transform& t) { tf_ = t;}
    inline void rotate(float deg) {tf2::Quaternion rot; rot.setRPY(0,0,deg); tf2::Transform t(rot, tf2::Vector3(0,0,0)); tf_= tf_* t;}
    inline void translate(tf2::Vector3 t) {tf2::Transform tf(tf2::Quaternion(0,0,0,1), t); tf_*=tf;}
    virtual void notify()= 0;
    
    //! Triangle area calculator
    /*!
        Calculates triangle with 3 vertices
        \param A pose of triagnel vertice
        \param B pose of triagnel vertice
        \param C pose of triagnel vertice
        \return float triangle area
    */
    float areaCalculation(tf2::Transform& A, tf2::Transform& B, tf2::Transform& C);

};


#endif