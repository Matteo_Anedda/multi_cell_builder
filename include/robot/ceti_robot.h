#ifndef CETI_ROBOT_
#define CETI_ROBOT_

#include "ros/ros.h"
#include "robot/abstract_robot.h"
#include "robot_element/abstract_robot_element.h"
#include "robot_element/decorators/abstract_robot_element_decorator.h"
#include "robot_element/observers/panel.h"

//! Concrete Ceti-Robot
/*!
Refers especially to Robot-Arms mounted on a table of variable size. 
*/
class CetiRobot : public AbstractRobot{
    protected:
    std::vector<std::unique_ptr<AbstractRobotElementDecorator>> observers_;  //!< Wing shared pointers
    std::bitset<3> observer_mask_;//!< Bitmap to set observers


    public:
    //! Ceti robot constructor
    /*!
    Initializes Ceti-Robot, calculates bounds out of arm and table size. 
        \param name std::string as Robot name, following the Robotarm([0-9]+) pattern
        \param tf Table center pose
        \param size Table size
    */
    CetiRobot(std::string& name, tf2::Transform tf, tf2::Vector3 size);



    std::string& name() override {return name_;}
    tf2::Transform& tf() override {return tf_;}
    tf2::Vector3& size() override {return size_;}
    tf2::Transform& rootTf() override {return root_tf_;}
    std::vector<tf2::Transform>& bounds() override {return bounds_;}
    std::vector<tf2::Transform>& robotRootBounds() override {return robot_root_bounds_;}
    
    //! Set observers
    /*!
        \param observer vector
    */
    inline void setObservers(std::vector<std::unique_ptr<AbstractRobotElementDecorator>>& observer) { 
        for (int i = 0; i < observer.size();i++){
            observers_.push_back(std::move(observer[i]));
        }
    }

    //! Get observers
    /*!
        \return observer vector
    */
    inline std::vector<std::unique_ptr<AbstractRobotElementDecorator>>& observers() { return observers_;}

    //! Resset all robot properties
    void reset();
    
    //! Collsion calculation
    /*!
        Iterates over Robot to calculate area und check collisions by other robot bounds
        \param R Robot which bounds are to check for collisions
        \return bool true if collision, false otherwise 
    */
    bool inCollision(CetiRobot* R);

    //! Collsion calculation for single objects 
    /*!
        Calculates collision between objects and robot_elemets, which are defined by the robot and its panels. Provides collision surface
        \param[IN] obj pose to check
        \param[IN] panel_mask discribing panels to check
        \param[OUT] robot_element std::string describing the collision surface
        \return true if collsision on robot or wings
    */
    bool checkSingleObjectCollision(tf2::Transform& obj, std::string& robot_element, std::bitset<3>& panel_mask) override;

    //! Observer pattern 
    /*!
        When changing pose, any observer calculates its new position in world frame
    */
    void notify() override;

    inline std::bitset<3> observerMask() {return observer_mask_;}
    inline void setObserverMask(int i) {observer_mask_ = std::bitset<3>(i);}



};




#endif