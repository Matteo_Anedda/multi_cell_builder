#ifndef UR5_DECORATOR_
#define UR5_DECORATOR_

#include "ros/ros.h"
#include "robot/abstract_robot.h"
#include "robot/decorators/abstract_robot_decorator.h"


//!  UR5 Decorator 
/*!
  Decorator who describes the root-area of the 'UR5' Robot
*/
class UR5Decorator  : public AbstractRobotDecorator{
    public:

    //! UR5 Decorator
    /*!
        initializes UR5 Decorator 
        \param next Abstract Robot to be mimiced
    */
    UR5Decorator(std::unique_ptr<AbstractRobot> next);

    //! UR5 Root spezififcation 
    /*!
        Overrides the actual Robot root bounds with a square, described from information of 'UR5' manual
    */
    void spezifieRootBounds() override;

    //! Panda Mapping spezififcation
    /*!
        Overrides the actual Robot mappings such as that it can further be used with Moveit Task Constructor 
    */
    void spezifieRobotGroups() override;

};


#endif