#ifndef ABSTRACT_ROBOT_DECORATOR_
#define ABSTRACT_ROBOT_DECORATOR_

#include <ros/ros.h>

#include <moveit/planning_pipeline/planning_pipeline.h>
#include <moveit/planning_scene_interface/planning_scene_interface.h>
#include <moveit/move_group_interface/move_group_interface.h>
#include <moveit_msgs/ApplyPlanningScene.h>
#include <moveit/robot_model_loader/robot_model_loader.h>
#include <moveit/planning_scene/planning_scene.h>
#include <moveit/kinematic_constraints/utils.h>
#include <gb_grasp/MapGenerator.h>

#include "robot/abstract_robot.h"


//!  Abstract Robot Decorator
/*!
    Implements the concept of different manifactures, who can set their own Tf_root area
*/
class AbstractRobotDecorator  : public AbstractRobot{
    protected:
    std::unique_ptr<AbstractRobot> next_; //!< Abstract Robot which is mimiced
    std::shared_ptr<moveit::planning_interface::MoveGroupInterface> mgi_; //!< MoveGroup Interface for the robots arm
    std::shared_ptr<moveit::planning_interface::MoveGroupInterface> mgi_hand_; //!< MoveGroup Interface for the Robots manipulator

    std::map<std::string, std::string>  map_; //!< //!< Mapping of specific task constructor variables
    std::shared_ptr<MapGenerator> grasp_map_generator_; //!< Coming soon 
    std::string pattern_; //!< Regexpattern 


    public:
    //! Abstract Robot Decorator
    /*!
        initializes Abstract Robot Decorator 
        \param next Abstract Robot to be mimiced
    */
    AbstractRobotDecorator(std::unique_ptr<AbstractRobot> next) 
    : AbstractRobot("blanc", tf2::Transform(tf2::Quaternion(0,0,0,1), tf2::Vector3(0,0,0)), tf2::Vector3(0,0,0))
    , mgi_(nullptr)
    , mgi_hand_(nullptr)
    , next_(std::move(next))
    , pattern_("*._arm([0-9]+)"){};

    //! Redirects name call to next_
    std::string& name() override { return next_->name();}

    //! Redirects check_single_object_collision call
    bool checkSingleObjectCollision(tf2::Transform& obj, std::string& robot_element, std::bitset<3>& panel_mask) override { return next_->checkSingleObjectCollision(obj, robot_element, panel_mask);}

    //! Redirects tf call to next_
    tf2::Transform& tf() override { return next_->tf();}

    //! Redirects size call to next_
    tf2::Vector3& size() override { return next_->size();}

    //! Redirects root_tf call to next_
    tf2::Transform& rootTf() override { return next_->rootTf();}

    //! Redirects bounds call to next_
    std::vector<tf2::Transform>& bounds() override {return next_->bounds();}

    //! Redirects robot_root_bounds call to next_
    std::vector<tf2::Transform>& robotRootBounds() override { return next_->robotRootBounds();}    

    //! Redirects notify call to next_
    void notify() override {next_->notify();}
    
    //! Call of mimiced objects
    /*!
        \return Abstract_robot* to mimiced object
    */
    inline AbstractRobot* next(){return next_.get();}

    //! Pure virtual function to specify root bounds
    virtual void spezifieRootBounds()=0;

    //! Pure virtual function to specify root groupts
    virtual void spezifieRobotGroups()=0;

    inline std::string& pattern() {return pattern_;}
    

    inline std::map<std::string, std::string>&  map(){return map_;} //!< //!< Mapping of specific task constructor variables

    inline std::shared_ptr<moveit::planning_interface::MoveGroupInterface> mgi() {return mgi_;}
    inline std::shared_ptr<moveit::planning_interface::MoveGroupInterface> mgiHand() {return mgi_hand_;}
};

#endif