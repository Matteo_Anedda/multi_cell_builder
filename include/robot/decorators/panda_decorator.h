#ifndef PANDA_DECORATOR_
#define PANDA_DECORATOR_

#include "ros/ros.h"
#include "robot/abstract_robot.h"
#include "robot/decorators/abstract_robot_decorator.h"


//!  Panda Decorator
/*!
  Decorator which describes the root-area of the 'Franca Emika Panda' Robot
*/
class PandaDecorator  : public AbstractRobotDecorator{
    public:
    //! Panda Decorator
    /*!
        initializes Panda Decorator 
        \param next Abstract Robot to be mimiced
    */
    PandaDecorator(std::unique_ptr<AbstractRobot> next);

    //! Panda Root spezififcation 
    /*!
        Overrides the actual Robot root bounds with a rectangle, described from information of 'Franka Panda' manual
    */
    void spezifieRootBounds() override;


    //! Panda Mapping spezififcation
    /*!
        Overrides the actual Robot mappings such as that it can further be used with Moveit Task Constructor 
    */
    void spezifieRobotGroups() override;

};


#endif