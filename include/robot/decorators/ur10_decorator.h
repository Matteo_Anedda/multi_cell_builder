#ifndef UR10_DECORATOR_
#define UR10_DECORATOR_

#include "ros/ros.h"
#include "robot/abstract_robot.h"
#include "robot/decorators/abstract_robot_decorator.h"

//!  Panda Mediator
/*!
  Decorator who describes the stand-plane/root-area of the 'UR10' Robot
*/
class UR10Decorator  : public AbstractRobotDecorator{
    public:

    //! UR10 Decorator
    /*!
        initializes UR10 Decorator 
        \param next Abstract Robot to be mimiced
    */
    UR10Decorator(std::unique_ptr<AbstractRobot> next);

    //! UR10 Root spezififcation 
    /*!
        Overrides the actual Robot root bounds with a square, described from information of 'UR10' manual
    */
    void spezifieRootBounds() override;

    //! UR10 Mapping spezififcation
    /*!
        Overrides the actual Robot mappings such as that it can further be used with Moveit Task Constructor 
    */
    void spezifieRobotGroups() override;

};


#endif