#ifndef CUBOID_READER_
#define CUBOID_READER_

#include <ros/ros.h>
#include <ros/package.h>
#include <xmlrpcpp/XmlRpc.h>
#include <gb_grasp/Cuboid.h>


#include "reader/abstract_param_reader.h"


//!  Cuboid reader
/*!
    Reader which returns cuboid objects as defined in gb_grasp
*/
class CuboidReader : public AbstractParamReader{
    protected:
    std::vector<Cuboid> cuboid_box_; //!< As box defined object
    std::vector<Cuboid> cuboid_obstacle_; //!< As obstacle defined objects

    public:
    //!  Cuboid reader constructor
    /*!
        Calls pure virtual read() methode
            \param d ROS nodehandle 
    */
    CuboidReader(std::shared_ptr<ros::NodeHandle> const& d) : AbstractParamReader(d){read();}
    
    //!  Set Cuboid box
    /*!
            \param cuboid_data 
    */
    inline void setCuboidBox(std::vector<Cuboid>& cuboid_data) {cuboid_box_ = cuboid_data;}

    //!  Set Cuboid obstacle
    /*!
            \param cuboid_data
    */
    inline void setCuboidObstacle(std::vector<Cuboid>& cuboid_data) {cuboid_obstacle_ = cuboid_data;}

    //!  Get Cuboid box
    /*!
            \return  Cuboid box vector
    */
    inline std::vector<Cuboid>& cuboidBox() {return cuboid_box_;}

    //!  Get Cuboid obstacle
    /*!
            \return  Cuboid obstacle vector
    */
    inline std::vector<Cuboid>& cuboidObstacle() {return cuboid_obstacle_;}

    //!  read implementatin
    void read() override;
};
#endif
