#ifndef MAP_READER_
#define MAP_READER_

#include "ros/ros.h"
#include <ros/package.h>
#include <xmlrpcpp/XmlRpc.h>

#include "reader/abstract_param_reader.h"


//!  Map reader
/*!
    Reads Reachabilty Map vector
*/
class MapReader : public AbstractParamReader{
    protected:
    std::vector<tf2::Transform> map_data_; //!< Map transforms

    public:
    //!  Map reader
    /*!
        Calls pure virtual read() methode
            \param d Ros Nodehandle
    */
    MapReader(std::shared_ptr<ros::NodeHandle> const& d) : AbstractParamReader(d){read();}
    
    //!  Set map_data
    /*!
            \param robot_data vector of transforms
    */
    inline void setMapData(std::vector<tf2::Transform>& robot_data) {map_data_ = robot_data;}

    //!  Get map_data
    /*!
            \return vector of transforms
    */
    inline std::vector<tf2::Transform>& mapData() {return map_data_;}

    //!  read implementatin
    void read() override;
};
#endif
