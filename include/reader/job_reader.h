#ifndef JOB_READER_
#define JOB_READER_

#include "ros/ros.h"
#include <ros/package.h>
#include <xmlrpcpp/XmlRpc.h>
#include "boost/circular_buffer.hpp"


#include "reader/abstract_param_reader.h"

//!  Job reader
/*!
    Reader which returns job data liked to robot
*/
class JobReader : public AbstractParamReader{
    protected:
    boost::circular_buffer<std::pair<std::string, job_data>> job_data_; //!< FIFO job information 

    public:
    //!  Job reader constructor
    /*!
        Calls read() implementation
            \param d ROS Nodehandle         
    */
    JobReader(std::shared_ptr<ros::NodeHandle> const& d) 
    : AbstractParamReader(d)
    {read();}
    
    //!  Set Job_data
    /*!
        Calls pure virtual read() methode
            \param robot_data FIFO job information      
    */
    inline void setJobData(boost::circular_buffer<std::pair<std::string, job_data>>& robot_data) {job_data_ = robot_data;}

    //!  Get Job_data
    /*!
            \return FIFO job information      
    */
    inline boost::circular_buffer<std::pair<std::string, job_data>>& robotData() {return job_data_;}

    //!  read implementation
    void read() override;
};
#endif
