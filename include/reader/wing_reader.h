#ifndef WING_READER_
#define WING_READER_

#include "ros/ros.h"
#include <ros/package.h>
#include <xmlrpcpp/XmlRpc.h>

#include "reader/abstract_param_reader.h"

//!  Wing reader 
/*!
    Reads panel data 
*/
class WingReader : public AbstractParamReader{
    protected:
    std::map<const std::string, std::pair<std::vector<object_data>, int>> wing_data_;  //!< Mask and object data mapped to robot

    public:
    //!  Wing reader constructor
    /*!
        Calls pure virtual read() methode
            \param d ROS nodehandle 
    */
    WingReader(std::shared_ptr<ros::NodeHandle> const& d) : AbstractParamReader(d){read();}
    
    //!  Set wing data
    /*!
            \param wing_data  Mask and object data mapped to robot
    */
    inline void setWingData(std::map<const std::string, std::pair<std::vector<object_data>, int>>& wing_data) {wing_data_ = wing_data;}

    //!  Get wing data
    /*!
            \return wing_data  Mask and object data mapped to robot
    */
    inline std::map<const std::string, std::pair<std::vector<object_data>, int>>& wingData() {return wing_data_;}

    //!  read implementatin
    void read() override;
};
#endif