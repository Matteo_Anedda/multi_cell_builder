#ifndef TASK_SPACE_READER_
#define TASK_SPACE_READER_

#include "ros/ros.h"
#include <ros/package.h>
#include <xmlrpcpp/XmlRpc.h>

#include "reader/abstract_param_reader.h"


//!  Task space reader
/*!
    Reads task space information an writes them into file
*/
class TaskSpaceReader : public AbstractParamReader{
    public:
    //!  Task space reader constructor
    /*!
        Calls pure virtual read() methode
            \param d ROS nodehandle 
    */
    TaskSpaceReader(std::shared_ptr<ros::NodeHandle> const& d) : AbstractParamReader(d){read();}
    
    //!  read implementatin
    void read() override;
};
#endif
