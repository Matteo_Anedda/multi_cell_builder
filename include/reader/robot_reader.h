#ifndef ROBOT_READER_
#define ROBOT_READER_

#include "ros/ros.h"
#include <ros/package.h>
#include <xmlrpcpp/XmlRpc.h>

#include "reader/abstract_param_reader.h"


//!  Robot reader
/*!
    Reads size and pose data for robot
*/
class RobotReader : public AbstractParamReader{
    protected:
    std::vector<object_data> robot_data_; //!< Map of object_data to robot

    public:
    //!  Robot reader constructor
    /*!
        Calls pure virtual read() methode
            \param d ROS nodehandle 
    */
    RobotReader(std::shared_ptr<ros::NodeHandle> const& d) : AbstractParamReader(d){read();}
    
    //!  Set robot data
    /*!
            \param robot_data Vector of object data
    */
    inline void setRobotData(std::vector<object_data>& robot_data) {robot_data_ = robot_data;}

    //!  Get robot data
    /*!
            \return Vector of object data
    */
    inline std::vector<object_data> robotData() {return robot_data_;}

    //!  read implementatin
    void read() override;
};
#endif
