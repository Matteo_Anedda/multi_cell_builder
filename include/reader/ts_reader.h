#ifndef TS_READER_
#define TS_READER_

#include "ros/ros.h"
#include <ros/package.h>
#include <xmlrpcpp/XmlRpc.h>

#include "reader/abstract_param_reader.h"


//!  TS reader 
/*!
    Reads task space data
*/
class TSReader : public AbstractParamReader{
    protected:
    std::map<const std::string, std::vector<object_data>> drop_off_data_;  //!< Map of drop off data to robot

    public:
    //!  TS reader constructor
    /*!
        Calls pure virtual read() methode
            \param d ROS nodehandle 
    */
    TSReader(std::shared_ptr<ros::NodeHandle> const& d) : AbstractParamReader(d){read();}

    //!  Get drop off data
    /*!
            \return Map of drop off data to robot
    */
    inline std::map< const std::string, std::vector<object_data>>& dropOffData() {return drop_off_data_;}

    //!  Set drop off data
    /*!
            \param dod of drop off data to robot
    */
    inline void setDropOffData(std::map< const std::string, std::vector<object_data>>& dod) {drop_off_data_= dod;}

    //!  read implementatin
    void read() override;
};
#endif
