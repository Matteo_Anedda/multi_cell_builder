#ifndef ABSTRACT_PARAM_READER_
#define ABSTRACT_PARAM_READER_

#include <ros/ros.h>
#include <ros/package.h>
#include <xmlrpcpp/XmlRpc.h>
#include <tf2/LinearMath/Transform.h>
#include <yaml-cpp/yaml.h>
#include <fstream>
#include <regex>
#include <yaml-cpp/node/detail/node_data.h>

//!  Object data
struct object_data {
    std::string name_;
    tf2::Transform pose_;
    tf2::Vector3 size_;
};

//!  Job data
struct job_data {
    std::vector<tf2::Transform> jobs_;
};

//!  Abstract ROS-Param reader class
/*!
    Class concerned with reading information from ROS-Param server.
*/
class AbstractParamReader{
    protected:
    std::shared_ptr<ros::NodeHandle> nh_; //!< ROS Nodehandle

    public:
    //!  AbstractParamReader constructor
    /*!
            \param d ROS Nodehandle
    */
    AbstractParamReader(std::shared_ptr<ros::NodeHandle> const& d) : nh_(d){}

    //!  Xmlrpc parser 
    /*!
        converts param to float value
            \param val XmlRPCValue reference from Nodehandle
    */
    float floatOf(XmlRpc::XmlRpcValue& val);

    //!  pure virtual read methode 
    virtual void read()=0;
};
#endif
