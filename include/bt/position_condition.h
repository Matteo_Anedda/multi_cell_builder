#ifndef POSITION_CONDITION_
#define POSITION_CONDITION_

#include <ros/ros.h>
#include <behaviortree_cpp_v3/behavior_tree.h>
#include <behaviortree_cpp_v3/tree_node.h>
#include <behaviortree_cpp_v3/basic_types.h>
#include <behaviortree_cpp_v3/leaf_node.h>
#include <moveit_task_constructor_msgs/ExecuteTaskSolutionAction.h>
#include <moveit/planning_scene_interface/planning_scene_interface.h>
#include <moveit/move_group_interface/move_group_interface.h>
#include <moveit_msgs/ApplyPlanningScene.h>
#include <moveit/robot_model_loader/robot_model_loader.h>
#include <moveit/planning_scene/planning_scene.h>
#include <moveit/kinematic_constraints/utils.h>

//! PositionCondition Node
/*!
Implements a pre-condition, in which Objects require a specific position to be processed. 
*/
class PositionCondition : public BT::ConditionNode {
    public:

    //! PositionCondition constructor
    /*!
      \param name Name displayed in Groot
      \param config Node configuration
    */
    PositionCondition(const std::string& name, const BT::NodeConfiguration& config);

    //! Initialize Node
    /*!
      \param obj_name Name of Object
      \param start_pos Position which is to check
      \param psi Planning scene interface reference
    */
    void init(const std::string& obj_name,tf2::Vector3& start_pos, moveit::planning_interface::PlanningSceneInterface* psi);

    //! Required port inferface
    inline static BT::PortsList providedPorts() { return {}; }

    protected:
    //! tick function
    /*!
      Actual object position is retrived by name and checked againt the start position of the execution
      \param obj_name Name of Object
      \param start_pos Position which is to check
      \param psi Planning scene interface reference
    */
    virtual BT::NodeStatus tick() override;

    std::string obj_name_; //!< Object name
    moveit::planning_interface::PlanningSceneInterface* psi_; //!< PSI 
    tf2::Vector3 start_pos_; //!< Start position
};

#endif