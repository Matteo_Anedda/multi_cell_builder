#ifndef EXECUTION_
#define EXECUTION_

#include <ros/ros.h>
#include <behaviortree_cpp_v3/behavior_tree.h>
#include <behaviortree_cpp_v3/tree_node.h>
#include <behaviortree_cpp_v3/basic_types.h>
#include <behaviortree_cpp_v3/leaf_node.h>
#include <moveit/move_group_interface/move_group_interface.h>
#include <moveit_msgs/ApplyPlanningScene.h>
#include <moveit/robot_model_loader/robot_model_loader.h>
#include <moveit/planning_scene/planning_scene.h>
#include <moveit/kinematic_constraints/utils.h>
#include <moveit_task_constructor_msgs/ExecuteTaskSolutionAction.h>

#include "mediator/abstract_mediator.h"
#include "robot/decorators/abstract_robot_decorator.h"

//! Execution node as StatefulActionNode
/*!
Execution node that maintains its state (success, failure or execution). It implements robot task execution as a model in which robots can be treated as threads. It represents a container of successive tasks that a robot has to execute and stores a trajectory and a planning scene per task and robot.
*/
class Execution : public BT::StatefulActionNode{
    private:
    moveit_task_constructor_msgs::ExecuteTaskSolutionGoal ets_; //!< ExecuteTaskSolutionGoal, which is a reference to trajetory and planningscene
    AbstractRobotDecorator* mr_reference_; //!< Reference to the robot
    std::map<std::string, std::pair<moveit_msgs::RobotTrajectory, moveit_msgs::PlanningScene>>* executions_; //!< trajetory and planningscene mapped to robot
    std::vector<moveit_task_constructor_msgs::SubTrajectory>::iterator it_; //!< next iteration of ExecuteTaskSolutionGoal


    public:

    //! constructor
    /*!
      \param name Name displayed in Groot
      \param config Node configuration
    */
    Execution(const std::string& name, const BT::NodeConfiguration& config);

    //! Initialize Node
    /*!
      \param executions_ 
      \param mr_reference_
      \param ets_
    */
    void init(std::map<std::string, std::pair<moveit_msgs::RobotTrajectory, moveit_msgs::PlanningScene>>* executions_, AbstractRobotDecorator* mr_reference_, moveit_task_constructor_msgs::ExecuteTaskSolutionGoal& ets_);

    //! Required port interface
    inline static BT::PortsList providedPorts() { return {}; }

    //! Function called at first execution
    BT::NodeStatus onStart() override;

    //! Function called while node is in running state
    BT::NodeStatus onRunning() override;

    //! Function called on halt
    void onHalted() override;
};
#endif



