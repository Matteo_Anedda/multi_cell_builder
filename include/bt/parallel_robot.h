#ifndef PARALLEL_ROBOT_
#define PARALLEL_ROBOT_

#pragma once

#include <set>
#include "behaviortree_cpp_v3/control_node.h"

class Parallel_robot : public BT::ControlNode{
public:
  Parallel_robot(const std::string& name, int success_threshold, int failure_threshold = 1);
  Parallel_robot(const std::string& name, const BT::NodeConfiguration& config);

  static BT::PortsList providedPorts(){
    return {BT::InputPort<int>(THRESHOLD_SUCCESS, "number of childen which need to succeed "
                                              "to "
                                              "trigger a SUCCESS"),
            BT::InputPort<int>(THRESHOLD_FAILURE, 1,
                           "number of childen which need to fail to trigger a FAILURE")};
  }

  ~Parallel_robot() override = default;

  virtual void halt() override;

  size_t successThreshold() const;
  size_t failureThreshold() const;
  void setSuccessThreshold(int threshold_M);
  void setFailureThreshold(int threshold_M);

private:
  int success_threshold_;
  int failure_threshold_;

  std::set<int> skip_list_;

  bool read_parameter_from_ports_;
  static constexpr const char* THRESHOLD_SUCCESS = "success_threshold";
  static constexpr const char* THRESHOLD_FAILURE = "failure_threshold";

  virtual BT::NodeStatus tick() override;
};

#endif


