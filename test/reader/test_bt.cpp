#include <ros/ros.h>
#include <behaviortree_cpp_v3/behavior_tree.h>
#include <behaviortree_cpp_v3/tree_node.h>
#include <behaviortree_cpp_v3/bt_factory.h>
#include <behaviortree_cpp_v3/loggers/bt_zmq_publisher.h>
#include <stdint.h>

#include "reader/abstract_param_reader.h"
#include "bt/execution.h"
#include "bt/position_condition.h"
#include "bt/parallel_robot.h"

#include <boost/circular_buffer.hpp>

TEST(BtTestSuit, basicParallelTest){
	std::map<const std::string, std::vector<std::tuple<const std::string, tf2::Vector3, std::vector<moveit_task_constructor_msgs::ExecuteTaskSolutionGoal>>>> task_but_different;

    boost::circular_buffer<std::pair<std::string, job_data>> cb_jd(2);
    job_data job1;
    job_data job2;
    job1.jobs_ = {tf2::Transform(tf2::Quaternion(0,0,0,1),tf2::Vector3(0,0,0)), tf2::Transform(tf2::Quaternion(0,0,0,1),tf2::Vector3(0,1,0)), tf2::Transform(tf2::Quaternion(0,0,0,1),tf2::Vector3(1,1,0)), tf2::Transform(tf2::Quaternion(0,0,0,1),tf2::Vector3(1,0,0))};
    job2.jobs_ = {tf2::Transform(tf2::Quaternion(2,2,0,1),tf2::Vector3(0,0,0)), tf2::Transform(tf2::Quaternion(0,0,0,1),tf2::Vector3(2,3,0)), tf2::Transform(tf2::Quaternion(0,0,0,1),tf2::Vector3(3,3,0)), tf2::Transform(tf2::Quaternion(0,0,0,1),tf2::Vector3(3,2,0))};

    std::vector<std::pair<std::string,tf2::Vector3>> boxes = {{"box1", tf2::Vector3(0,0,0)}, {"box2", tf2::Vector3(2,2,0)}};

    bool jobs_remaining = true;
    while (jobs_remaining && !cb_jd.empty() && !boxes.empty()) {
        bool moved = false;
        for (auto it = cb_jd.begin(); it != cb_jd.end();) {
            auto& temp = *it;
            bool ready_to_move = true;
            for (auto& box : boxes) {
                if (temp.second.jobs_.front().getOrigin() == box.second) {
                    for (int k = 1; k < temp.second.jobs_.size(); k++) {
                        box.second = temp.second.jobs_[k].getOrigin();
                    }
                    if (ready_to_move) {
                        moved = true;
                        it = cb_jd.erase(it);
                        break;
                    }
                } else {
                    ready_to_move = false;
                }
            }
            if (!ready_to_move) {
                ++it;
            }
        }
        jobs_remaining = moved;
    }
    ASSERT_EQ(cb_jd.size(), 0); //All jobs
}

TEST(BtTestSuit, ComplexCooperativeTest){
	std::map<const std::string, std::vector<std::tuple<const std::string, tf2::Vector3, std::vector<moveit_task_constructor_msgs::ExecuteTaskSolutionGoal>>>> task_but_different;
    boost::circular_buffer<std::pair<std::string, job_data>> cb_jd(3);
    job_data job1, job2, job3;
    job1.jobs_ = {tf2::Transform(tf2::Quaternion(0,0,0,1),tf2::Vector3(0,1.5f,0)), tf2::Transform(tf2::Quaternion(0,0,0,1),tf2::Vector3(0,2,0)), tf2::Transform(tf2::Quaternion(0,0,0,1),tf2::Vector3(0,2.5f,0)), tf2::Transform(tf2::Quaternion(0,0,0,1),tf2::Vector3(0,3,0))};
    job2.jobs_ = {tf2::Transform(tf2::Quaternion(0,0,0,1),tf2::Vector3(0,0,0)), tf2::Transform(tf2::Quaternion(0,0,0,1),tf2::Vector3(0,0.5f,0)), tf2::Transform(tf2::Quaternion(0,0,0,1),tf2::Vector3(0,1,0)), tf2::Transform(tf2::Quaternion(0,0,0,1),tf2::Vector3(0,1.5f,0))};
    job3.jobs_ = {tf2::Transform(tf2::Quaternion(0,0,0,1),tf2::Vector3(0,1.5f,0)), tf2::Transform(tf2::Quaternion(0,0,0,1),tf2::Vector3(0,2,0)), tf2::Transform(tf2::Quaternion(0,0,0,1),tf2::Vector3(0,2.5f,0)), tf2::Transform(tf2::Quaternion(0,0,0,1),tf2::Vector3(0,3,0))};


    std::vector<std::pair<std::string,tf2::Vector3>> boxes = {{"box1", tf2::Vector3(0,0,0)}, {"box2", tf2::Vector3(2,2,0)}};
    cb_jd.push_back({"panda_arm1", job3});
    cb_jd.push_back({"panda_arm1", job1});
    cb_jd.push_back({"panda_arm2", job2});

    //ASSERT_FALSE(boxes[0].second == cb_jd[0].second.jobs_[0].getOrigin());


    bool jobs_remaining = true;
    while (jobs_remaining && !cb_jd.empty() && !boxes.empty()) {
        bool moved = false;
        for (auto it = cb_jd.begin(); it != cb_jd.end();) {
            auto& temp = *it;
            bool ready_to_move = true;
            for (auto& box : boxes) {
                if (temp.second.jobs_.front().getOrigin() == box.second) {
                    for (int k = 1; k < temp.second.jobs_.size(); k++) {
                        box.second = temp.second.jobs_[k].getOrigin();
                    }
                    if (ready_to_move) {
                        moved = true;
                        it = cb_jd.erase(it);
                        break;
                    }
                } else {
                    ready_to_move = false;
                }
            }
            if (!ready_to_move) {
                ++it;
            }
        }
        jobs_remaining = moved;
    }

    ASSERT_EQ(cb_jd.size(), 1); // All jobs
    boxes.clear();
    cb_jd.push_back({"panda_arm1", job3});
    cb_jd.push_back({"panda_arm1", job1});
    cb_jd.push_back({"panda_arm2", job2});

    jobs_remaining = true;
    while (jobs_remaining && !cb_jd.empty() && !boxes.empty()) {
        bool moved = false;
        for (auto it = cb_jd.begin(); it != cb_jd.end();) {
            auto& temp = *it;
            bool ready_to_move = true;
            for (auto& box : boxes) {
                if (temp.second.jobs_.front().getOrigin() == box.second) {
                    for (int k = 1; k < temp.second.jobs_.size(); k++) {
                        box.second = temp.second.jobs_[k].getOrigin();
                    }
                    if (ready_to_move) {
                        moved = true;
                        it = cb_jd.erase(it);
                        break;
                    }
                } else {
                    ready_to_move = false;
                }
            }
            if (!ready_to_move) {
                ++it;
            }
        }
        jobs_remaining = moved;
    }

    ASSERT_EQ(cb_jd.size(), 3); // All jobs
}




int main(int argc, char **argv){
    testing::InitGoogleTest(&argc, argv);
    ros::init(argc, argv, "bt-test");
    ros::NodeHandle nh;
    return RUN_ALL_TESTS();
}