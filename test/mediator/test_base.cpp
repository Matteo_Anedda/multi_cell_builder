#include <ros/ros.h>
#include <behaviortree_cpp_v3/behavior_tree.h>
#include <behaviortree_cpp_v3/tree_node.h>
#include <behaviortree_cpp_v3/bt_factory.h>
#include <behaviortree_cpp_v3/loggers/bt_zmq_publisher.h>
#include <stdint.h>

#include "mediator/base_calculation_mediator.h"
#include "reader/abstract_param_reader.h"
#include "bt/execution.h"
#include "bt/position_condition.h"
#include "bt/parallel_robot.h"

#include <boost/circular_buffer.hpp>

TEST(BaseTestSuit, workcellCompositionLogicTest1){
    std::map<const std::string, std::vector<object_data>> inputMap = {
        {"key3", {
            {"", tf2::Transform(tf2::Quaternion(0,0,0,1), tf2::Vector3(10,11, 12)), tf2::Vector3(0,0,0)},
            {"", tf2::Transform(tf2::Quaternion(0,0,0,1), tf2::Vector3(13,14, 15)), tf2::Vector3(0,0,0)},
            {"", tf2::Transform(tf2::Quaternion(0,0,0,1), tf2::Vector3(16,17, 18)), tf2::Vector3(0,0,0)},
            }}, 
        {"key2", {
            {"", tf2::Transform(tf2::Quaternion(0,0,0,1), tf2::Vector3(19, 20, 21)), tf2::Vector3(0,0,0)}, 
            {"", tf2::Transform(tf2::Quaternion(0,0,0,1), tf2::Vector3(22, 23, 24)), tf2::Vector3(0,0,0)},
            {"", tf2::Transform(tf2::Quaternion(0,0,0,1), tf2::Vector3(16, 17, 18)), tf2::Vector3(0,0,0)}
            }}
    };

    std::vector<std::vector<std::string>> outputVec = BaseCalculationMediator::compareMapEntries(inputMap);
    ASSERT_EQ(outputVec.size(), 1);
}

TEST(BaseTestSuit, workcellCompositionLogicTest2){
    std::map<const std::string, std::vector<object_data>> inputMap = {
        {"key1", {
            {"", tf2::Transform(tf2::Quaternion(0,0,0,1), tf2::Vector3(9,99, 999)), tf2::Vector3(0,0,0)},
            {"", tf2::Transform(tf2::Quaternion(0,0,0,1), tf2::Vector3(1,11, 111)), tf2::Vector3(0,0,0)},
            {"", tf2::Transform(tf2::Quaternion(0,0,0,1), tf2::Vector3(2,22, 222)), tf2::Vector3(0,0,0)},
            }}, 
        {"key3", {
            {"", tf2::Transform(tf2::Quaternion(0,0,0,1), tf2::Vector3(10,11, 12)), tf2::Vector3(0,0,0)},
            {"", tf2::Transform(tf2::Quaternion(0,0,0,1), tf2::Vector3(13,14, 15)), tf2::Vector3(0,0,0)},
            {"", tf2::Transform(tf2::Quaternion(0,0,0,1), tf2::Vector3(16,17, 18)), tf2::Vector3(0,0,0)},
            }}, 
        {"key2", {
            {"", tf2::Transform(tf2::Quaternion(0,0,0,1), tf2::Vector3(19, 20, 21)), tf2::Vector3(0,0,0)}, 
            {"", tf2::Transform(tf2::Quaternion(0,0,0,1), tf2::Vector3(22, 23, 24)), tf2::Vector3(0,0,0)},
            {"", tf2::Transform(tf2::Quaternion(0,0,0,1), tf2::Vector3(16, 17, 18)), tf2::Vector3(0,0,0)}
            }}
    };

    std::vector<std::vector<std::string>> outputVec = BaseCalculationMediator::compareMapEntries(inputMap);
    ASSERT_EQ(outputVec.size(), 2);
}

TEST(BaseTestSuit, workcellCompositionLogicTest3){
    std::map<const std::string, std::vector<object_data>> inputMap = {
        {"key1", {
            {"", tf2::Transform(tf2::Quaternion(0,0,0,1), tf2::Vector3(9,99, 999)), tf2::Vector3(0,0,0)},
            {"", tf2::Transform(tf2::Quaternion(0,0,0,1), tf2::Vector3(1,11, 111)), tf2::Vector3(0,0,0)},
            {"", tf2::Transform(tf2::Quaternion(0,0,0,1), tf2::Vector3(2,22, 222)), tf2::Vector3(0,0,0)},
            }}, 
        {"key3", {
            {"", tf2::Transform(tf2::Quaternion(0,0,0,1), tf2::Vector3(10,11, 12)), tf2::Vector3(0,0,0)},
            {"", tf2::Transform(tf2::Quaternion(0,0,0,1), tf2::Vector3(13,14, 15)), tf2::Vector3(0,0,0)},
            {"", tf2::Transform(tf2::Quaternion(0,0,0,1), tf2::Vector3(16,17, 18)), tf2::Vector3(0,0,0)},
            }}, 
        {"key3", {
            {"", tf2::Transform(tf2::Quaternion(0,0,0,1), tf2::Vector3(10,11, 12)), tf2::Vector3(0,0,0)},
            {"", tf2::Transform(tf2::Quaternion(0,0,0,1), tf2::Vector3(1,11, 111)), tf2::Vector3(0,0,0)},
            {"", tf2::Transform(tf2::Quaternion(0,0,0,1), tf2::Vector3(2,22, 222)), tf2::Vector3(0,0,0)},
            }}, 
        {"key2", {
            {"", tf2::Transform(tf2::Quaternion(0,0,0,1), tf2::Vector3(19, 20, 21)), tf2::Vector3(0,0,0)}, 
            {"", tf2::Transform(tf2::Quaternion(0,0,0,1), tf2::Vector3(22, 23, 24)), tf2::Vector3(0,0,0)},
            {"", tf2::Transform(tf2::Quaternion(0,0,0,1), tf2::Vector3(16, 17, 18)), tf2::Vector3(0,0,0)}
            }}
    };

    std::vector<std::vector<std::string>> outputVec = BaseCalculationMediator::compareMapEntries(inputMap);
    ASSERT_EQ(outputVec.size(), 2);
}

TEST(BaseTestSuit, workcellCompositionLogicTest4){
    std::map<const std::string, std::vector<object_data>> inputMap = {
        {"key1", {
            {"", tf2::Transform(tf2::Quaternion(0,0,0,1), tf2::Vector3(10,11, 12)), tf2::Vector3(0,0,0)},
            {"", tf2::Transform(tf2::Quaternion(0,0,0,1), tf2::Vector3(1,11, 111)), tf2::Vector3(0,0,0)},
            {"", tf2::Transform(tf2::Quaternion(0,0,0,1), tf2::Vector3(2,22, 222)), tf2::Vector3(0,0,0)},
            }}, 
        {"key3", {
            {"", tf2::Transform(tf2::Quaternion(0,0,0,1), tf2::Vector3(10,11, 12)), tf2::Vector3(0,0,0)},
            {"", tf2::Transform(tf2::Quaternion(0,0,0,1), tf2::Vector3(13,14, 15)), tf2::Vector3(0,0,0)},
            {"", tf2::Transform(tf2::Quaternion(0,0,0,1), tf2::Vector3(16,17, 18)), tf2::Vector3(0,0,0)},
            }}, 
        {"key4", {
            {"", tf2::Transform(tf2::Quaternion(0,0,0,1), tf2::Vector3(10,11, 12)), tf2::Vector3(0,0,0)},
            {"", tf2::Transform(tf2::Quaternion(0,0,0,1), tf2::Vector3(1,11, 111)), tf2::Vector3(0,0,0)},
            {"", tf2::Transform(tf2::Quaternion(0,0,0,1), tf2::Vector3(2,22, 222)), tf2::Vector3(0,0,0)},
            }}, 
        {"key2", {
            {"", tf2::Transform(tf2::Quaternion(0,0,0,1), tf2::Vector3(19, 20, 21)), tf2::Vector3(0,0,0)}, 
            {"", tf2::Transform(tf2::Quaternion(0,0,0,1), tf2::Vector3(22, 23, 24)), tf2::Vector3(0,0,0)},
            {"", tf2::Transform(tf2::Quaternion(0,0,0,1), tf2::Vector3(2, 22, 222)), tf2::Vector3(0,0,0)}
            }}
    };

    

    std::vector<std::vector<std::string>> outputVec = BaseCalculationMediator::compareMapEntries(inputMap);
    ASSERT_EQ(outputVec.size(), 1);
}

TEST(BaseTestSuit, workcellSolutionTest){
    /* Workcell Logic
    A Solution should contain all possible robots, if in a cluster or not, so its a concatenation.
    */

    std::vector<std::vector<std::string>> inputVector = {
        {"panda_arm1"}, 
        {"panda_arm2", "panda_arm3"},
    };

    // solution is a  vector of protocol data of size  inputVector.size()
    protocol p1, p2, p3, p4;
    std::map<std::string, std::vector<protocol>> solutionMap;
    p1.robots = {{"panda_arm1", tf2::Transform(), 1}, {"panda_arm2", tf2::Transform(), 1}};
    p2.robots = {{"panda_arm1", tf2::Transform(), 3}, {"panda_arm2", tf2::Transform(), 4}};
    p3.robots = {{"panda_arm3", tf2::Transform(), 3}};
    p4.robots = {{"panda_arm4", tf2::Transform(), 3}};

    solutionMap["panda_arm1panda_arm2"] = {p1,p2};


    // std::vector<std::vector<protocol>> prots(inputVector.size());
    // prots[0].push_back(p1);
    // prots[1].push_back(p2);
    // prots[2].push_back(p3);

    std::vector<std::vector<protocol>> combinations;
    std::vector<protocol> currentCombination;
    std::vector<std::string> keys;
    for (const auto& entry : solutionMap) {
        keys.push_back(entry.first);
    }
    BaseCalculationMediator::generateCombinations(solutionMap, combinations, currentCombination, keys, 0);

    //BaseCalculationMediator::flatten(nestedVector, result);
    ASSERT_EQ(combinations.size(), 2);

}


int main(int argc, char **argv){
    testing::InitGoogleTest(&argc, argv);
    ros::init(argc, argv, "base-test");
    ros::NodeHandle nh;
    return RUN_ALL_TESTS();
}