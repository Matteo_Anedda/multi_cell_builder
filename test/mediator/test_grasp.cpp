#include <ros/ros.h>
#include <behaviortree_cpp_v3/behavior_tree.h>
#include <behaviortree_cpp_v3/tree_node.h>
#include <behaviortree_cpp_v3/bt_factory.h>
#include <behaviortree_cpp_v3/loggers/bt_zmq_publisher.h>
#include <stdint.h>

#include "mediator/grasp_mediator.h"
#include "reader/abstract_param_reader.h"
#include "bt/execution.h"
#include "bt/position_condition.h"
#include "bt/parallel_robot.h"

#include <boost/circular_buffer.hpp>


TEST(GraspTestSuit, configRewrite){
    /* Rewrite Jobs
    Goal of that step is to dynamicly rewrite startconfigurations, but also the job data.
    */

    std::string str = "{ 'id': 'blue1',  'type': 'BOX', 'pos': { 'x': 0.129998, 'y': 0.059998, 'z': 0.9355 },'size': { 'length': 0.0318, 'width': 0.0636, 'height': 0.091},'orientation': { 'x': 0, 'y': 0, 'z': 0.382683, 'w': 0.92388},'color': { 'b': 1 } }, #modified";

    try {
        // Parse the YAML-like string
        tf2::Transform tf;
        YAML::Node node = YAML::Load(str);

        // Extract positional and rotational information
        tf.getOrigin().setX((node["pos"]["x"]) ? node["pos"]["x"].as<double>() : 0);
        tf.getOrigin().setY((node["pos"]["y"]) ? node["pos"]["y"].as<double>() : 0);
        tf.getOrigin().setZ((node["pos"]["z"]) ? node["pos"]["z"].as<double>() : 0);

        ASSERT_NEAR(tf.getOrigin().distance2(tf2::Vector3(0.129998, 0.059998, 0.9355)), 0, 1e-6);

        // Print the extracted information
    } catch (const YAML::Exception& e) {
        std::cerr << "Error parsing YAML: " << e.what() << std::endl;
    }


}

int main(int argc, char **argv){
    testing::InitGoogleTest(&argc, argv);
    ros::init(argc, argv, "grasp-test");
    ros::NodeHandle nh;
    return RUN_ALL_TESTS();
}