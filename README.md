# multi_cell_builder 
multi_cell_builder is an approach that combines both the work of gb-auerswald and the earlier work of accessibility into a package that allows dynamic changes to the startup configuration and parallel execution.
Submodule of ['dynamc_modular_workcells'](git@git-st.inf.tu-dresden.de:Matteo_Anedda/dynamic_modular_workcells.git "GitLab")

## workflow
### Positioning
The .launch file of the "Positioning" part should contain a reachability map (/maps), the protobuf (/resources) and a task description (/descriptions), based on which all robot positions are calculated and stored in the /results folder. Each result is named after its timestamp, including additional .launch files and configuration files for later use.

```bash
roslaunch multi_cell_builder base_calculation_approach.launch
```
### Startconfiguration
The .launch files including the configurations are stored in the specific result folder, one for each robot that overwrites the result scene model. All overwritten box positions were marked with a comment (#modified robot->name()).

### Execution
The result folder also contains an execute_....launch file associated with the empty /jobs folder in this hierarchy. To execute a job, create the dummy.launch file in this /jobs folder following this scheme.

```bash
{ 'tasks': 
  {'groups' : [
    { 'name': 'panda_arm1', 'jobs':[[
        { 'pos': { 'x':  ,'y': ,'z':  }, 'orientation': { 'w':  } },
        { 'pos': { 'x':  ,'y': ,'z':  }, 'orientation': { 'w':  } }
      ]]},
    { 'name': 'panda_arm2', 'jobs':[[
        { 'pos': { 'x':  ,'y': ,'z':  }, 'orientation': { 'w':  } },
        { 'pos': { 'x':  ,'y': ,'z':  }, 'orientation': { 'w':  } }
      ]]}
    ]
  }
}
```

Make sure that the first position in a job matches a box position in the timestamp.yaml file of that folder. A simple example would be the /jobs/dummy.yaml file, which is not associated with any result.


## Current state
The following list shows open and closed ToDos.<br/>

:ballot_box_with_check: present a faster work-space execution <br/>
:ballot_box_with_check: generate more solutions for 1 protobuf reference file <br/>
:ballot_box_with_check: present solution for general robots <br/>
:ballot_box_with_check: elaborate the moveit task constructor <br/>
:ballot_box_with_check: present more general task files <br/>
:black_square_button: include cosmetic values <br/>
:black_square_button: resolve endless-loop bug <br/>
:ballot_box_with_check: passing yaml in xacro (franca_description) <br/>
:ballot_box_with_check: parallel task execution in container <br/>
:ballot_box_with_check: fix acm <br/>
:ballot_box_with_check: include ferdinands work (about to be done)<br/>