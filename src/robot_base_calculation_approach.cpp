#include <xmlrpcpp/XmlRpc.h>

#include <filesystem>

#include "reader/abstract_param_reader.h"
#include "reader/robot_reader.h"
#include "reader/wing_reader.h"
#include "bridge/abstract_base.h"
#include "bridge/abstract_base_implementation.h"
#include "bridge/simple_base.h"
#include "bridge/simple_base_implementation.h"
#include "robot/abstract_robot.h"
#include "robot/ceti_robot.h"
#include "robot/decorators/abstract_robot_decorator.h"
#include "robot/decorators/panda_decorator.h"
#include "robot_element/abstract_robot_element.h"
#include "robot_element/decorators/abstract_robot_element_decorator.h"
#include "robot_element/decorators/log_decorator.h"
#include "robot_element/observers/panel.h"
#include "robot_element/observers/rviz_panel.h"
#include "mediator/abstract_mediator.h"
#include "mediator/base_calculation_mediator.h"


int main(int argc, char **argv){
    ros::init(argc, argv, "robot_base_calculation_apprioach");
    std::shared_ptr<ros::NodeHandle> n(new ros::NodeHandle);

    std::shared_ptr<AbstractBase> simple_base = std::make_shared<SimpleBase>(n);
    std::shared_ptr<AbstractBaseImplementation> simple_base_implementation = std::make_shared<SimpleBaseImplementation>();
    std::shared_ptr<BaseCalculationMediator> mediator = std::make_shared<BaseCalculationMediator>(n);

    simple_base->setImplementation(simple_base_implementation);

    simple_base->baseCalculation();

    auto rd = mediator->robotReader()->robotData();
    for (int i = 0; i < rd.size() ;i++){
        std::unique_ptr<AbstractRobot> ceti = std::make_unique<CetiRobot>(rd[i].name_, rd[i].pose_, rd[i].size_);
        std::unique_ptr<PandaDecorator> ceti_panda = std::make_unique<PandaDecorator>(std::move(ceti));
        mediator->connectRobots(std::move(ceti_panda));
    }

    mediator->setResultVector(simple_base->result());
    //mediator->set_dirname(filename);

    mediator->setPanel();
    
    mediator->mediate();
    return 0;
}