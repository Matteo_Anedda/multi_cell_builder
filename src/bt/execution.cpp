#include "bt/execution.h"

Execution::Execution(const std::string& name, const BT::NodeConfiguration& config)
    : BT::StatefulActionNode(name, config) {}

BT::NodeStatus Execution::onStart(){
    ROS_INFO("PING");
    if (it_ != ets_.solution.sub_trajectory.end()){
        std::pair<moveit_msgs::RobotTrajectory, moveit_msgs::PlanningScene> pair(it_->trajectory, it_->scene_diff);
        executions_->insert_or_assign(mr_reference_->name(), pair);
        it_++;
        return BT::NodeStatus::RUNNING;
    } else {
        return BT::NodeStatus::SUCCESS;
    }
 }

BT::NodeStatus Execution::onRunning(){
    if (it_ != ets_.solution.sub_trajectory.end()){
        std::pair<moveit_msgs::RobotTrajectory, moveit_msgs::PlanningScene> pair(it_->trajectory, it_->scene_diff);
        executions_->insert_or_assign(mr_reference_->name(), pair);
        it_++;
        return BT::NodeStatus::RUNNING;
    } else {
        return BT::NodeStatus::SUCCESS;
    }
}
void Execution::onHalted(){}

void Execution::init(std::map<std::string, std::pair<moveit_msgs::RobotTrajectory, moveit_msgs::PlanningScene>>* executions , AbstractRobotDecorator* mr_reference, moveit_task_constructor_msgs::ExecuteTaskSolutionGoal& ets) {
    mr_reference_= mr_reference;
    ets_ = ets;
    it_ = ets_.solution.sub_trajectory.begin();
    executions_ = executions;
}