#include "bt/position_condition.h"

PositionCondition::PositionCondition(const std::string& name, const BT::NodeConfiguration& config)
    : BT::ConditionNode(name, config){}

void PositionCondition::init(const std::string& obj_name, tf2::Vector3& start_pos, moveit::planning_interface::PlanningSceneInterface* psi){
    obj_name_ = obj_name;
    psi_ = psi;
    start_pos_ = start_pos;
}

BT::NodeStatus PositionCondition::tick(){
    try {
        auto ref_pose = psi_->getObjects().at(obj_name_);
        if(tf2::tf2Distance2(start_pos_, tf2::Vector3(ref_pose.pose.position.x, ref_pose.pose.position.y, ref_pose.pose.position.z )) == 0){
            return BT::NodeStatus::SUCCESS;
        } else {
            return BT::NodeStatus::FAILURE;
        }
    } catch(std::out_of_range& oor) {
        ROS_INFO("Position to check not found");
        return BT::NodeStatus::FAILURE;
    }
}
