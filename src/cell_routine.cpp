#include <xmlrpcpp/XmlRpc.h>

#include "mediator/moveit_mediator.h"
#include "mediator/abstract_mediator.h"
#include "robot/decorators/panda_decorator.h"

#include "reader/abstract_param_reader.h"
#include "reader/robot_reader.h"
#include "reader/wing_reader.h"
#include "reader/cuboid_reader.h"

#include <moveit_grasps/two_finger_grasp_generator.h>
#include <moveit_grasps/two_finger_grasp_data.h>
#include <moveit_grasps/two_finger_grasp_filter.h>
#include <moveit_grasps/grasp_planner.h>



int main(int argc, char **argv){
    ros::init(argc, argv, "cell_routine");
    std::shared_ptr<ros::NodeHandle> n(new ros::NodeHandle);

    ros::AsyncSpinner spinner(1);
    spinner.start();

    std::shared_ptr<MoveitMediator> mediator = std::make_shared<MoveitMediator>(n);

    auto rd = mediator->robotReader()->robotData();
    for (int i = 0; i < rd.size() ;i++){
        std::unique_ptr<AbstractRobot> ceti = std::make_unique<CetiRobot>(rd[i].name_, rd[i].pose_, rd[i].size_);
        std::unique_ptr<PandaDecorator> ceti_panda = std::make_unique<PandaDecorator>(std::move(ceti));
        mediator->connectRobots(std::move(ceti_panda));
    }

    //mediator->set_dirname(filename);    
    mediator->mediate();

    while (ros::ok()){
        ros::spinOnce();
    }

    /*
    for (int i = 0; i < rd.size() ;i++){
        std::unique_ptr<Abstract_robot> ceti = std::make_unique<Ceti_robot>(rd[i].name_, rd[i].pose_, rd[i].size_);
        std::unique_ptr<Panda_decorator> ceti_panda = std::make_unique<Panda_decorator>(std::move(ceti));
        mediator->connect_robots(std::move(ceti_panda));
    }

    mediator->set_result_vector(simple_base->result());
    //mediator->set_dirname(filename);

    mediator->set_panel();
    
    mediator->mediate();
    */
}