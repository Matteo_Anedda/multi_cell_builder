#include "mediator/grasp_mediator.h"

GraspMediator::GraspMediator(std::shared_ptr<ros::NodeHandle> const& nh) 
    : AbstractMediator(nh)
    , planning_scene_monitor_(std::make_shared<planning_scene_monitor::PlanningSceneMonitor>("robot_description"))
    , grasp_pipeline_demo_(std::make_unique<moveit_grasps_demo::GraspPipelineDemo>())
    {
        nh->getParam("/referenceRobot", referenceRobot_); 
        nh->getParam("/resultPath", resultPath_);
        
    }

void GraspMediator::connectRobots(std::unique_ptr<AbstractRobotDecorator> robot) {
	ROS_INFO("connecting %s...", robot->name().c_str());
	robots_.insert_or_assign(robot->name(), std::move(robot)); 
}

void GraspMediator::setPanel(){
	auto wd = wing_reader_->wingData();

	for (const auto& s_r : robots_){
        try{
            auto ceti_bot = dynamic_cast<CetiRobot*>(s_r.second->next());
            ceti_bot->setObserverMask(static_cast<wing_config>(wd.at(s_r.first).second));
        } catch(const std::out_of_range& oor) {
            ROS_INFO("No mask data for %s", s_r.first.c_str());
        }
    }

	for (const auto& s_r : robots_){
        auto ceti_bot = dynamic_cast<CetiRobot*>(s_r.second->next());
        std::vector<std::unique_ptr<AbstractRobotElementDecorator>> panels(3);
            
        for (std::size_t j = 0; j < ceti_bot->observerMask().size(); j++){
            if (ceti_bot->observerMask()[j]){
                try{
                    tf2::Transform relative_tf = ceti_bot->tf().inverse() * wd.at(ceti_bot->name()).first[j].pose_;

                    std::unique_ptr<AbstractRobotElement> panel = std::make_unique<MoveitPanel>(wd.at(s_r.first).first[j].name_, relative_tf, wd.at(s_r.first).first[j].size_);
                    std::unique_ptr<AbstractRobotElementDecorator> log = std::make_unique<LogDecorator>(std::move(panel));
                    panels[j] = std::move(log);
                } catch (std::out_of_range &oor){
                    ROS_INFO("OOR in set_panel");
                }
            }
        }
        ceti_bot->setObservers(panels);
    }

	for (const auto& s_r : robots_){
		s_r.second->notify();
	}
}

void GraspMediator::mediate() {
    setPanel();
    //if(autoMapping_)mapReconfigure();
    ros::Duration(2).sleep();
    std::vector<Cuboid> relevant_box;
    std::vector<Cuboid> relevant_obstacle;

    Cuboid rob(referenceRobot_);
    rob.Pose.position.x = robots_.at(referenceRobot_)->tf().getOrigin().getX();
    rob.Pose.position.y = robots_.at(referenceRobot_)->tf().getOrigin().getY();
    rob.Pose.position.z = robots_.at(referenceRobot_)->tf().getOrigin().getZ();
    rob.Pose.orientation.x = robots_.at(referenceRobot_)->tf().getRotation().getX();
    rob.Pose.orientation.y = robots_.at(referenceRobot_)->tf().getRotation().getY();
    rob.Pose.orientation.z = robots_.at(referenceRobot_)->tf().getRotation().getZ();
    rob.Pose.orientation.w = robots_.at(referenceRobot_)->tf().getRotation().getW();
    rob.x_depth = robots_.at(referenceRobot_)->size().getX();
    rob.y_width = robots_.at(referenceRobot_)->size().getY();
    rob.z_heigth = robots_.at(referenceRobot_)->tf().getOrigin().getZ()*2;
    relevant_obstacle.push_back(rob);

	auto ceti_bot = dynamic_cast<CetiRobot*>(robots_.at(referenceRobot_)->next());

	for (std::size_t k = 0; k < ceti_bot->observerMask().size(); k++){
		if(ceti_bot->observerMask()[k]){
            Cuboid wing(ceti_bot->observers()[k]->name());
            wing.Pose.position.x = ceti_bot->observers()[k]->worldTf().getOrigin().getX();
            wing.Pose.position.y = ceti_bot->observers()[k]->worldTf().getOrigin().getY();
            wing.Pose.position.z = ceti_bot->observers()[k]->worldTf().getOrigin().getZ() - ceti_bot->observers()[k]->size().getZ()/2;
            wing.Pose.orientation.x = ceti_bot->observers()[k]->worldTf().getRotation().getX();
            wing.Pose.orientation.y = ceti_bot->observers()[k]->worldTf().getRotation().getY();
            wing.Pose.orientation.z = ceti_bot->observers()[k]->worldTf().getRotation().getZ();
            wing.Pose.orientation.w = ceti_bot->observers()[k]->worldTf().getRotation().getW();
            wing.x_depth = ceti_bot->observers()[k]->size().getX();
            wing.y_width = ceti_bot->observers()[k]->size().getY();
            wing.z_heigth = ceti_bot->observers()[k]->size().getZ();
            relevant_obstacle.push_back(wing);
        }
    }

    for (auto& ro :relevant_obstacle){
        grasp_pipeline_demo_->sceneObstacles().push_back(ro);
    } 

    for (const auto& b : cuboid_reader_->cuboidBox()){
        std::string temp;
        std::bitset<3> temp2 = ceti_bot->observerMask();
        tf2::Transform pos(tf2::Quaternion(b.Pose.orientation.x, b.Pose.orientation.y, b.Pose.orientation.z, b.Pose.orientation.w), tf2::Vector3(b.Pose.position.x, b.Pose.position.y, b.Pose.position.z));
        if (robots_.at(referenceRobot_)->checkSingleObjectCollision(pos, temp, temp2)){
            grasp_pipeline_demo_->sceneBoxes().push_back(b);
            relevant_boxes_.insert_or_assign(b.Name, b);
        }
    }

    for (const auto& b : cuboid_reader_->cuboidObstacle()){
        std::string temp;
        std::bitset<3> temp2 = ceti_bot->observerMask();
        tf2::Transform pos(tf2::Quaternion(b.Pose.orientation.x, b.Pose.orientation.y, b.Pose.orientation.z, b.Pose.orientation.w), tf2::Vector3(b.Pose.position.x, b.Pose.position.y, b.Pose.position.z));
        if (robots_.at(referenceRobot_)->checkSingleObjectCollision(pos, temp, temp2)){
            grasp_pipeline_demo_->sceneObstacles().push_back(b);
        }
    }

    srand(ros::Time::now().toSec());
    grasp_pipeline_demo_->publishConfigScene();
    grasp_pipeline_demo_->generateGraspMap();
    grasp_pipeline_demo_->getVoxelizedEnv();
    grasp_pipeline_demo_->computeConfig();
    rewriteResult();
}

void GraspMediator::rewriteResult(){
    std::string inputPath = ros::package::getPath("multi_cell_builder") + "/results/" + resultPath_;
    std::ifstream input_file(inputPath);
    if (!input_file) {
        ROS_INFO("file not found");
        ros::shutdown();
    }

    std::ofstream output_file("input.txt.tmp");
    if (!output_file) {
        ROS_INFO("temp file not open");
        ros::shutdown();
    }

    for (auto& c: grasp_pipeline_demo_->sortConfig().objects){
        ROS_INFO("%s", c.first.c_str());
    }


    std::string line;
    while (std::getline(input_file, line)) {
        for (const auto& c: grasp_pipeline_demo_->sortConfig().objects){
            std::size_t found = line.find(c.first);
            if (found != std::string::npos) {
                std::stringstream ss;
                ss << "{ 'id': '"<< c.first<<"',  'type': 'BOX', 'pos': { 'x': "<< c.second->Pose.position.x <<", 'y': " << c.second->Pose.position.y <<", 'z': " << c.second->Pose.position.z <<" },'size': { 'length': "<< relevant_boxes_.at(c.first).x_depth <<", 'width': "<< relevant_boxes_.at(c.first).y_width <<", 'height': " << relevant_boxes_.at(c.first).z_heigth << "},'orientation': { 'x': "<< c.second->Pose.orientation.x <<", 'y': "<< c.second->Pose.orientation.y<<", 'z': "<< c.second->Pose.orientation.z<<", 'w': " << c.second->Pose.orientation.w<<"},'color': { 'b': 1 } }, #modified "<< referenceRobot_;
                line = ss.str();
            }
        }
        output_file << line << "\n";
    }

    input_file.close();
    output_file.close();

    // replace input file with temporary file
    if (std::rename("input.txt.tmp", inputPath.c_str()) != 0) {
        std::cerr << "Failed to replace input file\n";
    }
}   