#include "mediator/base_calculation_mediator.h"


BaseCalculationMediator::BaseCalculationMediator(std::shared_ptr<ros::NodeHandle> const& nh) 
    : AbstractMediator(nh)
    , pub_(std::make_unique<ros::Publisher>(nh->advertise< visualization_msgs::MarkerArray >("visualization_marker_array", 1))){
        nh_->getParam("/resource_name", filename_);

        // create new folder
        if(std::filesystem::exists(ros::package::getPath("multi_cell_builder") + "/results/" +  filename_)) std::filesystem::remove_all(ros::package::getPath("multi_cell_builder") + "/results/" +  filename_);
        std::filesystem::create_directory(ros::package::getPath("multi_cell_builder") + "/results/" +  filename_);
    };

void BaseCalculationMediator::generateGrounds(const tf2::Vector3 origin, const float diameter, float resolution){
  for(const auto& s_r : robots_){
    std::vector<pcl::PointXYZ> ground_plane; 
    for (float x = origin.getX() - diameter * 1.5; x <= origin.getX() + diameter * 1.5; x += resolution){
        for (float y = origin.getY() - diameter * 1.5; y <= origin.getY() + diameter * 1.5; y += resolution){
        ground_plane.push_back(pcl::PointXYZ(x,y, s_r.second->tf().getOrigin().getZ() - 0.0025f));
        }
    }
    grounds_.insert(std::pair<const std::string, std::vector<pcl::PointXYZ>>(s_r.first, ground_plane));
  }
}

void BaseCalculationMediator::connectRobots(std::unique_ptr<AbstractRobotDecorator> robot) {
    ROS_INFO("connecting %s...", robot->name().c_str());
    robots_.insert_or_assign(robot->name(), std::move(robot)); 
}

void BaseCalculationMediator::setPanel(){
    auto wd = wing_reader_->wingData();

    for (const auto& s_r : robots_){
        try{
            auto ceti_bot = dynamic_cast<CetiRobot*>(s_r.second->next());
            ceti_bot->setObserverMask(static_cast<wing_config>(wd.at(s_r.first).second));
        } catch(const std::out_of_range& oor) {
            ROS_INFO("No mask data for %s", s_r.first.c_str());
        }
    }
    
    for (const auto& s_r : robots_){
        auto ceti_bot = dynamic_cast<CetiRobot*>(s_r.second->next());
        std::vector<std::unique_ptr<AbstractRobotElementDecorator>> panels(3);
            
        for (std::size_t j = 0; j < ceti_bot->observerMask().size(); j++){
            if (ceti_bot->observerMask()[j]){
                try{
                    tf2::Transform relative_tf = ceti_bot->tf().inverse() * wd.at(ceti_bot->name()).first[j].pose_;

                    std::unique_ptr<AbstractRobotElement> panel = std::make_unique<RvizPanel>(wd.at(s_r.first).first[j].name_, relative_tf, wd.at(s_r.first).first[j].size_);
                    std::unique_ptr<AbstractRobotElementDecorator> log = std::make_unique<LogDecorator>(std::move(panel));
                    panels[j] = std::move(log);
                } catch (std::out_of_range &oor){
                    ROS_INFO("OOR in set_panel");
                }
            }
        }
        ceti_bot->setObservers(panels);
    }
}

bool BaseCalculationMediator::checkWorkcellCollision(const std::vector<std::string>& ws, std::vector<std::bitset<3>>& panel_mask, bool& workload){
    std::vector<tf2::Transform> combined_ts;
    for(const auto& robot : ws){
        for (const auto& vec : task_space_reader_->dropOffData().at(robot)){
            if (std::find(combined_ts.begin(), combined_ts.end(), vec.pose_) == combined_ts.end()) {
                combined_ts.push_back(vec.pose_);
            }
        }
    }

        


    workload = true;
    std::map<std::string, int> workload_map;

    std::string some_strings[ws.size()];
    for (int i = 0; i < some_strings->size(); i++) some_strings[i] = std::string("");
    visualization_msgs::MarkerArray ma;
    for  (int x = 0;  x < combined_ts.size(); x++){
        visualization_msgs::Marker marker;
        marker.header.frame_id = "map";
        marker.header.stamp = ros::Time();
        marker.ns = "combined TS";
        marker.id = x;
        marker.type = visualization_msgs::Marker::CUBE;
        marker.action = visualization_msgs::Marker::ADD;
        marker.pose.position.x = combined_ts[x].getOrigin().getX();
        marker.pose.position.y = combined_ts[x].getOrigin().getY();
        marker.pose.position.z = combined_ts[x].getOrigin().getZ();
        marker.pose.orientation.x = combined_ts[x].getRotation().getX();
        marker.pose.orientation.y = combined_ts[x].getRotation().getY();
        marker.pose.orientation.z = combined_ts[x].getRotation().getZ();
        marker.pose.orientation.w = combined_ts[x].getRotation().getW();
        marker.color.r = 1;
        marker.color.g = 0;
        marker.color.b = 0;
        marker.color.a = 1.0;
        marker.scale.x = 0.1f;
        marker.scale.y = 0.1f;
        marker.scale.z = 0.1f;
        
        // check if the targets stand at least at one robot

        bool standings = false;
        for (int i = 0; i < ws.size(); i++){
            bool standing = true;
            CetiRobot* ceti1;
            try{
                ceti1 = dynamic_cast<CetiRobot*>(robots_.at(ws[i])->next());
            } catch (std::out_of_range& oor){}
            tf2::Transform tr1 =  combined_ts[x] * tf2::Transform(tf2::Quaternion(0,0,0,1), tf2::Vector3(marker.scale.x*0.5f, marker.scale.y*0.5f, 0)); 
            tf2::Transform tr2 =  combined_ts[x] * tf2::Transform(tf2::Quaternion(0,0,0,1), tf2::Vector3(-marker.scale.x*0.5f, marker.scale.y*0.5f, 0)); 
            tf2::Transform tr3 =  combined_ts[x] * tf2::Transform(tf2::Quaternion(0,0,0,1), tf2::Vector3(-marker.scale.x*0.5f, -marker.scale.y*0.5f, 0));
            tf2::Transform tr4 =  combined_ts[x] * tf2::Transform(tf2::Quaternion(0,0,0,1), tf2::Vector3(marker.scale.x*0.5f, -marker.scale.y*0.5f, 0));
            standing &= ceti1->checkSingleObjectCollision(tr1, some_strings[i], panel_mask[i]);
            standing &= ceti1->checkSingleObjectCollision(tr2, some_strings[i], panel_mask[i]);
            standing &= ceti1->checkSingleObjectCollision(tr3, some_strings[i], panel_mask[i]);
            standing &= ceti1->checkSingleObjectCollision(tr4, some_strings[i], panel_mask[i]);
            standings |= standing;
        }

        if (!standings) {
            visualization_msgs::MarkerArray markerarray;
            for  (int y = x;  y < combined_ts.size(); y++){
                visualization_msgs::Marker somemarker;
                somemarker.header.frame_id = "map";
                somemarker.header.stamp = ros::Time();
                somemarker.ns = "combined TS";
                somemarker.id = y;
                somemarker.type = visualization_msgs::Marker::CUBE;
                somemarker.action = visualization_msgs::Marker::DELETE;
                somemarker.color.r = 0;
                somemarker.color.g = 0;
                somemarker.color.b = 0;
                somemarker.color.a = 0;
                markerarray.markers.push_back(somemarker);   
            }
            pub_->publish(markerarray);
            return false;
        }

        bool matches = false;
        for (int i = 0; i < ws.size(); i++){
            CetiRobot* ceti1 = dynamic_cast<CetiRobot*>(robots_.at(ws[i])->next());
            std::string str;
            if(ceti1->checkSingleObjectCollision(combined_ts[x], str, panel_mask[i])){
                try {
                    workload_map.at(str)++;
                } catch (std::out_of_range& oor) {
                    workload_map.insert(std::pair<std::string,int>(str,1));
                }
                matches |= true;

                } else {
                matches |= false;
            }
        }

        // no robot hit
        if (!matches) {            
            return false;
        } else {
            marker.color.r = 0;
            marker.color.g = 1.0;
            marker.color.b = 0;
            marker.color.a = 1;
        }
        ma.markers.push_back(marker);

    }

    // At this point, any position should have matched 
    // the wotkload map shoud be base + panel masks

    for (int i = 0; i < ws.size(); i++){
        CetiRobot* ceti1 = dynamic_cast<CetiRobot*>(robots_.at(ws[i])->next());

        for(std::size_t j = 0; j < ceti1->observers().size(); j++){
            if(ceti1->observerMask()[j] && panel_mask[i][j]){
                try{
                    workload_map.at(ceti1->observers()[j]->name());
                } catch( std::out_of_range& oor){
                    workload = false;
                }
            }
        }
    }
    if(workload) pub_->publish(ma);


    //pub_->publish(somemarker);

    return true;
}

bool BaseCalculationMediator::checkCollision(const std::string& robot, std::bitset<3>& panel_mask, bool& workload, std::vector<object_data>& ts){

    // Visualization workspace definition
    std::string str;
    std::stringstream ns;
    ns << "Targets " << robot;

    // Get Robot reference by name 
    CetiRobot* ceti1;
    try{
        ceti1 = dynamic_cast<CetiRobot*>(robots_.at(robot)->next());
    } catch (std::out_of_range& oor){}
   
    // prepare map for workload check
    std::map<std::string, int> workload_map;

    bool succ = true;

    visualization_msgs::MarkerArray ma;
    for(int i = 0; i < ts.size(); i++){
        visualization_msgs::Marker marker;
        marker.header.frame_id = "map";
        marker.header.stamp = ros::Time();
        marker.ns = ns.str();
        marker.id = i;
        marker.type = visualization_msgs::Marker::CUBE;
        marker.action = visualization_msgs::Marker::ADD;
        marker.pose.position.x = ts[i].pose_.getOrigin().getX();
        marker.pose.position.y = ts[i].pose_.getOrigin().getY();
        marker.pose.position.z = ts[i].pose_.getOrigin().getZ();
        marker.pose.orientation.x = ts[i].pose_.getRotation().getX();
        marker.pose.orientation.y = ts[i].pose_.getRotation().getY();
        marker.pose.orientation.z = ts[i].pose_.getRotation().getZ();
        marker.pose.orientation.w = ts[i].pose_.getRotation().getW();
        marker.scale.x = ts[i].size_.getX();
        marker.scale.y = ts[i].size_.getY();
        marker.scale.z = 0.1f;

        // ROS_INFO("%f, %f, %f", ts.at(robot)[i].pose_.getOrigin().getX(), ts.at(robot)[i].pose_.getOrigin().getY(), ts.at(robot)[i].pose_.getOrigin().getZ());
        // std::vector<tf2::Transform> box_bounds;
        std::string some_string;
        bool standing = true;
        tf2::Transform tr1 =  ts[i].pose_ * tf2::Transform(tf2::Quaternion(0,0,0,1), tf2::Vector3(ts[i].size_.getX()*0.5f, ts[i].size_.getY()*0.5f, 0)); 
        tf2::Transform tr2 =  ts[i].pose_ * tf2::Transform(tf2::Quaternion(0,0,0,1), tf2::Vector3(-ts[i].size_.getX()*0.5f, ts[i].size_.getY()*0.5f, 0)); 
        tf2::Transform tr3 =  ts[i].pose_ * tf2::Transform(tf2::Quaternion(0,0,0,1), tf2::Vector3(-ts[i].size_.getX()*0.5f, -ts[i].size_.getY()*0.5f, 0));
        tf2::Transform tr4 =  ts[i].pose_ * tf2::Transform(tf2::Quaternion(0,0,0,1), tf2::Vector3(ts[i].size_.getX()*0.5f, -ts[i].size_.getY()*0.5f, 0));
        standing &= ceti1->checkSingleObjectCollision(tr1, some_string, panel_mask);
        standing &= ceti1->checkSingleObjectCollision(tr2, some_string, panel_mask);
        standing &= ceti1->checkSingleObjectCollision(tr3, some_string, panel_mask);
        standing &= ceti1->checkSingleObjectCollision(tr4, some_string, panel_mask);
        if (!standing) {
            succ = false;
            return false;
        }

        // Publish Bounds

        if(ceti1->checkSingleObjectCollision(ts[i].pose_, str, panel_mask)){
            try {
                workload_map.at(str)++;
            } catch (std::out_of_range& oor) {
                ROS_INFO("Support surface not in map");
                workload_map.insert(std::pair<std::string,int>(str,1));
            }
            marker.color.r = 0;
            marker.color.g = 1.0;
            marker.color.b = 0;
            marker.color.a = 1;
        } else {
            marker.color.r = 1;
            marker.color.g = 0;
            marker.color.b = 0;
            marker.color.a = 1.0;
            succ = false;
            return false;
        }
    ma.markers.push_back(marker);
    }
    
    pub_->publish(ma);
    workload = (workload_map.size() <= panel_mask.count()+1) && succ;
    return succ;
}


void BaseCalculationMediator::mediate(){
    generateGrounds(tf2::Vector3(0,0,0), 3.0f, 0.1f);

    // estimate workcells
    auto ts = task_space_reader_->dropOffData();
    std::vector<std::vector<std::string>> wcs = compareMapEntries(ts);

    /* wcs 
        [[panda_arm1, panda_arm2], [] ...]
    */

    /*
    [{panda_arm1: [xyz, xyz], panda_arm2: [xyz, xyz]}, {} ...]
    */
    std::vector<std::map<const std::string, std::vector<tf2::Transform>>> workcells_with_bases;

    
    for(auto& workcell: wcs){
        std::map<const std::string, std::vector<tf2::Transform>> ground_projection;
        for (auto& robot: workcell){
            std::vector<tf2::Transform> ground_per_robot;
            pcl::octree::OctreePointCloudSearch< pcl::PointXYZ >cloud(0.4f);
            try{
                cloud.setInputCloud(AbstractMediator::vector2cloud(result_vector_.at(robot)));
                cloud.addPointsFromInputCloud();
            
                for(pcl::PointXYZ& p : grounds_.at(robot)){
                    double min_x, min_y, min_z, max_x, max_y, max_z;
                    cloud.getBoundingBox(min_x, min_y, min_z, max_x, max_y, max_z);
                    bool isInBox = (p.x >= min_x && p.x <= max_x) && (p.y >= min_y && p.y <= max_y) && (p.z >= min_z && p.z <= max_z);
                    if (isInBox && cloud.isVoxelOccupiedAtPoint(p)) {
                        std::vector< int > pointIdxVec;
                        if (cloud.voxelSearch(p, pointIdxVec)) ground_per_robot.push_back(tf2::Transform(tf2::Quaternion(0,0,0,1), tf2::Vector3(p.x, p.y, p.z)));
                    }
                }
            } catch (std::out_of_range& oor) {
                ROS_INFO("No result for robot");
                ros::shutdown();
            }
            ground_projection.insert(std::pair<const std::string, std::vector<tf2::Transform>>(robot,ground_per_robot));
        }
        workcells_with_bases.push_back(ground_projection);
    }

    // Debug 
    for (const auto& ws: workcells_with_bases){
        for (const auto& r : ws){
            ROS_INFO("%s with %i results", r.first, r.second.size());
        }
        ROS_INFO("------------");
    }

    // Solution map
    for (const auto& ws: workcells_with_bases){
        std::stringstream ss;
        for (const auto& r : ws){
            ss << r.first;
        }
        protocol_map_.insert_or_assign(ss.str(), std::vector<protocol>());
    }

    


    ROS_INFO("assigne result to first robot...");
    for(auto& wc: workcells_with_bases){
        /* 
        calculate pose per wc {panda_arm1: [,,,], panda_arm2: [,,,]}
        */
        calculate(wc);
    }
}


void BaseCalculationMediator::new_approximate(std::map<const std::string, std::vector<tf2::Transform>>& workcell, std::map<const std::string, std::vector<tf2::Transform>>::iterator& it, std::vector<std::string>& state, std::vector<std::bitset<3>>& panel_state){
    if (it == workcell.end()) {
        std::stringstream ss;
        protocol prot;

        for(int i = 0; i < state.size();i++){
            ss << state[i];
            std::tuple<std::string, tf2::Transform, std::bitset<3>> tuple(state[i], robots_.at(state[i])->tf(), panel_state[i]);
            prot.robots.push_back(tuple);
        }
        protocol_map_.at(ss.str()).push_back(prot);
        return;
    }



    std::vector<tf2::Transform> access_map;
    float padding = 0.0025f;

    // Choose next robot 
    CetiRobot* next_ceti =  dynamic_cast<CetiRobot*>(robots_.at(it->first)->next());


    for (int x = 0; x < state.size();x++){
        CetiRobot* ceti = dynamic_cast<CetiRobot*>(robots_.at(state[x])->next());
        ceti->notify();

        for (int i = 0; i <= 2; i++){
            for (int j = 0; j <= 2; j++){
                if(i == 0 && j == 0) {continue;}
                if(i == 2 && j == 2) {continue;}
                if(i == 0) {
                    access_map.push_back(tf2::Transform(tf2::Quaternion(0,0,0,1), tf2::Vector3(0,ceti->size().getY()*j + padding,0)));
                    access_map.push_back(tf2::Transform(tf2::Quaternion(0,0,0,1), tf2::Vector3(0,-ceti->size().getY()*j + padding,0)));
                } else if (j == 0){
                    access_map.push_back(tf2::Transform(tf2::Quaternion(0,0,0,1), tf2::Vector3(ceti->size().getX()*i + padding,0,0)));
                    access_map.push_back(tf2::Transform(tf2::Quaternion(0,0,0,1), tf2::Vector3(-ceti->size().getX()*i + padding,0,0)));
                } else {
                access_map.push_back(tf2::Transform(tf2::Quaternion(0,0,0,1), tf2::Vector3(ceti->size().getX()*i+padding,ceti->size().getY()*j +padding,0)));
                access_map.push_back(tf2::Transform(tf2::Quaternion(0,0,0,1), tf2::Vector3(-ceti->size().getX()*i+padding,ceti->size().getY()*j +padding,0)));
                access_map.push_back(tf2::Transform(tf2::Quaternion(0,0,0,1), tf2::Vector3(ceti->size().getX()*i+padding,-ceti->size().getY()*j +padding,0)));
                access_map.push_back(tf2::Transform(tf2::Quaternion(0,0,0,1), tf2::Vector3(-ceti->size().getX()*i+padding,-ceti->size().getY()*j+padding,0)));
                }

            };
        };

        for(std::size_t i = 0; i < ceti->observers().size(); i++){
            if(ceti->observerMask()[i] && panel_state[x][i]){
            access_map.push_back(tf2::Transform(tf2::Quaternion(0,0,0,1), tf2::Vector3(0.5f * ceti->size().getX() + ceti->observers()[i]->size().getX()  + 2*padding + next_ceti->size().getX()*0.5f,0,0)));
            access_map.push_back(tf2::Transform(tf2::Quaternion(0,0,0,1), tf2::Vector3(-0.5f * ceti->size().getX() - ceti->observers()[i]->size().getX() - 2*padding - next_ceti->size().getX()*0.5f,0,0)));
            access_map.push_back(tf2::Transform(tf2::Quaternion(0,0,0,1), tf2::Vector3(0,0.5f*ceti->size().getY() + ceti->observers()[i]->size().getY()  + 2*padding + next_ceti->size().getY()*0.5f ,0)));
            access_map.push_back(tf2::Transform(tf2::Quaternion(0,0,0,1), tf2::Vector3(0,-0.5f*ceti->size().getY() - ceti->observers()[i]->size().getY() - 2*padding - next_ceti->size().getY()*0.5f,0))); 
            }
        }

        for(auto& am: access_map){
            am = ceti->tf() * am;
        }
    }

    visualization_msgs::MarkerArray ma;
    visualization_msgs::Marker m;
    m.header.frame_id = "map";
    m.header.stamp = ros::Time();
    m.ns = next_ceti->name();
    m.id = 1;
    m.type = visualization_msgs::Marker::CUBE;
    m.action = visualization_msgs::Marker::ADD;
    m.pose.position.x = next_ceti->tf().getOrigin().getX();
    m.pose.position.y = next_ceti->tf().getOrigin().getY();
    m.pose.position.z = next_ceti->tf().getOrigin().getZ();
    m.pose.orientation.x = next_ceti->tf().getRotation().getX();
    m.pose.orientation.y = next_ceti->tf().getRotation().getY();
    m.pose.orientation.z = next_ceti->tf().getRotation().getZ();
    m.pose.orientation.w = next_ceti->tf().getRotation().getW();

    m.scale.x = next_ceti->size().getX();
    m.scale.y = next_ceti->size().getY();
    m.scale.z = next_ceti->tf().getOrigin().getZ()*2;
    m.color.r = 1.0;
    m.color.g = 1.0;
    m.color.b = 1.0;
    m.color.a = 1.0;

    for(int j = 0; j < next_ceti->robotRootBounds().size(); j++){
        tf2::Transform temp1 = next_ceti->tf() * next_ceti->rootTf() * next_ceti->robotRootBounds()[j];
        visualization_msgs::Marker boundsMarker;
        boundsMarker.header.frame_id = "map";
        boundsMarker.header.stamp = ros::Time();
        boundsMarker.ns = next_ceti->name() + "bounds";
        boundsMarker.id = j;
        boundsMarker.type = visualization_msgs::Marker::CUBE;
        boundsMarker.action = visualization_msgs::Marker::ADD;
        boundsMarker.pose.position.x = temp1.getOrigin().getX();
        boundsMarker.pose.position.y = temp1.getOrigin().getY();
        boundsMarker.pose.position.z = temp1.getOrigin().getZ();
        boundsMarker.pose.orientation.x = temp1.getRotation().getX();
        boundsMarker.pose.orientation.y = temp1.getRotation().getY();
        boundsMarker.pose.orientation.z = temp1.getRotation().getZ();
        boundsMarker.pose.orientation.w = temp1.getRotation().getW();
        boundsMarker.scale.x = 0.01f;
        boundsMarker.scale.y = 0.01f;
        boundsMarker.scale.z = 0.1f;
        boundsMarker.color.r = 0;
        boundsMarker.color.g = 0;
        boundsMarker.color.b = 1;
        boundsMarker.color.a = 1;
        ma.markers.push_back(boundsMarker);
    }

    for (std::size_t k = 0; k < next_ceti->observerMask().size(); k++){
        if(next_ceti->observerMask()[k]){
            auto wrd = dynamic_cast<RvizPanel*>(next_ceti->observers()[k]->next());
            ma.markers.push_back(wrd->marker());
        }
    }


    ma.markers.push_back(m);
    pub_->publish(ma);
    ma.markers.clear();
    
    for(int j = 0; j <= 7; j++){
        std::bitset<3> wing_config(j);
        for (long unsigned int i = 0; i < access_map.size(); i++){
            next_ceti->setTf(access_map[i]);
            for ( float p = 0; p < 2*M_PI; p += M_PI/2){
                next_ceti->rotate(M_PI/2);
                next_ceti->notify();
                m.action = visualization_msgs::Marker::MODIFY;
                m.pose.position.x = next_ceti->tf().getOrigin().getX();
                m.pose.position.y = next_ceti->tf().getOrigin().getY();
                m.pose.position.z = next_ceti->tf().getOrigin().getZ();
                m.pose.orientation.x = next_ceti->tf().getRotation().getX();
                m.pose.orientation.y = next_ceti->tf().getRotation().getY();
                m.pose.orientation.z = next_ceti->tf().getRotation().getZ();
                m.pose.orientation.w = next_ceti->tf().getRotation().getW();
                ma.markers.push_back(m);

                for(int j = 0; j < next_ceti->robotRootBounds().size(); j++){
                    tf2::Transform temp1 = next_ceti->tf() * next_ceti->rootTf() * next_ceti->robotRootBounds()[j];
                    visualization_msgs::Marker boundsMarker;
                    boundsMarker.header.frame_id = "map";
                    boundsMarker.header.stamp = ros::Time();
                    boundsMarker.ns = next_ceti->name() + "bounds";
                    boundsMarker.id = j;
                    boundsMarker.type = visualization_msgs::Marker::CUBE;
                    boundsMarker.action = visualization_msgs::Marker::MODIFY;
                    boundsMarker.pose.position.x = temp1.getOrigin().getX();
                    boundsMarker.pose.position.y = temp1.getOrigin().getY();
                    boundsMarker.pose.position.z = temp1.getOrigin().getZ();
                    boundsMarker.pose.orientation.x = temp1.getRotation().getX();
                    boundsMarker.pose.orientation.y = temp1.getRotation().getY();
                    boundsMarker.pose.orientation.z = temp1.getRotation().getZ();
                    boundsMarker.pose.orientation.w = temp1.getRotation().getW();
                    boundsMarker.scale.x = 0.01f;
                    boundsMarker.scale.y = 0.01f;
                    boundsMarker.scale.z = 0.1f;
                    boundsMarker.color.r = 0;
                    boundsMarker.color.g = 0;
                    boundsMarker.color.b = 1;
                    boundsMarker.color.a = 1;
                    ma.markers.push_back(boundsMarker);
                }

                for (std::size_t k = 0; k < next_ceti->observerMask().size(); k++){
                    if(next_ceti->observerMask()[k] & wing_config[k]){
                        auto wrd = dynamic_cast<RvizPanel*>(next_ceti->observers()[k]->next());
                        wrd->marker().color.a = 1;
                        ma.markers.push_back(wrd->marker());
                    } else if (next_ceti->observers()[k]){
                        auto wrd = dynamic_cast<RvizPanel*>(next_ceti->observers()[k]->next());
                        wrd->marker().color.a = 0;
                        ma.markers.push_back(wrd->marker());
                    }
                }
                pub_->publish(ma);
                ma.markers.clear();

                bool workload;
                auto temp_state = state;
                temp_state.push_back(it->first);

                auto temp_panel_state = panel_state;
                temp_panel_state.push_back(wing_config);
                bool target_collisions = checkWorkcellCollision(temp_state, temp_panel_state, workload);

                auto nextIt = it;
                ++nextIt;
                if (target_collisions && workload) {
                    new_approximate(workcell, nextIt, temp_state, temp_panel_state);
                }
            }
        }
    }

    visualization_msgs::Marker marks;
    marks.ns = next_ceti->name();
    marks.action = visualization_msgs::Marker::DELETEALL;
    pub_->publish(marks);

    for (std::size_t k = 0; k < next_ceti->observerMask().size(); k++){
        if(next_ceti->observerMask()[k]){
            auto wrd = dynamic_cast<RvizPanel*>(next_ceti->observers()[k]->next());
            marks = wrd->marker();
            marks.action = visualization_msgs::Marker::DELETEALL;
            pub_->publish(marks);
        }
    }

    for(int j = 0; j < next_ceti->robotRootBounds().size(); j++){
        visualization_msgs::Marker boundsMarker;
        boundsMarker.header.frame_id = "map";
        boundsMarker.header.stamp = ros::Time();
        boundsMarker.ns = next_ceti->name() + "bounds";
        boundsMarker.id = j;
        boundsMarker.action = visualization_msgs::Marker::DELETEALL;
        pub_->publish(boundsMarker);

    }

}

void BaseCalculationMediator::calculate(std::map<const std::string, std::vector<tf2::Transform>>& workcell){
    /*
    Iterate through map, safe all possible solutions in [[(name, pose, config), (name, pose, config)], [....]]
    */
    int count = 0;
    visualization_msgs::MarkerArray ma;

    auto first_robot = workcell.begin();
    CetiRobot* ceti1 = dynamic_cast<CetiRobot*>(robots_.at(first_robot->first)->next());
    visualization_msgs::Marker m;
    m.header.frame_id = "map";
    m.header.stamp = ros::Time();
    m.ns = ceti1->name();
    m.id = 1;
    m.type = visualization_msgs::Marker::CUBE;
    m.action = visualization_msgs::Marker::ADD;
    m.pose.position.x = ceti1->tf().getOrigin().getX();
    m.pose.position.y = ceti1->tf().getOrigin().getY();
    m.pose.position.z = ceti1->tf().getOrigin().getZ();
    m.pose.orientation.x = ceti1->tf().getRotation().getX();
    m.pose.orientation.y = ceti1->tf().getRotation().getY();
    m.pose.orientation.z = ceti1->tf().getRotation().getZ();
    m.pose.orientation.w = ceti1->tf().getRotation().getW();

    m.scale.x = ceti1->size().getX();
    m.scale.y = ceti1->size().getY();
    m.scale.z = ceti1->tf().getOrigin().getZ()*2;
    m.color.r = 1.0;
    m.color.g = 1.0;
    m.color.b = 1.0;
    m.color.a = 1.0;

    for(int j = 0; j < ceti1->robotRootBounds().size(); j++){
        tf2::Transform temp1 = ceti1->tf() * ceti1->rootTf() * ceti1->robotRootBounds()[j];
        visualization_msgs::Marker boundsMarker;
        boundsMarker.header.frame_id = "map";
        boundsMarker.header.stamp = ros::Time();
        boundsMarker.ns = ceti1->name() + "bounds";
        boundsMarker.id = j;
        boundsMarker.type = visualization_msgs::Marker::CUBE;
        boundsMarker.action = visualization_msgs::Marker::ADD;
        boundsMarker.pose.position.x = temp1.getOrigin().getX();
        boundsMarker.pose.position.y = temp1.getOrigin().getY();
        boundsMarker.pose.position.z = temp1.getOrigin().getZ();
        boundsMarker.pose.orientation.x = temp1.getRotation().getX();
        boundsMarker.pose.orientation.y = temp1.getRotation().getY();
        boundsMarker.pose.orientation.z = temp1.getRotation().getZ();
        boundsMarker.pose.orientation.w = temp1.getRotation().getW();
        boundsMarker.scale.x = 0.01f;
        boundsMarker.scale.y = 0.01f;
        boundsMarker.scale.z = 0.1f;
        boundsMarker.color.r = 0;
        boundsMarker.color.g = 0;
        boundsMarker.color.b = 1;
        boundsMarker.color.a = 1;
        ma.markers.push_back(boundsMarker);
    }

    for (std::size_t k = 0; k < ceti1->observerMask().size(); k++){
        if(ceti1->observerMask()[k]){
            auto wrd = dynamic_cast<RvizPanel*>(ceti1->observers()[k]->next());
            ma.markers.push_back(wrd->marker());
        }
    }

    for(int j = 5; j <= 7; j++){
        std::bitset<3> wing_config(j);
        for (long unsigned int i = 0; i < first_robot->second.size(); i++){
            ceti1->setTf(first_robot->second[i]);
            for ( float p = 0; p < 2*M_PI; p += 0.0872665f){
                ceti1->rotate(0.0872665f);
                ceti1->notify();
                m.action = visualization_msgs::Marker::MODIFY;
                m.pose.position.x = ceti1->tf().getOrigin().getX();
                m.pose.position.y = ceti1->tf().getOrigin().getY();
                m.pose.position.z = ceti1->tf().getOrigin().getZ();
                m.pose.orientation.x = ceti1->tf().getRotation().getX();
                m.pose.orientation.y = ceti1->tf().getRotation().getY();
                m.pose.orientation.z = ceti1->tf().getRotation().getZ();
                m.pose.orientation.w = ceti1->tf().getRotation().getW();
                ma.markers.push_back(m);

                for(int j = 0; j < ceti1->robotRootBounds().size(); j++){
                    tf2::Transform temp1 = ceti1->tf() * ceti1->rootTf() * ceti1->robotRootBounds()[j];
                    visualization_msgs::Marker boundsMarker;
                    boundsMarker.header.frame_id = "map";
                    boundsMarker.header.stamp = ros::Time();
                    boundsMarker.ns = ceti1->name() + "bounds";
                    boundsMarker.id = j;
                    boundsMarker.type = visualization_msgs::Marker::CUBE;
                    boundsMarker.action = visualization_msgs::Marker::MODIFY;
                    boundsMarker.pose.position.x = temp1.getOrigin().getX();
                    boundsMarker.pose.position.y = temp1.getOrigin().getY();
                    boundsMarker.pose.position.z = temp1.getOrigin().getZ();
                    boundsMarker.pose.orientation.x = temp1.getRotation().getX();
                    boundsMarker.pose.orientation.y = temp1.getRotation().getY();
                    boundsMarker.pose.orientation.z = temp1.getRotation().getZ();
                    boundsMarker.pose.orientation.w = temp1.getRotation().getW();
                    boundsMarker.scale.x = 0.01f;
                    boundsMarker.scale.y = 0.01f;
                    boundsMarker.scale.z = 0.1f;
                    boundsMarker.color.r = 0;
                    boundsMarker.color.g = 0;
                    boundsMarker.color.b = 1;
                    boundsMarker.color.a = 1;
                    ma.markers.push_back(boundsMarker);
                }

                for (std::size_t k = 0; k < ceti1->observerMask().size(); k++){
                    if(ceti1->observerMask()[k] & wing_config[k]){
                        auto wrd = dynamic_cast<RvizPanel*>(ceti1->observers()[k]->next());
                        wrd->marker().color.a = 1;
                        ma.markers.push_back(wrd->marker());
                    } else if (ceti1->observers()[k]){
                        auto wrd = dynamic_cast<RvizPanel*>(ceti1->observers()[k]->next());
                        wrd->marker().color.a = 0;
                        ma.markers.push_back(wrd->marker());
                    }
                }
                pub_->publish(ma);
                ma.markers.clear();

                bool workload;
                std::vector<std::string> check_ws = {first_robot->first};
                std::vector<std::bitset<3>> check_panel = {wing_config};
                bool target_collisions = checkWorkcellCollision(check_ws, check_panel, workload);

                auto nextIt = first_robot;
                ++nextIt;
                if (target_collisions && workload) {
                    new_approximate(workcell, nextIt, check_ws, check_panel);
                    count++;
                }
            }
        }

    }

    std::vector<std::vector<protocol>> combinations;
    std::vector<protocol> currentCombination;
    std::vector<std::string> keys;
    for (const auto& entry : protocol_map_) {
        keys.push_back(entry.first);
    }
    BaseCalculationMediator::generateCombinations(protocol_map_, combinations, currentCombination, keys, 0);
    ROS_INFO("%i", combinations.size());
    writeFile(combinations);
    ros::shutdown;
}

//     for (auto& s_pos : workcell){
//         CetiRobot* ceti1;
//         try{
//             ceti1 = dynamic_cast<CetiRobot*>(robots_.at(s_pos.first)->next());
//         } catch(std::out_of_range& oor){}
       
//         visualization_msgs::Marker m;
//         m.header.frame_id = "map";
//         m.header.stamp = ros::Time();
//         m.ns = ceti1->name();
//         m.id = 1;
//         m.type = visualization_msgs::Marker::CUBE;
//         m.action = visualization_msgs::Marker::ADD;
//         m.pose.position.x = ceti1->tf().getOrigin().getX();
//         m.pose.position.y = ceti1->tf().getOrigin().getY();
//         m.pose.position.z = ceti1->tf().getOrigin().getZ();
//         m.pose.orientation.x = ceti1->tf().getRotation().getX();
//         m.pose.orientation.y = ceti1->tf().getRotation().getY();
//         m.pose.orientation.z = ceti1->tf().getRotation().getZ();
//         m.pose.orientation.w = ceti1->tf().getRotation().getW();

//         m.scale.x = ceti1->size().getX();
//         m.scale.y = ceti1->size().getY();
//         m.scale.z = ceti1->tf().getOrigin().getZ()*2;
//         m.color.r = 1.0;
//         m.color.g = 1.0;
//         m.color.b = 1.0;
//         m.color.a = 1.0;

//         for (std::size_t k = 0; k < ceti1->observerMask().size(); k++){
//             if(ceti1->observerMask()[k]){
//                 auto wrd = dynamic_cast<RvizPanel*>(ceti1->observers()[k]->next());
//                 ma.markers.push_back(wrd->marker());
//             }
//         }


//         ma.markers.push_back(m);
//         pub_->publish(ma);
//         ma.markers.clear();

//         for(int j = 5; j <= 7; j++){
//             std::bitset<3> wing_config(j);
//             for (long unsigned int i = 0; i < s_pos.second.size(); i++){
//                 ceti1->setTf(s_pos.second[i]);
//                 for ( float p = 0; p < 2*M_PI; p += 0.0872665f){
//                     ceti1->rotate(0.0872665f);
//                     ceti1->notify();
//                     m.action = visualization_msgs::Marker::MODIFY;
//                     m.pose.position.x = ceti1->tf().getOrigin().getX();
//                     m.pose.position.y = ceti1->tf().getOrigin().getY();
//                     m.pose.position.z = ceti1->tf().getOrigin().getZ();
//                     m.pose.orientation.x = ceti1->tf().getRotation().getX();
//                     m.pose.orientation.y = ceti1->tf().getRotation().getY();
//                     m.pose.orientation.z = ceti1->tf().getRotation().getZ();
//                     m.pose.orientation.w = ceti1->tf().getRotation().getW();
//                     ma.markers.push_back(m);

//                     for (std::size_t k = 0; k < ceti1->observerMask().size(); k++){
//                         if(ceti1->observerMask()[k] & wing_config[k]){
//                             auto wrd = dynamic_cast<RvizPanel*>(ceti1->observers()[k]->next());
//                             wrd->marker().color.a = 1;
//                             ma.markers.push_back(wrd->marker());
//                         } else if (ceti1->observers()[k]){
//                             auto wrd = dynamic_cast<RvizPanel*>(ceti1->observers()[k]->next());
//                             wrd->marker().color.a = 0;
//                             ma.markers.push_back(wrd->marker());
//                         }
//                     }
//                     pub_->publish(ma);
//                     ma.markers.clear();

//                     bool workload;
//                     std::vector<std::string> check_ws = {s_pos.first};
//                     std::vector<std::bitset<3>> check_panel = {wing_config};
//                     bool target_collisions = checkWorkcellCollision(check_ws, check_panel, workload);

//                     if (target_collisions) count++;
//                     m.action = visualization_msgs::Marker::DELETEALL;
//                     ma.markers.push_back(m);
//                     pub_->publish(ma);
//                     ma.markers.clear();
//                     // if (workload && target_collisions){
//                     //     std::vector<protobuf_entry> wc_solution;// std::vector<protobuff> is one workcell solution
//                     //     wc_solution.push_back(protobuf_entry{ceti1->name(), ceti1->tf(), j});
//                     //     approximation(workcell, wc_solution);
//                     //     if (workcell.size() == wc_solution.size()){
//                     //         all_solutions_per_workcell.push_back(wc_solution);
//                     //     }
//                     // }
//                 }
//             }
//         }
//         m.action = visualization_msgs::Marker::DELETEALL;
//         ma.markers.push_back(m);
//         pub_->publish(ma);
//         ma.markers.clear();
//         protobuf_.push_back(all_solutions_per_workcell);
//     }

    
    
// }

void BaseCalculationMediator::approximation(std::map<const std::string, std::vector<tf2::Transform>>& workcell, std::vector<protobuf_entry>& wc_solution){
    // build access fiels to existing wc_solution
    std::vector<tf2::Transform> access_map;
    float padding = 0.0025f;

    // Choose next robot 
    CetiRobot* next_ceti;
    if (!(workcell.size() == wc_solution.size())){
        for (auto& r: workcell){
            for (auto& s: wc_solution){
                if (!(r.first == s.name_)){
                    next_ceti = dynamic_cast<CetiRobot*>(robots_.at(r.first)->next());
                    break;
                }
            }
        }
    } else {
        return;
    }

    // make task_space unique
    auto ts = task_space_reader_->dropOffData().at(next_ceti->name());
    for (auto& r: wc_solution){
        for(auto& vec: task_space_reader_->dropOffData().at(r.name_)){
            for (auto it = ts.begin(); it != ts.end();){
                if(tf2::tf2Distance2(it->pose_.getOrigin(), vec.pose_.getOrigin()) == 0) {
                    ts.erase(it);
                } else {
                    ++it;
                }
            }
        }
    }

    for (auto& r : wc_solution){
        CetiRobot* ceti = dynamic_cast<CetiRobot*>(robots_.at(r.name_)->next());
        ceti->setTf(r.tf_);
        ceti->notify();

        for (int i = 0; i <= 2; i++){
            for (int j = 0; j <= 2; j++){
                if(i == 0 && j == 0) {continue;}
                if(i == 2 && j == 2) {continue;}
                if(i == 0) {
                    access_map.push_back(tf2::Transform(tf2::Quaternion(0,0,0,1), tf2::Vector3(0,ceti->size().getY()*j + padding,0)));
                    access_map.push_back(tf2::Transform(tf2::Quaternion(0,0,0,1), tf2::Vector3(0,-ceti->size().getY()*j + padding,0)));
                } else if (j == 0){
                    access_map.push_back(tf2::Transform(tf2::Quaternion(0,0,0,1), tf2::Vector3(ceti->size().getX()*i + padding,0,0)));
                    access_map.push_back(tf2::Transform(tf2::Quaternion(0,0,0,1), tf2::Vector3(-ceti->size().getX()*i + padding,0,0)));
                } else {
                access_map.push_back(tf2::Transform(tf2::Quaternion(0,0,0,1), tf2::Vector3(ceti->size().getX()*i+padding,ceti->size().getY()*j +padding,0)));
                access_map.push_back(tf2::Transform(tf2::Quaternion(0,0,0,1), tf2::Vector3(-ceti->size().getX()*i+padding,ceti->size().getY()*j +padding,0)));
                access_map.push_back(tf2::Transform(tf2::Quaternion(0,0,0,1), tf2::Vector3(ceti->size().getX()*i+padding,-ceti->size().getY()*j +padding,0)));
                access_map.push_back(tf2::Transform(tf2::Quaternion(0,0,0,1), tf2::Vector3(-ceti->size().getX()*i+padding,-ceti->size().getY()*j+padding,0)));
                }

            };
        };

        std::bitset<3> actual_config(r.wing_config_);
        for(std::size_t i = 0; i < ceti->observers().size(); i++){
            if(actual_config[i]){
            access_map.push_back(tf2::Transform(tf2::Quaternion(0,0,0,1), tf2::Vector3(0.5f * ceti->size().getX() + ceti->observers()[i]->size().getX()  + 2*padding + next_ceti->size().getX()*0.5f,0,0)));
            access_map.push_back(tf2::Transform(tf2::Quaternion(0,0,0,1), tf2::Vector3(-0.5f * ceti->size().getX() - ceti->observers()[i]->size().getX() - 2*padding - next_ceti->size().getX()*0.5f,0,0)));
            access_map.push_back(tf2::Transform(tf2::Quaternion(0,0,0,1), tf2::Vector3(0,0.5f*ceti->size().getY() + ceti->observers()[i]->size().getY()  + 2*padding + next_ceti->size().getY()*0.5f ,0)));
            access_map.push_back(tf2::Transform(tf2::Quaternion(0,0,0,1), tf2::Vector3(0,-0.5f*ceti->size().getY() - ceti->observers()[i]->size().getY() - 2*padding - next_ceti->size().getY()*0.5f,0))); 
            }
        }

        for(auto& am: access_map){
            am = r.tf_ * am;
        }

    }

    visualization_msgs::MarkerArray ma;
    visualization_msgs::Marker m;
    m.header.frame_id = "map";
    m.header.stamp = ros::Time();
    m.ns = next_ceti->name();
    m.id = 1;
    m.type = visualization_msgs::Marker::CUBE;
    m.action = visualization_msgs::Marker::ADD;
    m.pose.position.x = next_ceti->tf().getOrigin().getX();
    m.pose.position.y = next_ceti->tf().getOrigin().getY();
    m.pose.position.z = next_ceti->tf().getOrigin().getZ();
    m.pose.orientation.x = next_ceti->tf().getRotation().getX();
    m.pose.orientation.y = next_ceti->tf().getRotation().getY();
    m.pose.orientation.z = next_ceti->tf().getRotation().getZ();
    m.pose.orientation.w = next_ceti->tf().getRotation().getW();

    m.scale.x = next_ceti->size().getX();
    m.scale.y = next_ceti->size().getY();
    m.scale.z = next_ceti->tf().getOrigin().getZ()*2;
    m.color.r = 1.0;
    m.color.g = 1.0;
    m.color.b = 1.0;
    m.color.a = 1.0;

    for (std::size_t k = 0; k < next_ceti->observerMask().size(); k++){
        if(next_ceti->observerMask()[k]){
            auto wrd = dynamic_cast<RvizPanel*>(next_ceti->observers()[k]->next());
            ma.markers.push_back(wrd->marker());
        }
    }

    ma.markers.push_back(m);
    pub_->publish(ma);
    ma.markers.clear();
    
    for(int j = 0; j <= 7; j++){
        std::bitset<3> wing_config(j);
        for (long unsigned int i = 0; i < access_map.size(); i++){
            next_ceti->setTf(access_map[i]);
            for ( float p = 0; p < 2*M_PI; p += M_PI/2){
                next_ceti->rotate(M_PI/2);
                next_ceti->notify();
                m.action = visualization_msgs::Marker::MODIFY;
                m.pose.position.x = next_ceti->tf().getOrigin().getX();
                m.pose.position.y = next_ceti->tf().getOrigin().getY();
                m.pose.position.z = next_ceti->tf().getOrigin().getZ();
                m.pose.orientation.x = next_ceti->tf().getRotation().getX();
                m.pose.orientation.y = next_ceti->tf().getRotation().getY();
                m.pose.orientation.z = next_ceti->tf().getRotation().getZ();
                m.pose.orientation.w = next_ceti->tf().getRotation().getW();
                ma.markers.push_back(m);

                for (std::size_t k = 0; k < next_ceti->observerMask().size(); k++){
                    if(next_ceti->observerMask()[k] & wing_config[k]){
                        auto wrd = dynamic_cast<RvizPanel*>(next_ceti->observers()[k]->next());
                        wrd->marker().color.a = 1;
                        ma.markers.push_back(wrd->marker());
                    } else if (next_ceti->observers()[k]){
                        auto wrd = dynamic_cast<RvizPanel*>(next_ceti->observers()[k]->next());
                        wrd->marker().color.a = 0;
                        ma.markers.push_back(wrd->marker());
                    }
                }
                pub_->publish(ma);
                ma.markers.clear();

                bool workload;
                bool target_collisions = checkCollision(next_ceti->name(), wing_config, workload, ts);
                if(workload && target_collisions) {
                    wc_solution.push_back({next_ceti->name(), next_ceti->tf(), j});
                    if (sceneCollision(wc_solution)) {
                        wc_solution.pop_back();
                    } else {
                        if ( workcell.size() == wc_solution.size()) {
                            //ros::shutdown();
                        }
                    }
                }
            }
        }
    }
    m.action = visualization_msgs::Marker::DELETEALL;
    ma.markers.push_back(m);
    pub_->publish(ma);
    ma.markers.clear();
}

bool BaseCalculationMediator::sceneCollision(std::vector<protobuf_entry>& wc_solution){
    for(auto& r1 : wc_solution){
        std::vector<tf2::Transform> bounds;
        for(auto& r2 : wc_solution){
            if (r1.name_ == r2.name_) continue;
            auto robot2 = dynamic_cast<CetiRobot*>(robots_.at(r2.name_)->next());
            bounds.push_back(r2.tf_);

            robot2->setTf(r2.tf_);
            robot2->notify();
            
            bounds.push_back(r2.tf_ * robot2->rootTf() * robot2->robotRootBounds()[0]);
            bounds.push_back(r2.tf_ * robot2->rootTf() * robot2->robotRootBounds()[1]);
            bounds.push_back(r2.tf_ * robot2->rootTf() * robot2->robotRootBounds()[2]);
            bounds.push_back(r2.tf_ * robot2->rootTf() * robot2->robotRootBounds()[3]);

            bounds.push_back(r2.tf_ * robot2->bounds()[0]);
            bounds.push_back(r2.tf_ * robot2->bounds()[1]);
            bounds.push_back(r2.tf_ * robot2->bounds()[2]);
            bounds.push_back(r2.tf_ * robot2->bounds()[3]);

            std::bitset<3> panel_mask(r2.wing_config_);
            for(std::size_t i = 0; i < robot2->observers().size(); i++){
                if(robot2->observerMask()[i] & panel_mask[i]){
                    bounds.push_back(robot2->observers()[i]->worldTf());
                    bounds.push_back(robot2->observers()[i]->worldTf() * robot2->observers()[i]->bounds()[0]);
                    bounds.push_back(robot2->observers()[i]->worldTf() * robot2->observers()[i]->bounds()[1]);
                    bounds.push_back(robot2->observers()[i]->worldTf() * robot2->observers()[i]->bounds()[2]);
                    bounds.push_back(robot2->observers()[i]->worldTf() * robot2->observers()[i]->bounds()[3]);
                } 
            }
        }

        auto robot1 = dynamic_cast<CetiRobot*>(robots_.at(r1.name_)->next());
        robot1->setTf(r1.tf_);
        robot1->notify();

        std::bitset<3> panel_mask(r1.wing_config_);
        std::string str;
        for(int i = 0; i < bounds.size(); i++){
            if (robot1->checkSingleObjectCollision(bounds[i], str, panel_mask)) {
                /*
                ROS_INFO("%s, %s, %f, %f, %f", str.c_str(),robot1->name().c_str(), bounds[i].getOrigin().getX(), bounds[i].getOrigin().getY(), bounds[i].getOrigin().getZ());
                visualization_msgs::MarkerArray ma;
                visualization_msgs::Marker m;
                m.header.frame_id = "map";
                m.header.stamp = ros::Time();
                m.ns = "point" +i;
                m.id = 1;
                m.type = visualization_msgs::Marker::CUBE;
                m.action = visualization_msgs::Marker::ADD;
                m.pose.position.x = bounds[i].getOrigin().getX();
                m.pose.position.y = bounds[i].getOrigin().getY();
                m.pose.position.z = bounds[i].getOrigin().getZ();
                m.pose.orientation.x = 0;
                m.pose.orientation.y = 0;
                m.pose.orientation.z = 0;
                m.pose.orientation.w = 1;

                m.scale.x = 0.01f;
                m.scale.y = 0.01f;
                m.scale.z = 0.01f;
                m.color.r = 1.0;
                m.color.g = 0;
                m.color.b = 0;
                m.color.a = 1.0;
                ma.markers.push_back(m);
                pub_->publish(ma);
                */
                return true;
            }
        }
    }
    return false;
}
/*
    
void Base_calculation_mediator::write_file(){
    for(auto& workcell: protobuf_){
        for(auto& single_solution: workcell){
            
        }
    }
}
*/

void BaseCalculationMediator::writeFile(std::vector<std::vector<protocol>>& combinations){
    std::stringstream box_ss;

    for (auto& box : cuboid_reader_->cuboidBox()){
        box_ss << "{ 'id': '" << box.Name << "',  'type': 'BOX', 'pos': { 'x': "<<box.Pose.position.x<<", 'y': "<<box.Pose.position.y<<", 'z': "<< box.Pose.position.z<<" },'size': { 'length': "<< box.x_depth<<", 'width': "<<box.y_width<<", 'height': "<< box.z_heigth<<" },'orientation': { 'x':"<< box.Pose.orientation.x <<", 'y':" << box.Pose.orientation.y<< ", 'z':" << box.Pose.orientation.z << ", 'w':" << box.Pose.orientation.w<< "},'color': { 'b': 1 } }, \n";
    }

    for (auto& box : cuboid_reader_->cuboidObstacle()){
        box_ss << "{ 'id': '" << box.Name << "',  'type': 'BIN', 'pos': { 'x': "<<box.Pose.position.x<<", 'y': "<<box.Pose.position.y<<", 'z': "<< box.Pose.position.z<<" },'size': { 'length': "<< box.x_depth<<", 'width': "<<box.y_width<<", 'height': "<< box.z_heigth<<" },'orientation': { 'x':"<< box.Pose.orientation.x <<", 'y':" << box.Pose.orientation.y<< ", 'z':" << box.Pose.orientation.z << ", 'w':" << box.Pose.orientation.w<< "},'color': { 'b': 1 } }, \n";
    }

    for (int c = 0; c < combinations.size(); c++){
        std::string resultFile = std::to_string(static_cast<int>(ros::Time::now().toNSec()));
        std::filesystem::create_directory(ros::package::getPath("multi_cell_builder") + "/results/" +  filename_ + "/" + resultFile);
        std::filesystem::create_directory(ros::package::getPath("multi_cell_builder") + "/results/" +  filename_ + "/" + resultFile + "/configs");
        std::filesystem::create_directory(ros::package::getPath("multi_cell_builder") + "/results/" +  filename_ + "/" + resultFile + "/launch");
        std::filesystem::create_directory(ros::package::getPath("multi_cell_builder") + "/results/" +  filename_ + "/" + resultFile + "/jobs");

        std::stringstream ss;
        std::stringstream panel_ss;
        std::stringstream root_ss;

        ss << "{ 'objects' : [ \n";

        for (int x = 0; x < combinations[c].size(); x++){
            for (auto& prot: combinations[c][x].robots){
            try{   
                std::regex rx("panda_arm([0-9]+)");
                std::smatch match;
                std::regex_match(std::get<0>(prot), match, rx);

                auto ceti = dynamic_cast<CetiRobot*>(robots_.at(std::get<0>(prot))->next());
                ceti->setTf(std::get<1>(prot));
                ceti->notify();

                double r,p,y;
                tf2::Matrix3x3 m(std::get<1>(prot).getRotation());
                m.getRPY(r,p,y);

                float size_x = ceti->size().getX();
                float size_y = ceti->size().getY();
                float size_z = ceti->size().getZ();

                float pos_x = ceti->tf().getOrigin().getX();
                float pos_y = ceti->tf().getOrigin().getY();
                float pos_z = ceti->tf().getOrigin().getZ() *2 ;
                float rot_x = ceti->tf().getRotation().getX();
                float rot_y = ceti->tf().getRotation().getY();
                float rot_z = ceti->tf().getRotation().getZ();
                float rot_w = ceti->tf().getRotation().getW();

                // initial stardconfig
                ss << "{ 'id' : 'table" << match[1] << "_wheel_1', 'pos': { 'x': 0,'y': 0,'z': 0.06 },'size': { 'length': 0.12,'width': 0.12,'height': 0.12 },'orientation': { 'w': 1 },'color': { 'r': 0.15,'g': 0.15,'b': 0.15 }}, \n";
                ss << "{ 'id' : 'table" << match[1] << "_wheel_2', 'pos': { 'x': 0,'y': 0,'z': 0.06 },'size': { 'length': 0.12,'width': 0.12,'height': 0.12 },'orientation': { 'w': 1 },'color': { 'r': 0.15,'g': 0.15,'b': 0.15 }},\n";
                ss << "{ 'id' : 'table" << match[1] << "_wheel_3', 'pos': { 'x': 0,'y': 0,'z': 0.06 },'size': { 'length': 0.12,'width': 0.12,'height': 0.12 },'orientation': { 'w': 1 },'color': { 'r': 0.15,'g': 0.15,'b': 0.15 }}, \n";
                ss << "{ 'id' : 'table" << match[1] << "_wheel_4', 'pos': { 'x': 0,'y': 0,'z': 0.06 },'size': { 'length': 0.12,'width': 0.12,'height': 0.12 },'orientation': { 'w': 1 },'color': { 'r': 0.15,'g': 0.15,'b': 0.15 }}, \n";
                ss << "{ 'id' : 'table" << match[1] << "_body_front', 'pos': { 'x': 0,'y': 0, 'z': 0.50  },'size': { 'length': 0.12,'width': 0.12,'height': 0.12 },'orientation': { 'w': 1 },'color': { 'r': 0.15,'g': 0.15,'b': 0.15 }}, \n";
                ss << "{ 'id' : 'table" << match[1] << "_body_back', 'pos': { 'x': 0,'y': 0,'z': 0.45 },'size': { 'length': 0.12,'width': 0.12,'height': 0.12 },'orientation': { 'w': 1 },'color': { 'r': 0.15,'g': 0.15,'b': 0.15 }}, \n";
                ss << "{ 'id' : 'table" << match[1] << "_body_left', 'pos': { 'x': 0,'y': 0,'z': 0.50  },'size': { 'length': 0.12,'width': 0.12,'height': 0.12 },'orientation': { 'w': 1 },'color': { 'r': 0.15,'g': 0.15,'b': 0.15 }}, \n";
                ss << "{ 'id' : 'table" << match[1] << "_body_right', 'pos': { 'x': 0,'y': 0,'z': 0.50  },'size': { 'length': 0.12,'width': 0.12,'height': 0.12 },'orientation': { 'w': 1 },'color': { 'r': 0.15,'g': 0.15,'b': 0.15 }}, \n";
                ss << "{ 'id' : 'table" << match[1] << "_table_top', 'pos': { 'x': " << std::to_string(pos_x) << " , 'y': "<< std::to_string(pos_y) << " , 'z': "<< std::to_string(pos_z) << " },'size': { 'length': "<< std::to_string(size_x) << " ,'width': "<< std::to_string(size_y) << " ,'height': "<< std::to_string(size_z) << " },'orientation': { 'x': " << std::to_string(rot_x) << " , 'y': " << std::to_string(rot_y) << " , 'z': " << std::to_string(rot_z) << " , 'w': " << std::to_string(rot_w) << " },'color': { 'r': 0.15,'g': 0.15,'b': 0.15 }, 'rpy': { 'r': " << std::to_string(r) << " , 'p': " << std::to_string(p) << " , 'y': " << std::to_string(y) << " } },\n";
                
                // config
                std::stringstream config;
                config << "mapspace: {'dim': [";
                config << size_x/2 << ", ";
                config << size_y/2 << ", 0.04], ";
                config << "'pos': [";
                config << pos_x << ", ";
                config << pos_y << ", ";
                config << (pos_z + size_z/2.0f + 0.04f/2.0f) << "], ";
                config << "'rot': [";
                config << r << ", " << p << ", " << y <<"], ";
                config << "'verbose': false }\n";
                config << "voxel_size: 0.02 \n";
                config << "voxel_space: {'dim': [";
                config << (size_x + 0.1f)/2 << ", ";
                config << (size_y + 0.1f)/2 << ", 0.2], ";
                config << "'pos': [";
                config << pos_x << ", ";
                config << pos_y << ", ";
                config << (pos_z + size_z/2.0f + 0.2f/2.0f) << "], ";
                config << "'rot': [";
                config << r << ", " << p << ", " << y <<"]}\n";
                config << "voxel_verbose_level: 0\n";
                config << "translation_rate: 0.03\n";
                config << "rotation_rate: 45.0\n";
                config << "rotation_max: 90.0\n";
                config << "clearance: 0.01\n";
                config << "min_quality: 0.1\n";
                config << "max_candidate_count: 10\n";
                config << "allow_dependencies: false\n";
                config << "top_grasps_only: true\n";

                std::ofstream o(ros::package::getPath("multi_cell_builder") + "/results/"+ filename_ + "/" + resultFile + "/configs/" + resultFile + "_"+ std::get<0>(prot)+".yaml");
                o << config.str();
                o.close();


                std::bitset<3> panel_mask(std::get<2>(prot));
                for(std::size_t i = 0; i < ceti->observers().size(); i++){
                    if(ceti->observerMask()[i] & panel_mask[i]){
                        float x = ceti->observers()[i]->worldTf().getOrigin().getX();
                        float y = ceti->observers()[i]->worldTf().getOrigin().getY();
                        float z = ceti->observers()[i]->worldTf().getOrigin().getZ();
                        float qx = ceti->observers()[i]->worldTf().getRotation().getX();
                        float qy = ceti->observers()[i]->worldTf().getRotation().getY();
                        float qz = ceti->observers()[i]->worldTf().getRotation().getZ();
                        float qw = ceti->observers()[i]->worldTf().getRotation().getW();

                        float length = ceti->observers()[i]->size().getX();
                        float width = ceti->observers()[i]->size().getY();
                        float height = ceti->observers()[i]->size().getZ();
                        panel_ss << "{ 'id': '" << ceti->observers()[i]->name() << "' , 'pos': { 'x': "<< std::to_string(x) << " , 'y': "<< std::to_string(y) << " , 'z': "<< std::to_string(z - 0.25*height) << " } , 'size': { 'length': "<< std::to_string(length) << " , 'width': "<< std::to_string(width) << " , 'height': "<< std::to_string(height) << " } , 'orientation': { 'x': "<< std::to_string(qx) << " , 'y': "<< std::to_string(qy) << " , 'z': "<< std::to_string(qz) << " , 'w': "<< std::to_string(qw) << " } , 'color': { 'r': 0.15 , 'g': 0.15 , 'b': 0.15 } }, \n";
                    } 
                }

                tf2::Transform tf_arm =  std::get<1>(prot) * ceti->rootTf();
                float arm_x = tf_arm.getOrigin().getX();
                float arm_y = tf_arm.getOrigin().getY();
                float arm_z = tf_arm.getOrigin().getZ();
                float arm_qx = tf_arm.getRotation().getX();
                float arm_qy = tf_arm.getRotation().getY();
                float arm_qz = tf_arm.getRotation().getZ();
                float arm_qw = tf_arm.getRotation().getW();
                
                root_ss << "{ 'id': 'arm" << match[1] << "','type': 'ARM','pos': { 'x': " << std::to_string(arm_x) << ", 'y': " << std::to_string(arm_y) << ", 'z': 0.89 },'size': { },'orientation': { 'x': " << std::to_string(arm_qx) <<", 'y': " << std::to_string(arm_qy) << ", 'z': " << std::to_string(arm_qz) << ", 'w': " << std::to_string(arm_qw) << " },'color': { 'r': 1.00,'g': 1.00,'b': 1.00 } }, \n";
                
                //launch
                std::stringstream launch;
                launch << "<launch>\n";
                launch << "<arg name=\"referenceRobot\" default=\""<< std::get<0>(prot) << "\" />\n";
                launch << "<arg name=\"referenceXYZ\" default=\""<< arm_x <<" "<< arm_y << " " << arm_z <<"\"/>\n";
                launch << "<arg name=\"referenceRPY\" default=\"" << r << " " << p << " " << y << "\"/>\n";
                launch << "<arg name=\"result\" default=\"" << filename_ <<"/" << resultFile << "/" << resultFile <<".yaml\" />\n";
                launch << "<rosparam command=\"load\" file=\"$(find multi_cell_builder)/results/$(arg result)\"/>\n";
                launch << "<rosparam param=\"referenceRobot\" subst_value=\"True\"> $(arg referenceRobot)</rosparam>\n";
                launch << "<rosparam param=\"resultPath\" subst_value=\"True\"> $(arg result)</rosparam>\n";
                launch << "<arg name=\"pipeline\" default=\"ompl\"/>\n";
                launch << "<arg name=\"db\" default=\"false\"/>\n";
                launch << "<arg name=\"db_path\" default=\"$(find panda_moveit_config)/default_warehouse_mongo_db\"/>\n";
                launch << "<arg name=\"debug\" default=\"false\" />\n";
                launch << "<arg name=\"load_robot_description\" default=\"true\"/>\n";
                launch << "<arg name=\"moveit_controller_manager\" default=\"fake\"/>\n";
                launch << "<arg name=\"fake_execution_type\" default=\"interpolate\"/>\n";
                launch << "<arg name=\"use_gui\" default=\"false\"/>\n";
                launch << "<arg name=\"use_rviz\" default=\"true\"/>\n";
                launch << "<node pkg=\"tf2_ros\" type=\"static_transform_publisher\" name=\"virtual_joint_broadcaster_0\" args=\"0 0 0 0 0 0 world panda_link0\"/>\n";
                launch << "<group if=\"$(eval arg('moveit_controller_manager') == 'fake')\">\n";
                launch << "<node name=\"joint_state_publisher\" pkg=\"joint_state_publisher\" type=\"joint_state_publisher\" unless=\"$(arg use_gui)\">\n";
                launch << "<rosparam param=\"source_list\">[move_group/fake_controller_joint_states]</rosparam>\n";
                launch << "</node>\n";
                launch << "<node name=\"joint_state_publisher\" pkg=\"joint_state_publisher_gui\" type=\"joint_state_publisher_gui\" if=\"$(arg use_gui)\">\n";
                launch << "<rosparam param=\"source_list\">[move_group/fake_controller_joint_states]</rosparam>\n";
                launch << "</node>\n";
                launch << "<node name=\"robot_state_publisher\" pkg=\"robot_state_publisher\" type=\"robot_state_publisher\" respawn=\"true\" output=\"screen\"/>\n";
                launch << "</group>\n";
                launch << "<include file=\"$(find panda_moveit_config)/launch/move_group.launch\">\n";
                launch << "<arg name=\"allow_trajectory_execution\" value=\"true\"/>\n";
                launch << "<arg name=\"moveit_controller_manager\" value=\"$(arg moveit_controller_manager)\"/>\n";
                launch << "<arg name=\"fake_execution_type\" value=\"$(arg fake_execution_type)\"/>\n";
                launch << "<arg name=\"info\" value=\"true\"/>\n";
                launch << "<arg name=\"debug\" value=\"$(arg debug)\"/>\n";
                launch << "<arg name=\"pipeline\" value=\"$(arg pipeline)\"/>\n";
                launch << "<arg name=\"load_robot_description\" value=\"$(arg load_robot_description)\"/>\n";
                launch << "<arg name=\"referenceXYZ\" value=\"$(arg referenceXYZ)\"/>\n";
                launch << "<arg name=\"referenceRPY\" value=\"$(arg referenceRPY)\"/>\n";
                launch << "</include>\n";

                launch << "<include file=\"$(find panda_moveit_config)/launch/default_warehouse_db.launch\" if=\"$(arg db)\">\n";
                launch << "<arg name=\"moveit_warehouse_database_path\" value=\"$(arg db_path)\"/>\n";
                launch << "</include>\n";

                launch << "<node name=\"$(anon rviz)\" pkg=\"rviz\" type=\"rviz\" respawn=\"false\" args=\"-d $(find moveit_grasps)/launch/rviz/grasps.rviz\" output=\"screen\">\n";
                launch << "<rosparam command=\"load\" file=\"$(find panda_moveit_config)/config/kinematics.yaml\"/>\n";
                launch << "</node>\n";

                launch << "<node name=\"config_routine\"  pkg=\"multi_cell_builder\" type=\"config_routine\" output=\"screen\">\n";
                launch << "<param name=\"gripper\" value=\"two_finger\"/>\n";
                launch << "<param name=\"ee_group_name\" value=\"hand\"/>\n";
                launch << "<param name=\"planning_group_name\" value=\"panda_arm_hand\" />\n";
                launch << "<rosparam command=\"load\" file=\"$(find gb_grasp)/config_robot/panda_grasp_data.yaml\"/>\n";
                launch << "<rosparam command=\"load\" file=\"$(find gb_grasp)/config/moveit_grasps_config.yaml\"/>\n";
                launch << "<rosparam command=\"load\" file=\"$(find multi_cell_builder)/results/" << filename_ << "/" << resultFile << "/configs/" << resultFile << "_" << std::get<0>(prot) << ".yaml\"/>\n";
                launch << "</node>\n";

                launch << "<arg name=\"planner\" default=\"ompl\" />\n";
                launch << "<include ns=\"moveit_grasps_demo\" file=\"$(find panda_moveit_config)/launch/planning_pipeline.launch.xml\">\n";
                launch << "<arg name=\"pipeline\" value=\"$(arg planner)\" />\n";
                launch << "</include>\n";
                launch << "</launch>\n";

                std::ofstream l(ros::package::getPath("multi_cell_builder") + "/results/"+ filename_ + "/" + resultFile + "/launch/" + std::get<0>(prot)+ "_" + resultFile + ".launch");
                l << launch.str();
                l.close();



                
            } catch (std::out_of_range& oor) {
            }
            }
        }
        std::ofstream o(ros::package::getPath("multi_cell_builder") + "/results/"+ filename_ + "/" + resultFile + "/" + resultFile + ".yaml");
        ss << panel_ss.str();
        ss << box_ss.str();
        ss << root_ss.str();
        ss << "]}";
        o << ss.str();
        o.close();

        std::stringstream execute;
        execute << "<launch>\n";
        execute << "<arg name=\"result\" default=\""<< filename_<< "/" << resultFile << "/" << resultFile << ".yaml\" />\n";
        execute << "<arg name=\"jobs\" default=\""<< filename_<< "/" << resultFile << "/jobs/dummy.yaml\" />\n";
        execute << "<rosparam command=\"load\" file=\"$(find multi_cell_builder)/results/$(arg result)\"/>\n";
        execute << "<rosparam command=\"load\" file=\"$(find multi_cell_builder)/results/$(arg jobs)\"/>\n";
        execute << "<rosparam ns=\"planning_pipelines\" param=\"pipeline_names\">[\"ompl\"]</rosparam>\n";

        if (robots_.size() == 2) execute << "<include file=\"$(find ceti_double)/launch/demo.launch\">\n";
        if (robots_.size() == 3) execute << "<include file=\"$(find ceti_triple)/launch/demo.launch\">\n";
        if (robots_.size() == 4) execute << "<include file=\"$(find ceti_quadruple)/launch/demo.launch\">\n";
        execute << "<arg name=\"use_rviz\" value=\"false\"/>\n";
        execute << "<arg name=\"scene\" value=\"$(arg result)\" />\n";
        execute << "</include> \n";
        if (robots_.size() == 2) execute << "<include ns=\"cell_routine\" file=\"$(find ceti_double)/launch/fake_moveit_controller_manager.launch.xml\" /> \n";
        if (robots_.size() == 3) execute << "<include ns=\"cell_routine\" file=\"$(find ceti_triple)/launch/fake_moveit_controller_manager.launch.xml\" /> \n";
        if (robots_.size() == 4) execute << "<include ns=\"cell_routine\" file=\"$(find ceti_quadruple)/launch/fake_moveit_controller_manager.launch.xml\" /> \n";
        execute << "<param name=\"move_group/capabilities\" value=\"move_group/ExecuteTaskSolutionCapability\"/>\n";

        if (robots_.size() == 2) execute << "<include file=\"$(find ceti_double)/launch/moveit_rviz.launch\"> \n";
        if (robots_.size() == 3) execute << "<include file=\"$(find ceti_triple)/launch/moveit_rviz.launch\"> \n";
        if (robots_.size() == 4) execute << "<include file=\"$(find ceti_quadruple)/launch/moveit_rviz.launch\"> \n";
        execute << "<arg name=\"rviz_config\" value=\"$(find multi_cell_builder)/launch/rviz/cell_routine.rviz\" /> \n";
        execute << "</include> \n";
        execute << "<node pkg=\"groot\" type=\"Groot\" name=\"Groot\" output=\"screen\" required=\"true\"> \n";
        execute << "</node> \n";
        execute << "<node pkg=\"multi_cell_builder\" type=\"cell_routine\" name=\"cell_routine\" output=\"screen\" required=\"true\"> \n";
        execute << "</node> \n";
        execute << "</launch>\n";
        std::ofstream e(ros::package::getPath("multi_cell_builder") + "/results/"+ filename_ + "/" + resultFile + "/execute" + resultFile + ".launch");
        e << execute.str();

    }

}

void BaseCalculationMediator::publish(CetiRobot* ar){
    visualization_msgs::MarkerArray ma;
    visualization_msgs::Marker marker;
    marker.header.frame_id = "map";
    marker.header.stamp = ros::Time();
    marker.ns = ar->name();
    marker.id = 1;
    marker.type = visualization_msgs::Marker::CUBE;
    marker.action = visualization_msgs::Marker::MODIFY;
    marker.pose.position.x = ar->tf().getOrigin().getX();
    marker.pose.position.y = ar->tf().getOrigin().getY();
    marker.pose.position.z = ar->tf().getOrigin().getZ();
    marker.pose.orientation.x = ar->tf().getRotation().getX();
    marker.pose.orientation.y = ar->tf().getRotation().getY();
    marker.pose.orientation.z = ar->tf().getRotation().getZ();
    marker.pose.orientation.w = ar->tf().getRotation().getW();
    marker.scale.x = ar->size().getX();
    marker.scale.y = ar->size().getY();
    marker.scale.z = ar->size().getZ();
    marker.color.r = 1.0;
    marker.color.g = 1.0;
    marker.color.b = 1.0;
    marker.color.a = 1.0;
    for (int i = 0; i < ar->observers().size(); i++){
        auto wrd = dynamic_cast<RvizPanel*>(ar->observers()[i].get());
        ma.markers.push_back(wrd->marker());
    }
    
    //pub_->publish(ma);
}
