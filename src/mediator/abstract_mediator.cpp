#include "mediator/abstract_mediator.h"

AbstractMediator::AbstractMediator(std::shared_ptr<ros::NodeHandle> const& d)
    : nh_(d)
    , task_space_reader_(std::make_unique<TSReader>(d))
    , wing_reader_(std::make_unique<WingReader>(d))
    , robot_reader_(std::make_unique<RobotReader>(d))
    , cuboid_reader_(std::make_unique<CuboidReader>(d))
    {}

pcl::PointCloud< pcl::PointXYZ >::Ptr AbstractMediator::vector2cloud(std::vector<pcl::PointXYZ>& vector){
  pcl::PointCloud< pcl::PointXYZ >::Ptr task_voxel(new pcl::PointCloud< pcl::PointXYZ >);
  for(pcl::PointXYZ& point : vector)
    task_voxel->push_back(point);
  
  return task_voxel;
}
