#include "mediator/moveit_mediator.h"
#include "mediator/abstract_mediator.h"

#include <thread>
#include <mutex> 
#include <algorithm>
#include <gb_grasp/MapGenerator.h>
#include <regex>
#include <actionlib_msgs/GoalStatusArray.h>
#include <chrono>

MoveitMediator::MoveitMediator(std::shared_ptr<ros::NodeHandle> const& nh)
    : AbstractMediator(nh)
    , sampling_planner_(std::make_unique<moveit::task_constructor::solvers::PipelinePlanner>())
    , cartesian_planner_(std::make_unique<moveit::task_constructor::solvers::CartesianPath>())
    , psi_(std::make_unique<moveit::planning_interface::PlanningSceneInterface>())
    , mgi_(std::make_shared<moveit::planning_interface::MoveGroupInterface>("panda_arms"))
    , planning_scene_diff_publisher_(std::make_shared<ros::Publisher>(nh_->advertise<moveit_msgs::PlanningScene>("planning_scene", 1)))
	, job_reader_(std::make_unique<JobReader>(nh_)){

        robot_model_loader::RobotModelLoaderPtr robot_model_loader;
        robot_model_loader = std::make_shared<robot_model_loader::RobotModelLoader>("robot_description");
        robot_model_ = robot_model_loader->getModel();
        ps_ = std::make_shared<planning_scene::PlanningScene>(robot_model_);

        // planner 
        sampling_planner_->setProperty("goal_joint_tolerance", 1e-5);

        // cartesian
	    cartesian_planner_->setMaxVelocityScaling(1.0);
	    cartesian_planner_->setMaxAccelerationScaling(1.0);
	    cartesian_planner_->setStepSize(.01);

    };

void MoveitMediator::connectRobots(std::unique_ptr<AbstractRobotDecorator> robot) {
	ROS_INFO("connecting %s...", robot->name().c_str());
	robot->spezifieRobotGroups();


	acm_.insert(std::pair<std::string, std::vector<uint8_t>>(robot->map().at("base").c_str(), std::vector<uint8_t>()));

	
	for(auto name: robot->mgi()->getLinkNames()) acm_.insert(std::pair<std::string, std::vector<uint8_t>>(name.c_str(), std::vector<uint8_t>()));
	for(auto name: robot->mgiHand()->getLinkNames()) acm_.insert(std::pair<std::string, std::vector<uint8_t>>(name.c_str(), std::vector<uint8_t>()));

	for (auto link : robot->mgi()->getLinkNames()) rs_.insert_or_assign(link, std::vector<std::uint8_t>());
	rs_.insert_or_assign(robot->map()["left_finger"], std::vector<std::uint8_t>());
	rs_.insert_or_assign(robot->map()["right_finger"], std::vector<std::uint8_t>());
	rs_.insert_or_assign(robot->map()["hand_link"], std::vector<std::uint8_t>());
	rs_.insert_or_assign(robot->map()["base"], std::vector<std::uint8_t>());

	robots_.insert_or_assign(robot->name(), std::move(robot)); 
}


void MoveitMediator::setPanel(){
	auto wd = wing_reader_->wingData();

	for (const auto& s_r : robots_){
        try{
            auto ceti_bot = dynamic_cast<CetiRobot*>(s_r.second->next());
            ceti_bot->setObserverMask(static_cast<wing_config>(wd.at(s_r.first).second));
        } catch(const std::out_of_range& oor) {
            ROS_INFO("No mask data for %s", s_r.first.c_str());
        }
    }

	for (const auto& s_r : robots_){
        auto ceti_bot = dynamic_cast<CetiRobot*>(s_r.second->next());
        std::vector<std::unique_ptr<AbstractRobotElementDecorator>> panels(3);
            
        for (std::size_t j = 0; j < ceti_bot->observerMask().size(); j++){
            if (ceti_bot->observerMask()[j]){
                try{
                    tf2::Transform relative_tf = ceti_bot->tf().inverse() * wd.at(ceti_bot->name()).first[j].pose_;

                    std::unique_ptr<AbstractRobotElement> panel = std::make_unique<MoveitPanel>(wd.at(s_r.first).first[j].name_, relative_tf, wd.at(s_r.first).first[j].size_);
                    std::unique_ptr<AbstractRobotElementDecorator> log = std::make_unique<LogDecorator>(std::move(panel));
                    panels[j] = std::move(log);
                } catch (std::out_of_range &oor){
                    ROS_INFO("OOR in set_panel");
                }
            }
        }
        ceti_bot->setObservers(panels);
    }

	for (const auto& s_r : robots_){
		s_r.second->notify();
	}
}

void MoveitMediator::publishTables(){
    ros::WallDuration sleep_time(1.0);

	for (const auto& s_r : robots_){
		auto ceti_bot = dynamic_cast<CetiRobot*>(s_r.second->next());

		for (std::size_t k = 0; k < ceti_bot->observerMask().size(); k++){
			if(ceti_bot->observerMask()[k]){
				auto wmd = dynamic_cast<MoveitPanel*>(ceti_bot->observers()[k]->next());
				psi_->applyCollisionObject(wmd->marker());
				acm_.insert(std::pair<std::string, std::vector<uint8_t>>(wmd->marker().id.c_str(), std::vector<uint8_t>()));
				sleep_time.sleep();
			}
        }
    }
}

void MoveitMediator::mediate(){
	setPanel();
    publishTables();
	taskPlanner();
	ros::waitForShutdown();
}

void MoveitMediator::manipulateACM(AbstractRobotDecorator* mr, moveit_msgs::PlanningScene& ps){
	// First find ID from panda to start with
	std::regex rx_panda(mr->pattern());
	std::smatch match;
	std::regex_match(mr->name(), match, rx_panda);

	// build panda link regex
	std::stringstream ss;
	ss << "panda_" << match[1] << "_.*";
	std::regex rx_panda_links(ss.str());
	std::regex rx_box("box_.*");
	std::regex rx_table("table.*");


	// Iterate task collsion matrix
	for(int j = 0; j < ps.allowed_collision_matrix.entry_names.size(); j++ ){
		if( mr->map().at("base") == ps.allowed_collision_matrix.entry_names[j]){
			//In case an entry matches an robot spezific link

			for(int k = 0; k < ps.allowed_collision_matrix.entry_values[j].enabled.size(); k++){
				// For that specific entry, loop over values								
				int distance = std::distance(acm_.begin(),acm_.find(ps.allowed_collision_matrix.entry_names[k]));								

				if (std::regex_match(ps.allowed_collision_matrix.entry_names[k], match, rx_panda_links)){
					acm_.at(mr->map().at("base"))[distance] = ps.allowed_collision_matrix.entry_values[j].enabled[k];
				}

				if (std::regex_match(ps.allowed_collision_matrix.entry_names[k], match, rx_box)){
					acm_.at(mr->map().at("base"))[distance] = ps.allowed_collision_matrix.entry_values[j].enabled[k];
				}				

				if (std::regex_match(ps.allowed_collision_matrix.entry_names[k], match, rx_table)){
					acm_.at(mr->map().at("base"))[distance] = ps.allowed_collision_matrix.entry_values[j].enabled[k];
				}	

				if (mr->map().at("base")== ps.allowed_collision_matrix.entry_names[k]){
					acm_.at(mr->map().at("base"))[distance] = ps.allowed_collision_matrix.entry_values[j].enabled[k];
				}
			}
		}

		if(std::regex_match(ps.allowed_collision_matrix.entry_names[j], match, rx_panda_links)){
			//In case an entry matches an robot spezific link
			ROS_INFO("entry matches link %s at index %i", ps.allowed_collision_matrix.entry_names[j].c_str(), j);
			for(int k = 0; k < ps.allowed_collision_matrix.entry_values[j].enabled.size(); k++){
					//For that specific entry, loop over values							
				int distance = std::distance(acm_.begin(),acm_.find(ps.allowed_collision_matrix.entry_names[k]));								

				if (std::regex_match(ps.allowed_collision_matrix.entry_names[k], match, rx_panda_links)){
					acm_.at(ps.allowed_collision_matrix.entry_names[j])[distance] = ps.allowed_collision_matrix.entry_values[j].enabled[k];
				}

				if (std::regex_match(ps.allowed_collision_matrix.entry_names[k], match, rx_box)){
					acm_.at(ps.allowed_collision_matrix.entry_names[j])[distance] = ps.allowed_collision_matrix.entry_values[j].enabled[k];
				}				

				if (std::regex_match(ps.allowed_collision_matrix.entry_names[k], match, rx_table)){
					acm_.at(ps.allowed_collision_matrix.entry_names[j])[distance] = ps.allowed_collision_matrix.entry_values[j].enabled[k];
				}	

				if (mr->map().at("base")== ps.allowed_collision_matrix.entry_names[k]){
					acm_.at(ps.allowed_collision_matrix.entry_names[j])[distance] = ps.allowed_collision_matrix.entry_values[j].enabled[k];
				}
			}
		}

		if(std::regex_match(ps.allowed_collision_matrix.entry_names[j], match, rx_box)){
				//In case an entry matches an robot spezific link
			for(int k = 0; k < ps.allowed_collision_matrix.entry_values[j].enabled.size(); k++){
				//For that specific entry, loop over values							
				int distance = std::distance(acm_.begin(),acm_.find(ps.allowed_collision_matrix.entry_names[k]));								

				if (std::regex_match(ps.allowed_collision_matrix.entry_names[k], match, rx_panda_links)){
					acm_.at(ps.allowed_collision_matrix.entry_names[j])[distance] = ps.allowed_collision_matrix.entry_values[j].enabled[k];
				}

				if (std::regex_match(ps.allowed_collision_matrix.entry_names[k], match, rx_box)){
					acm_.at(ps.allowed_collision_matrix.entry_names[j])[distance] = ps.allowed_collision_matrix.entry_values[j].enabled[k];
				}				

				if (std::regex_match(ps.allowed_collision_matrix.entry_names[k], match, rx_table)){
					acm_.at(ps.allowed_collision_matrix.entry_names[j])[distance] = ps.allowed_collision_matrix.entry_values[j].enabled[k];
				}	

				if (mr->map().at("base")== ps.allowed_collision_matrix.entry_names[k]){
					acm_.at(ps.allowed_collision_matrix.entry_names[j])[distance] = ps.allowed_collision_matrix.entry_values[j].enabled[k];
				}
			}
		}

		if(std::regex_match(ps.allowed_collision_matrix.entry_names[j], match, rx_table)){
			//In case an entry matches an robot spezific link
			for(int k = 0; k < ps.allowed_collision_matrix.entry_values[j].enabled.size(); k++){
				//For that specific entry, loop over values									
				int distance = std::distance(acm_.begin(),acm_.find(ps.allowed_collision_matrix.entry_names[k]));								

				if (std::regex_match(ps.allowed_collision_matrix.entry_names[k], match, rx_panda_links)){
					acm_.at(ps.allowed_collision_matrix.entry_names[j])[distance] = ps.allowed_collision_matrix.entry_values[j].enabled[k];
				}

				if (std::regex_match(ps.allowed_collision_matrix.entry_names[k], match, rx_box)){
					acm_.at(ps.allowed_collision_matrix.entry_names[j])[distance] = ps.allowed_collision_matrix.entry_values[j].enabled[k];
				}				

				if (std::regex_match(ps.allowed_collision_matrix.entry_names[k], match, rx_table)){
					acm_.at(ps.allowed_collision_matrix.entry_names[j])[distance] = ps.allowed_collision_matrix.entry_values[j].enabled[k];
				}	

				if (mr->map().at("base")== ps.allowed_collision_matrix.entry_names[k]){
					acm_.at(ps.allowed_collision_matrix.entry_names[j])[distance] = ps.allowed_collision_matrix.entry_values[j].enabled[k];
				}
			}
		}
	}
}



void MoveitMediator::taskPlanner(){
	/* There are 2 ways to interprete a Task 
	1. A box position in acm is also the first entry in task, 
	2. A box position in acm is not the first entry in task, in that case we can might try for each position
	*/
	auto jq = job_reader_->robotData();
	auto cd = cuboid_reader_->cuboidBox();

	//std::vector<std::string> objs =  {"bottle1", "bottle2"};

	for (int i = 0; i < cd.size(); i ++){
		std::stringstream ss;
		ss << "box_" << i;

		moveit_msgs::CollisionObject item;
		item.id = ss.str();
		item.header.frame_id = "world";
		item.header.stamp = ros::Time::now();
		item.primitives.resize(1);
		item.primitives[0].type = item.primitives[0].BOX;
		item.primitives[0].dimensions.resize(3);
		item.primitives[0].dimensions[0] = cd[i].x_depth;
		item.primitives[0].dimensions[1] = cd[i].y_width;
		item.primitives[0].dimensions[2] = cd[i].z_heigth;

		item.primitive_poses.resize(1);
		item.primitive_poses[0].position.x = cd[i].Pose.position.x;
		item.primitive_poses[0].position.y = cd[i].Pose.position.y;
		item.primitive_poses[0].position.z = cd[i].Pose.position.z;
		item.primitive_poses[0].orientation.x = cd[i].Pose.orientation.x;
		item.primitive_poses[0].orientation.y = cd[i].Pose.orientation.y;
		item.primitive_poses[0].orientation.z = cd[i].Pose.orientation.z;
		item.primitive_poses[0].orientation.w = cd[i].Pose.orientation.w;
		item.operation = item.ADD;

		psi_->applyCollisionObject(item);
		acm_.insert(std::pair<std::string, std::vector<uint8_t>>(item.id, std::vector<uint8_t>()));

		// Could also safe id's somewhere
	}
	
	// Setup shared ACM
	for(auto& a: acm_) a.second.resize(acm_.size(), 0);

	std::regex item("box_([0-9]+)");
    std::smatch match;

	// ----------------------------------
	// GROOT Strategie => erst FROM TEXT
	// ----------------------------------

	BT::BehaviorTreeFactory factory;
	factory.registerNodeType<Execution>("Execution");
	factory.registerNodeType<PositionCondition>("PositionCondition");
	factory.registerNodeType<Parallel_robot>("Parallel_robot");

	std::map<const std::string, std::vector<std::tuple<const std::string, tf2::Vector3, std::vector<moveit_task_constructor_msgs::ExecuteTaskSolutionGoal>>>> task_but_different;

	while(!jq.empty()){
		for(auto& s_co : psi_->getObjects()){
			if(!std::regex_match(s_co.first, match, item)) continue;
			
			std::pair<std::string, job_data> temp = jq.front();
			ROS_INFO("1. job entry %f %f %f", temp.second.jobs_.front().getOrigin().getX(), temp.second.jobs_.front().getOrigin().getY(), temp.second.jobs_.front().getOrigin().getZ());
			ROS_INFO("object position %f %f %f", s_co.second.pose.position.x, s_co.second.pose.position.y, s_co.second.pose.position.z);
			if(tf2::tf2Distance2(temp.second.jobs_.front().getOrigin(), tf2::Vector3(s_co.second.pose.position.x, s_co.second.pose.position.y, s_co.second.pose.position.z )) == 0) {
				std::vector<moveit_task_constructor_msgs::ExecuteTaskSolutionGoal> bt_list;
				
				AbstractRobotDecorator* ard = nullptr;
				try{
					ard= robots_.at(temp.first).get();
				} catch (std::out_of_range& oor){
					ROS_INFO("Robot not found");
					ros::shutdown();
				}

				// loop jobs
				for (int k = 1; k < temp.second.jobs_.size(); k++){
					moveit::task_constructor::Task mgt = createTask(ard, psi_->getObjects().at(s_co.first), temp.second.jobs_[k]);
					if(mgt.plan(1)) {

						moveit_task_constructor_msgs::ExecuteTaskSolutionGoal e;
						mgt.solutions().front()->fillMessage(e.solution, &mgt.introspection());
						bt_list.push_back(e);

						moveit_msgs::CollisionObject temp_co = psi_->getObjects().at(s_co.first);
						temp_co.operation = temp_co.MOVE;
						temp_co.pose.position.x = temp.second.jobs_[k].getOrigin().getX();
						temp_co.pose.position.y = temp.second.jobs_[k].getOrigin().getY();
						temp_co.pose.position.z = temp.second.jobs_[k].getOrigin().getZ();
						temp_co.pose.orientation.x = temp.second.jobs_[k].getRotation().getX();
						temp_co.pose.orientation.y = temp.second.jobs_[k].getRotation().getY();
						temp_co.pose.orientation.z = temp.second.jobs_[k].getRotation().getZ();
						temp_co.pose.orientation.w = temp.second.jobs_[k].getRotation().getW();
						psi_->applyCollisionObject(temp_co);
					}
				}

				/*
				if(auto condition = dynamic_cast<Position_condition*>(node_it->get())) {ROS_INFO("om");condition->init(s_co.first, temp.second.jobs_.front().getOrigin(), psi_.get()); ++node_it;}

				for(auto& ets : bt_list)
					if(auto execution = dynamic_cast<Execution*>(node_it->get())) {ROS_INFO("tr");execution->init(planning_scene_diff_publisher_.get(), mr->mgi().get(), ets); ++node_it;}
				*/

				for (int i = 0; i < cd.size(); i ++){
					std::stringstream ss;
					ss << "box_" << i;

					moveit_msgs::CollisionObject item;
					item.id = ss.str();
					item.header.frame_id = "world";
					item.header.stamp = ros::Time::now();
					item.primitives.resize(1);
					item.primitives[0].type = item.primitives[0].BOX;
					item.primitives[0].dimensions.resize(3);
					item.primitives[0].dimensions[0] = cd[i].x_depth;
					item.primitives[0].dimensions[1] = cd[i].y_width;
					item.primitives[0].dimensions[2] = cd[i].z_heigth;

					item.pose.position.x = cd[i].Pose.position.x;
					item.pose.position.y = cd[i].Pose.position.y;
					item.pose.position.z = cd[i].Pose.position.z;
					item.pose.orientation.x = cd[i].Pose.orientation.x;
					item.pose.orientation.y = cd[i].Pose.orientation.y;
					item.pose.orientation.z = cd[i].Pose.orientation.z;
					item.pose.orientation.w = cd[i].Pose.orientation.w;
					item.operation = item.MOVE;

					psi_->applyCollisionObject(item);
					// Could also safe id's somewhere
				}

				
				try{
					task_but_different.at(ard->name()).push_back(std::tuple<std::string, tf2::Vector3, std::vector<moveit_task_constructor_msgs::ExecuteTaskSolutionGoal>>(s_co.first, temp.second.jobs_.front().getOrigin(), bt_list));
				} catch(std::out_of_range &oor) {
					std::tuple<const std::string&, tf2::Vector3&, std::vector<moveit_task_constructor_msgs::ExecuteTaskSolutionGoal>&> tuple(s_co.first, temp.second.jobs_.front().getOrigin(), bt_list);
					task_but_different.insert(std::pair<std::string, std::vector<std::tuple<const std::string, tf2::Vector3, std::vector<moveit_task_constructor_msgs::ExecuteTaskSolutionGoal>>>>(ard->name(),{tuple}));
				}
				jq.pop_front();
			} else {jq.push_back(temp);}
		}
	}

	std::stringstream ss;
	ss << "<root main_tree_to_execute = \"MainTree\">\n";
	ss << "<BehaviorTree ID=\"MainTree\">\n";
	ss << "<Control ID=\"Parallel\" name=\"Agents\" success_threshold=\""<< 2 << "\" failure_threshold=\""<< 1 << "\">\n";
	
	for (const auto& s_r : robots_){
		try {
			auto a = task_but_different.at(s_r.first);
		} catch (std::out_of_range& oor){
			continue;
		}
		ss << "<Control ID=\"Parallel_robot\" name=\""<< s_r.first << "\" success_threshold=\""<< task_but_different.at(s_r.first).size() << "\" failure_threshold=\""<< 1 << "\">\n";
		for (auto& p_obj: task_but_different.at(s_r.first)){
			ss << "<SequenceStar name=\"root_sequence\">\n";
			ss << "<Condition ID=\"PositionCondition\" name=\"Position_condition\"/>\n";
			for(int j = 0; j < std::get<2>(p_obj).size(); j++){
				ss << "<Action ID=\"Execution\" name=\"Execution\"/>\n";
			}
			ss << "</SequenceStar>\n";
		}
		ss << "</Control>\n";
    }
	ss << "</Control>\n";
	ss << "</BehaviorTree>\n";
	ss << "</root>\n";

	std::cout << ss.str();
	auto tree = factory.createTreeFromText(ss.str());
	auto node_it = tree.nodes.begin();

	for (const auto& s_r : robots_){
		try {
			auto a = task_but_different.at(s_r.first);
		} catch (std::out_of_range& oor){
			continue;
		}
		for (auto& p_obj: task_but_different.at(s_r.first)){
			while (node_it->get()->type() == BT::NodeType::CONTROL) ++node_it;
			if(node_it->get()->type() == BT::NodeType::ACTION) ROS_INFO("Action");
			if(node_it->get()->type() == BT::NodeType::CONDITION) ROS_INFO("Condition");
			if(node_it->get()->type() == BT::NodeType::CONTROL) ROS_INFO("Control");
			if(node_it->get()->type() == BT::NodeType::DECORATOR) ROS_INFO("Decorator");

			if(auto condition = dynamic_cast<PositionCondition*>(node_it->get())) {ROS_INFO("init Condition");condition->init(std::get<0>(p_obj), std::get<1>(p_obj), psi_.get());++node_it;}
			for(int j = 0; j < std::get<2>(p_obj).size(); j++){
				if(auto execution = dynamic_cast<Execution*>(node_it->get())) {ROS_INFO("init Action");execution->init(&executions_, s_r.second.get(), std::get<2>(p_obj)[j]);++node_it;}
			}
		}
    }

	BT::PublisherZMQ zmq(tree);
	ros::Duration sleep(1);
	BT::NodeStatus status = BT::NodeStatus::RUNNING;
	auto t_start = std::chrono::high_resolution_clock::now();
	while( status == BT::NodeStatus::RUNNING){
		status = tree.tickRoot();
		
      	// std::this_thread::sleep_for(std::chrono::milliseconds(100));


		std::vector<std::thread> th;
		moveit_msgs::PlanningScene ps_m;
		bool is_executing = !(executions_.empty());

		for (const auto& s_r : robots_){
			try{
				ROS_INFO("%s", s_r.first.c_str());
				auto temp = executions_.at(s_r.first);
				th.push_back(std::thread(&MoveitMediator::parallelExec, this, std::ref(*s_r.second.get()), temp.first));
				//mr->mgi()->execute(temp.first);
				// ROS_INFO("acm in msg");
				// for(int k = 0; k < temp.second.allowed_collision_matrix.entry_names.size(); k++){
				// 	ROS_INFO("%s", temp.second.allowed_collision_matrix.entry_names[k].c_str());
				// 	for (auto& enable : temp.second.allowed_collision_matrix.entry_values[k].enabled) std::cout << +enable << " ";
				// 	std::cout<< "\n";
				// }

				manipulateACM(s_r.second.get(), temp.second);
				mergePS(ps_m, temp.second, s_r.second.get());
				executions_.erase(s_r.first);
			} catch (std::out_of_range& oor){}
		}
		
		for(auto& t : th){
			t.join();
		}
		if (is_executing){
			mergeACM(ps_m);
			planning_scene_diff_publisher_->publish(ps_m);
		}
		

		// for(auto& exec : executions_){
		//  	for (int i = 0; i < robots_.size(); i++){
		//  		if (exec.first == robots_[i]->name()){
		//  			auto mr = dynamic_cast<Moveit_robot*>(robots_[i]);
	 	// 			manipulate_acm(mr, exec.second.second);
		//  			merge_ps(ps_m, exec.second.second, mr);

		//  		}
		// 	}
		// }

		
	}
	auto t_end = std::chrono::high_resolution_clock::now();
	double elapsed_time_ms = std::chrono::duration<double, std::milli>(t_end-t_start).count();
	ROS_INFO("%f", elapsed_time_ms);

	// clean up
	for (int i = 0; i < cd.size(); i ++){
		std::stringstream ss;
		ss << "box_" << i;

		moveit_msgs::CollisionObject item;
		item.id = ss.str();
		item.header.frame_id = "world";
		item.header.stamp = ros::Time::now();
		item.primitives.resize(1);
		item.primitives[0].type = item.primitives[0].BOX;
		item.primitives[0].dimensions.resize(3);
		item.primitives[0].dimensions[0] = cd[i].x_depth;
		item.primitives[0].dimensions[1] = cd[i].y_width;
		item.primitives[0].dimensions[2] = cd[i].z_heigth;

		item.pose.position.x = cd[i].Pose.position.x;
		item.pose.position.y = cd[i].Pose.position.y;
		item.pose.position.z = cd[i].Pose.position.z;
		item.pose.orientation.x = cd[i].Pose.orientation.x;
		item.pose.orientation.y = cd[i].Pose.orientation.y;
		item.pose.orientation.z = cd[i].Pose.orientation.z;
		item.pose.orientation.w = cd[i].Pose.orientation.w;
		item.operation = item.MOVE;

		psi_->applyCollisionObject(item);
		// Could also safe id's somewhere
	}
}


void MoveitMediator::mergeACM(moveit_msgs::PlanningScene& ps_m){
	moveit_msgs::PlanningScene::_allowed_collision_matrix_type acmt;
	acmt.entry_values.resize(acm_.size());

	int i = 0;
	for (auto& a : acm_){
		acmt.entry_names.push_back(a.first);
		acmt.entry_values[i].enabled = a.second;
		i++;
	}

	// ROS_INFO("merge_acm");
	// for(int k = 0; k < acmt.entry_names.size(); k++){
	// 	ROS_INFO("%s", acmt.entry_names[k].c_str());
	// 	for (auto& enable : acmt.entry_values[k].enabled){
	// 		std::cout << +enable << " ";
	// 	}
	// 	std::cout<< "\n";
	// }
	
	ps_m.allowed_collision_matrix = acmt;

}

void MoveitMediator::mergePS(moveit_msgs::PlanningScene& out, moveit_msgs::PlanningScene in, AbstractRobotDecorator* mr){
	// get full mr link list 

	for (auto ao : in.robot_state.attached_collision_objects) out.robot_state.attached_collision_objects.push_back(ao);
	out.robot_state.is_diff |= in.robot_state.is_diff;
	out.is_diff |= in.is_diff;
	out.robot_state.joint_state.header = in.robot_state.joint_state.header;
	out.robot_model_name = "panda";

	std::vector<std::string> links;
	for (auto link : mr->mgi()->getLinkNames()) links.push_back(link);
	links.push_back(mr->map()["left_finger"]);
	links.push_back(mr->map()["right_finger"]);
	links.push_back(mr->map()["hand_link"]);
	links.push_back(mr->map()["base"]);

	for (auto link : links) {
		for(int i = 0; i < in.robot_state.joint_state.name.size(); i++){
			if(link == in.robot_state.joint_state.name[i]){
				out.robot_state.joint_state.effort.push_back(in.robot_state.joint_state.effort[i]);
				out.robot_state.joint_state.position.push_back(in.robot_state.joint_state.position[i]);
				out.robot_state.joint_state.velocity.push_back(in.robot_state.joint_state.velocity[i]);
			}
		}

		for(int i = 0; i < in.link_padding.size(); i++){
			if(link == in.link_padding[i].link_name){
				out.link_padding.push_back(in.link_padding[i]);
			}
		}

		for(int i = 0; i < in.link_scale.size(); i++){
			if(link == in.link_scale[i].link_name){
				out.link_scale.push_back(in.link_scale[i]);
			}
		}
	}	
}

	
void MoveitMediator::parallelExec(AbstractRobotDecorator& mr, moveit_msgs::RobotTrajectory rt){
	mr.mgi()->execute(rt);
}

moveit::task_constructor::Task MoveitMediator::createTask(AbstractRobotDecorator* mr, moveit_msgs::CollisionObject& source, tf2::Transform& target){
    tf2::Transform t(tf2::Quaternion(source.pose.orientation.x, source.pose.orientation.y, source.pose.orientation.z, source.pose.orientation.w), tf2::Vector3(source.pose.position.x, source.pose.position.y, source.pose.position.z));
	std::string support_surface1 = "nichts";
    std::string support_surface2 = "nichts";

	for (const auto& s_r : robots_){
		std::string str;
		std::bitset<3> panel_mask;
        if(s_r.second->checkSingleObjectCollision(t, str, panel_mask)) support_surface1 = str;
        if(s_r.second->checkSingleObjectCollision(target, str, panel_mask)) support_surface2= str;
	}

	ROS_INFO("ss1 %s", support_surface1.c_str());
	ROS_INFO("ss2 %s", support_surface2.c_str());


	const std::string object = source.id;
    moveit::task_constructor::Task task_;

	std::string name = "Pick&Place";
	task_.stages()->setName(name + std::to_string(static_cast<int>(ros::Time::now().toNSec())));
	task_.loadRobotModel();
	task_.setRobotModel(mr->mgi()->getRobotModel());

	// Set task properties
	task_.setProperty("group", mr->name());
	task_.setProperty("eef", mr->map()["eef_name"]);
	task_.setProperty("hand", mr->map()["hand_group_name"]);
	task_.setProperty("hand_grasping_frame", mr->map()["hand_frame"]);
	task_.setProperty("ik_frame", mr->map()["hand_frame"]);

	moveit::task_constructor::Stage* current_state_ptr = nullptr;  
	{
		auto current_state = std::make_unique< moveit::task_constructor::stages::CurrentState>("current state");
		auto applicability_filter = std::make_unique< moveit::task_constructor::stages::PredicateFilter>("applicability test", std::move(current_state));
		applicability_filter->setPredicate([object](const moveit::task_constructor::SolutionBase& s, std::string& comment) {
			if (s.start()->scene()->getCurrentState().hasAttachedBody(object)) {
				comment = "object with id '" + object + "' is already attached and cannot be picked";
				return false;
			}
			return true;
		});

		current_state_ptr = applicability_filter.get();
		task_.add(std::move(applicability_filter));
	}

	{  // Open Hand
		auto stage = std::make_unique< moveit::task_constructor::stages::MoveTo>("open hand", sampling_planner_);
		stage->setGroup(mr->map()["eef_name"]);
		stage->setGoal("open");
		task_.add(std::move(stage));
	}

	{  // Move-to pre-grasp
		auto stage = std::make_unique< moveit::task_constructor::stages::Connect>(
		    "move to pick",  moveit::task_constructor::stages::Connect::GroupPlannerVector{ { mr->name(),  sampling_planner_} });
		stage->setTimeout(5.0);
		stage->properties().configureInitFrom(moveit::task_constructor::Stage::PARENT);
		task_.add(std::move(stage));
	}

	 moveit::task_constructor::Stage* attach_object_stage = nullptr;  // Forward attach_object_stage to place pose generator
	{
		auto grasp = std::make_unique<moveit::task_constructor::SerialContainer>("pick object");
		task_.properties().exposeTo(grasp->properties(), { "eef", "hand", "group", "ik_frame" });
		grasp->properties().configureInitFrom(moveit::task_constructor::Stage::PARENT, { "eef", "hand", "group", "ik_frame" });


		{	// Approach Obj
			auto stage = std::make_unique<moveit::task_constructor::stages::MoveRelative>("approach object", cartesian_planner_);
			stage->properties().set("marker_ns", "approach_object");
			stage->properties().set("link", mr->map()["hand_frame"]);
			stage->properties().configureInitFrom(moveit::task_constructor::Stage::PARENT, { "group" });
			stage->setMinMaxDistance(0.07, 0.2);

			// Set hand forward direction
			geometry_msgs::Vector3Stamped vec;
			vec.header.frame_id = mr->map()["hand_frame"];
			vec.vector.z = 1.0;
			stage->setDirection(vec);
			grasp->insert(std::move(stage));
		}

		{
			// Sample grasp pose
			auto stage = std::make_unique<moveit::task_constructor::stages::GenerateGraspPose>("generate grasp pose");
			stage->properties().configureInitFrom(moveit::task_constructor::Stage::PARENT);
			stage->properties().set("marker_ns", "grasp_pose");
			stage->setPreGraspPose("open");
			stage->setObject(object);
			stage->setAngleDelta(M_PI / 2);
			stage->setMonitoredStage(current_state_ptr);  // Hook into current state

			// Compute IK
			Eigen::Quaterniond eigen = Eigen::AngleAxisd(1.571f, Eigen::Vector3d::UnitX()) *
                                   Eigen::AngleAxisd(0.785f, Eigen::Vector3d::UnitY()) *
                                   Eigen::AngleAxisd(1.571f, Eigen::Vector3d::UnitZ());
			Eigen::Translation3d trans(0.1f, 0, 0);
			Eigen::Isometry3d ik = eigen * trans;

			auto wrapper = std::make_unique<moveit::task_constructor::stages::ComputeIK>("grasp pose IK", std::move(stage));
			wrapper->setMaxIKSolutions(8);
			wrapper->setMinSolutionDistance(1.0);
			wrapper->setIKFrame(ik, mr->map()["hand_frame"]);
			wrapper->properties().configureInitFrom(moveit::task_constructor::Stage::PARENT, { "eef", "group" });
			wrapper->properties().configureInitFrom(moveit::task_constructor::Stage::INTERFACE, { "target_pose" });
			grasp->insert(std::move(wrapper));
		}

		{
			auto stage = std::make_unique<moveit::task_constructor::stages::ModifyPlanningScene>("allow collision (hand,object)");
			stage->allowCollisions(
			    object, task_.getRobotModel()->getJointModelGroup(mr->map()["eef_name"])->getLinkModelNamesWithCollisionGeometry(),
			    true);
			grasp->insert(std::move(stage));
		}

		{
			auto stage = std::make_unique<moveit::task_constructor::stages::MoveTo>("close hand", sampling_planner_);
			stage->setGroup(mr->map()["eef_name"]);
			stage->setGoal("close");
			grasp->insert(std::move(stage));
		}

		{	// Attach obj
			auto stage = std::make_unique<moveit::task_constructor::stages::ModifyPlanningScene>("attach object");
			stage->attachObject(object, mr->map()["hand_frame"]);
			attach_object_stage = stage.get();
			grasp->insert(std::move(stage));
		}

		{	// Allow Collision obj table
			auto stage = std::make_unique<moveit::task_constructor::stages::ModifyPlanningScene>("allow collision (object,support)");
			stage->allowCollisions({ object }, support_surface1, true);
			grasp->insert(std::move(stage));
		}

		{
			auto stage = std::make_unique<moveit::task_constructor::stages::MoveRelative>("lift object", cartesian_planner_);
			stage->properties().configureInitFrom(moveit::task_constructor::Stage::PARENT, { "group" });
			stage->setMinMaxDistance(0.1, 0.2);
			stage->setIKFrame(mr->map()["hand_frame"]);
			stage->properties().set("marker_ns", "lift_object");

			// Set upward direction
			geometry_msgs::Vector3Stamped vec;
			vec.header.frame_id = "world";
			vec.vector.z = 1.0;
			stage->setDirection(vec);
			grasp->insert(std::move(stage));
		}

		{	// forbid collision
			auto stage = std::make_unique<moveit::task_constructor::stages::ModifyPlanningScene>("forbid collision (object,surface)");
			stage->allowCollisions({ object }, support_surface1, false);
			grasp->insert(std::move(stage));
		}

		// Add grasp container to task
		task_.add(std::move(grasp));
	}

	{
		auto stage = std::make_unique<moveit::task_constructor::stages::Connect>(
		    "move to place", moveit::task_constructor::stages::Connect::GroupPlannerVector{ { mr->name(), sampling_planner_ } });
		stage->setTimeout(5.0);
		stage->properties().configureInitFrom(moveit::task_constructor::Stage::PARENT);
		task_.add(std::move(stage));
	}

	{
		auto place = std::make_unique<moveit::task_constructor::SerialContainer>("place object");
		task_.properties().exposeTo(place->properties(), { "eef", "hand", "group" });
		place->properties().configureInitFrom(moveit::task_constructor::Stage::PARENT, { "eef", "hand", "group" });

		{
		auto stage = std::make_unique<moveit::task_constructor::stages::ModifyPlanningScene>("allow cokbkmomsurface)");
		stage->allowCollisions( {object} , support_surface2, true);
		place->insert(std::move(stage));
		}


		{
			auto stage = std::make_unique<moveit::task_constructor::stages::MoveRelative>("lower object", cartesian_planner_);
			stage->properties().set("marker_ns", "lower_object");
			stage->properties().set("link", mr->map()["hand_frame"]);
			stage->properties().configureInitFrom(moveit::task_constructor::Stage::PARENT, { "group" });
			stage->setMinMaxDistance(.03, .13);

			// Set downward direction
			geometry_msgs::Vector3Stamped vec;
			vec.header.frame_id = "world";
			vec.vector.z = -1.0;
			stage->setDirection(vec);
			place->insert(std::move(stage));
		}

		{
			// Generate Place Pose
			auto stage = std::make_unique<moveit::task_constructor::stages::GeneratePlacePose>("generate place pose");
			stage->properties().configureInitFrom(moveit::task_constructor::Stage::PARENT, { "ik_frame" });
			stage->properties().set("marker_ns", "place_pose");
			stage->setObject(object);

			// Set target pose
			geometry_msgs::PoseStamped p;
			p.header.frame_id = "world";
			p.pose.position.x = target.getOrigin().getX();
			p.pose.position.y = target.getOrigin().getY();
			p.pose.position.z = target.getOrigin().getZ();
			p.pose.orientation.x = target.getRotation().getX();
			p.pose.orientation.y = target.getRotation().getY();
			p.pose.orientation.z = target.getRotation().getZ();
			p.pose.orientation.w = target.getRotation().getW();



			stage->setPose(p);
			stage->setMonitoredStage(attach_object_stage);  // Hook into attach_object_stage

			// Compute IK
			Eigen::Quaterniond eigen = Eigen::AngleAxisd(1.571f, Eigen::Vector3d::UnitX()) *
                                   Eigen::AngleAxisd(0.785f, Eigen::Vector3d::UnitY()) *
                                   Eigen::AngleAxisd(1.571f, Eigen::Vector3d::UnitZ());
			Eigen::Translation3d trans(0.1f, 0, 0);
			Eigen::Isometry3d ik = eigen * trans;
			auto wrapper = std::make_unique<moveit::task_constructor::stages::ComputeIK>("place pose IK", std::move(stage));
			wrapper->setMaxIKSolutions(2);
			wrapper->setIKFrame(ik, mr->map()["hand_frame"]);
			wrapper->properties().configureInitFrom(moveit::task_constructor::Stage::PARENT, { "eef", "group" });
			wrapper->properties().configureInitFrom(moveit::task_constructor::Stage::INTERFACE, { "target_pose" });
			place->insert(std::move(wrapper));
		}

		{
			auto stage = std::make_unique<moveit::task_constructor::stages::MoveTo>("open hand", sampling_planner_);
			stage->setGroup(mr->map()["eef_name"]);
			stage->setGoal("open");
			place->insert(std::move(stage));
		}

		{
			auto stage = std::make_unique<moveit::task_constructor::stages::ModifyPlanningScene>("allow collision (hand,object)");
			stage->allowCollisions(
			    object, task_.getRobotModel()->getJointModelGroup(mr->map()["eef_name"])->getLinkModelNamesWithCollisionGeometry(),
			    false);
			place->insert(std::move(stage));
		}

		{
			auto stage = std::make_unique<moveit::task_constructor::stages::ModifyPlanningScene>("detach object");
			stage->detachObject(object, mr->map()["hand_frame"]);
			place->insert(std::move(stage));
		}

		{
			auto stage = std::make_unique<moveit::task_constructor::stages::MoveRelative>("retreat after place", cartesian_planner_);
			stage->properties().configureInitFrom(moveit::task_constructor::Stage::PARENT, { "group" });
			stage->setMinMaxDistance(.1, .2);
			stage->setIKFrame(mr->map()["hand_frame"]);
			stage->properties().set("marker_ns", "retreat");
			geometry_msgs::Vector3Stamped vec;
			vec.header.frame_id = mr->map()["hand_frame"];
			vec.vector.z = -1.0;
			stage->setDirection(vec);
			place->insert(std::move(stage));
		}

		{
			auto stage = std::make_unique<moveit::task_constructor::stages::MoveTo>("close hand", sampling_planner_);
			stage->setGroup(mr->map()["eef_name"]);
			stage->setGoal("close");
			place->insert(std::move(stage));
		}

		// Add place container to task
		task_.add(std::move(place));
	}

	{
		auto stage = std::make_unique<moveit::task_constructor::stages::MoveTo>("move home", sampling_planner_);
		stage->properties().configureInitFrom(moveit::task_constructor::Stage::PARENT, { "group" });
		stage->setGoal("ready");
		stage->restrictDirection(moveit::task_constructor::stages::MoveTo::FORWARD);
		task_.add(std::move(stage));
	}

	return task_;
}



