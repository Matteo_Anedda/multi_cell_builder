#include "mediator/abstract_mediator.h"
#include "mediator/grasp_mediator.h"
#include "robot/decorators/panda_decorator.h"
#include <gb_grasp/GraspPipelineDemo.h>



int main(int argc, char *argv[]) {

    ros::init(argc, argv, "config_routine"); 
    std::shared_ptr<ros::NodeHandle> n(new ros::NodeHandle);

    // Allow the action server to recieve and send ros messages
    ros::AsyncSpinner spinner(2);
    spinner.start();

    std::shared_ptr<GraspMediator> mediator = std::make_shared<GraspMediator>(n);

    auto rd = mediator->robotReader()->robotData();
    for (int i = 0; i < rd.size() ;i++){
        std::unique_ptr<AbstractRobot> ceti = std::make_unique<CetiRobot>(rd[i].name_, rd[i].pose_, rd[i].size_);
        std::unique_ptr<PandaDecorator> ceti_panda = std::make_unique<PandaDecorator>(std::move(ceti));
        mediator->connectRobots(std::move(ceti_panda));
    }

    mediator->mediate();

    while (ros::ok()){
        ros::spinOnce();
    }
    return 0;
}