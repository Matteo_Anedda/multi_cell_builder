#include "robot/ceti_robot.h"
#include <regex>

CetiRobot::CetiRobot(std::string& name, tf2::Transform tf, tf2::Vector3 size) 
: AbstractRobot(name, tf, size)
, observer_mask_(std::bitset<3>(0))
{}


bool CetiRobot::checkSingleObjectCollision(tf2::Transform& obj, std::string& str, std::bitset<3>& panel_mask){

    // 1. Root joint check
    tf2::Transform A = tf_ * root_tf_ * robot_root_bounds_[0];
    tf2::Transform B = tf_ * root_tf_ * robot_root_bounds_[1];
    tf2::Transform C = tf_ * root_tf_ * robot_root_bounds_[2];
    tf2::Transform D = tf_ * root_tf_ * robot_root_bounds_[3];

    float full_area = areaCalculation(A,B,C) + areaCalculation(A,D,C);
    float sum = areaCalculation(A,obj,D) + areaCalculation(D,obj,C) + areaCalculation(C,obj,B) + areaCalculation(obj, B, A);
    if ((std::floor(sum*1000)/1000.f) <= full_area) {return false; } 
    
    // 2. table_top check
    std::stringstream ss;
    std::regex rx("panda_arm([0-9]+)");
    std::smatch match;
    std::regex_match(str, match, rx);

    ss << "base_" << match[1]; 
    A = tf_ * bounds_[0];
    B = tf_ * bounds_[1];
    C = tf_ * bounds_[2];
    D = tf_ * bounds_[3];

    full_area = areaCalculation(A,B,C) + areaCalculation(A,D,C);
    sum = areaCalculation(A,obj,D) + areaCalculation(D,obj,C) + areaCalculation(C,obj,B) + areaCalculation(obj, B, A);
    if ((std::floor(sum*1000)/1000.f) <= full_area){
        str = ss.str();
        return true;
    }

    // 3. panel collsision check
    
    for(std::size_t i = 0; i < observers_.size(); i++){
        if(observer_mask_[i] & panel_mask[i]){
            tf2::Transform A = observers_[i]->worldTf() * observers_[i]->bounds()[0];
            tf2::Transform B = observers_[i]->worldTf() * observers_[i]->bounds()[1];
            tf2::Transform C = observers_[i]->worldTf() * observers_[i]->bounds()[2];
            tf2::Transform D = observers_[i]->worldTf() * observers_[i]->bounds()[3];

            full_area = areaCalculation(A,B,C) + areaCalculation(A,D,C);
            sum = areaCalculation(A,obj,D) + areaCalculation(D,obj,C) + areaCalculation(C,obj,B) + areaCalculation(obj, B, A);
            if ((std::floor(sum*1000)/1000.f) <= full_area) {
                str = observers_[i]->name();
                return true;
            } 
        }
    }
    
    return false;
}

void CetiRobot::reset(){
    observers_.clear();
    tf_ = tf2::Transform(tf2::Quaternion(0,0,0,1), tf2::Vector3(0,0, tf().getOrigin().getZ()));
}

void CetiRobot::notify(){ 
    for(int i = 0; i < observers_.size(); i++) if(observers_[i]) observers_[i]->update(tf_); 
    // for(int i = 0; i < access_fields_.size(); i++) access_fields_[i]->update(tf_);
}

bool CetiRobot::inCollision(CetiRobot* rob){
    tf2::Transform A = tf_ * bounds_[0];
    tf2::Transform B = tf_ * bounds_[1];
    tf2::Transform C = tf_ * bounds_[2];
    tf2::Transform D = tf_ * bounds_[3];

    float full_area = areaCalculation(A,B,C) + areaCalculation(A,D,C);
    float sum = areaCalculation(A,rob->tf(),D) + areaCalculation(D,rob->tf(),C) + areaCalculation(C,rob->tf(),B) + areaCalculation(rob->tf(), B, A);
    if ((std::floor(sum*100)/100.f) <= full_area) return true;

    for(int i = 0; i < rob->observers().size(); i++) {
        tf2::Transform WA = rob->observers()[i]->worldTf() * rob->observers()[i]->bounds()[0];
        tf2::Transform WB = rob->observers()[i]->worldTf() * rob->observers()[i]->bounds()[1];
        tf2::Transform WC = rob->observers()[i]->worldTf() * rob->observers()[i]->bounds()[2];
        tf2::Transform WD = rob->observers()[i]->worldTf() * rob->observers()[i]->bounds()[3];
        sum = areaCalculation(A,WA,D) + areaCalculation(D,WA,C) + areaCalculation(C,WA,B) + areaCalculation(WA, B, A); if ((std::floor(sum*100)/100.f) <= full_area) return true;
        sum = areaCalculation(A,WB,D) + areaCalculation(D,WB,C) + areaCalculation(C,WB,B) + areaCalculation(WB, B, A); if ((std::floor(sum*100)/100.f) <= full_area) return true;
        sum = areaCalculation(A,WC,D) + areaCalculation(D,WC,C) + areaCalculation(C,WC,B) + areaCalculation(WC, B, A); if ((std::floor(sum*100)/100.f) <= full_area) return true;
        sum = areaCalculation(A,WD,D) + areaCalculation(D,WD,C) + areaCalculation(C,WD,B) + areaCalculation(WD, B, A); if ((std::floor(sum*100)/100.f) <= full_area) return true;
    }

    for (int i = 0; i < observers_.size(); i++){
        tf2::Transform WA = observers_[i]->worldTf() * observers_[i]->bounds()[0];
        tf2::Transform WB = observers_[i]->worldTf() * observers_[i]->bounds()[1];
        tf2::Transform WC = observers_[i]->worldTf() * observers_[i]->bounds()[2];
        tf2::Transform WD = observers_[i]->worldTf() * observers_[i]->bounds()[3];

        full_area = areaCalculation(WA,WB,WC) + areaCalculation(WA,WD,WC);
        for(int j = 0; j < rob->observers().size(); j++) {
            tf2::Transform WA2 = rob->observers()[j]->worldTf() * observers_[j]->bounds()[0];
            tf2::Transform WB2 = rob->observers()[j]->worldTf() * observers_[j]->bounds()[1];
            tf2::Transform WC2 = rob->observers()[j]->worldTf() * observers_[j]->bounds()[2];
            tf2::Transform WD2 = rob->observers()[j]->worldTf() * observers_[j]->bounds()[3];
            sum = areaCalculation(WA,WA2,WD) + areaCalculation(WD,WA2,WC) + areaCalculation(WC,WA2,WB) + areaCalculation(WA2, WB, WA); if ((std::floor(sum*100)/100.f) <= full_area) return true;
            sum = areaCalculation(WA,WB2,WD) + areaCalculation(WD,WB2,WC) + areaCalculation(WC,WB2,WB) + areaCalculation(WB2, WB, WA); if ((std::floor(sum*100)/100.f) <= full_area) return true;
            sum = areaCalculation(WA,WC2,WD) + areaCalculation(WD,WC2,WC) + areaCalculation(WC,WC2,WB) + areaCalculation(WC2, WB, WA); if ((std::floor(sum*100)/100.f) <= full_area) return true;
            sum = areaCalculation(WA,WD2,WD) + areaCalculation(WD,WD2,WC) + areaCalculation(WC,WD2,WB) + areaCalculation(WD2, WB, WA); if ((std::floor(sum*100)/100.f) <= full_area) return true;
        }
    }
    return false;
}
