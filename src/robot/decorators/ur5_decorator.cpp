#include "robot/decorators/ur5_decorator.h"
#include <regex>

UR5Decorator::UR5Decorator(std::unique_ptr<AbstractRobot> next) 
    : AbstractRobotDecorator(std::move(next))
    {
        spezifieRootBounds();
        // spezifie_robot_groups();


    }

void UR5Decorator::spezifieRootBounds(){
    // panda root
    next_->robotRootBounds().clear();
    next_->robotRootBounds().push_back(tf2::Transform(tf2::Quaternion(0,0,0,1), tf2::Vector3(0.0745f, 0.0745f, 0))); // ++
    next_->robotRootBounds().push_back(tf2::Transform(tf2::Quaternion(0,0,0,1), tf2::Vector3(-0.0745f, 0.0745f, 0))); // +-
    next_->robotRootBounds().push_back(tf2::Transform(tf2::Quaternion(0,0,0,1), tf2::Vector3(-0.0745f, -0.0745f, 0)));
    next_->robotRootBounds().push_back(tf2::Transform(tf2::Quaternion(0,0,0,1), tf2::Vector3(0.0745f, -0.0745f, 0)));
}

void UR5Decorator::spezifieRobotGroups(){
    std::stringstream hand_n, ik_frame_n, name_n, base_n;

    std::regex panda_id("panda_arm([0-9]+)"), left_finger("left_finger"), right_finger("right_finger"), hand_link("hand_link");
    std::smatch match;
    std::regex_match(next_->name(), match, panda_id);

    hand_n << "hand_" << match[1];
    ik_frame_n << "panda_" << match[1] << "_link8";
    base_n << "base_" << match[1];

    for (auto& link : mgi_hand_->getLinkNames()){
        if (std::regex_match(link, match, left_finger)) map_.insert(std::pair<std::string, std::string>("left_finger", link));
        if (std::regex_match(link, match, right_finger)) map_.insert(std::pair<std::string, std::string>("right_finger", link));
        if (std::regex_match(link, match, hand_link)) map_.insert(std::pair<std::string, std::string>("hand_link", link));
    }

    mgi_hand_ = std::make_shared<moveit::planning_interface::MoveGroupInterface>(hand_n.str());

    map_.insert(std::make_pair<std::string, std::string>("eef_name", hand_n.str()));
    map_.insert(std::make_pair<std::string, std::string>("hand_frame", ik_frame_n.str()));
    map_.insert(std::make_pair<std::string, std::string>("hand_group_name", hand_n.str()));
    map_.insert(std::make_pair<std::string, std::string>("base", base_n.str()));
}
