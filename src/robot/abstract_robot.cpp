#include "robot/abstract_robot.h"


AbstractRobot::AbstractRobot(std::string name, tf2::Transform tf, tf2::Vector3 size) 
: name_(name)
, tf_(tf)
, size_(size){
    root_tf_= tf2::Transform(tf2::Quaternion(0,0,0,1), tf2::Vector3(-0.22f, 0, tf.getOrigin().getZ()));

    bounds_.push_back(tf2::Transform(tf2::Quaternion(0,0,0,1), tf2::Vector3( size.getX()*0.5f, size.getY()*0.5f, tf.getOrigin().getZ()))); // ++
    bounds_.push_back(tf2::Transform(tf2::Quaternion(0,0,0,1), tf2::Vector3(-size.getX()*0.5f, size.getY()*0.5f, tf.getOrigin().getZ()))); // +-
    bounds_.push_back(tf2::Transform(tf2::Quaternion(0,0,0,1), tf2::Vector3(-size.getX()*0.5f, -size.getY()*0.5f, tf.getOrigin().getZ()))); //-+
    bounds_.push_back(tf2::Transform(tf2::Quaternion(0,0,0,1), tf2::Vector3( size.getX()*0.5f, -size.getY()*0.5f, tf.getOrigin().getZ()))); // --
    
    // default root
    robot_root_bounds_.push_back(tf2::Transform(tf2::Quaternion(0,0,0,1), tf2::Vector3(0.1f, 0.1f, 0))); // ++
    robot_root_bounds_.push_back(tf2::Transform(tf2::Quaternion(0,0,0,1), tf2::Vector3(-0.1f, 0.1f, 0))); // +-
    robot_root_bounds_.push_back(tf2::Transform(tf2::Quaternion(0,0,0,1), tf2::Vector3(-0.1f, -0.1f, 0)));
    robot_root_bounds_.push_back(tf2::Transform(tf2::Quaternion(0,0,0,1), tf2::Vector3(0.1f, -0.1f, 0)));
}

float AbstractRobot::areaCalculation(tf2::Transform& A, tf2::Transform& B, tf2::Transform& C){
    return std::abs(
                    (B.getOrigin().getX() * A.getOrigin().getY()) - (A.getOrigin().getX() * B.getOrigin().getY()) +
                    (C.getOrigin().getX() * B.getOrigin().getY()) - (B.getOrigin().getX() * C.getOrigin().getY()) +
                    (A.getOrigin().getX() * C.getOrigin().getY()) - (C.getOrigin().getX() * A.getOrigin().getY()))*0.5f;
}