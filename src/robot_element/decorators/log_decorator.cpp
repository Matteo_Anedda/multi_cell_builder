#include "robot_element/decorators/log_decorator.h"

LogDecorator::LogDecorator(std::unique_ptr<AbstractRobotElement> next) 
: AbstractRobotElementDecorator(std::move(next))
{
    ROS_INFO("LOG Decorator: %s", next_->name().c_str());
}

void LogDecorator::inputFilter() {
    ROS_INFO("--- Debug: %s --- State: PRE UPDATE", next_->name().c_str());
    ROS_INFO("=> Debug: pos('%f', '%f', '%f')", worldTf().getOrigin().getX(), worldTf().getOrigin().getY(), worldTf().getOrigin().getZ());
    ROS_INFO("=> Debug: orientation('%f', '%f', '%f', '%f')", worldTf().getRotation().getX(), worldTf().getRotation().getY(), worldTf().getRotation().getZ(), worldTf().getRotation().getW());
}

void LogDecorator::update(tf2::Transform& tf) {
    inputFilter();
    AbstractRobotElementDecorator::update(tf);
    outputFilter();
}

void LogDecorator::outputFilter() {
    ROS_INFO("--- Debug: %s --- State: POST UPDATE", next_->name().c_str());
    ROS_INFO("=> Debug: pos('%f', '%f', '%f')", worldTf().getOrigin().getX(), worldTf().getOrigin().getY(), worldTf().getOrigin().getZ());
    ROS_INFO("=> Debug: orientation('%f', '%f', '%f', '%f')", worldTf().getRotation().getX(), worldTf().getRotation().getY(), worldTf().getRotation().getZ(), worldTf().getRotation().getW());
}

