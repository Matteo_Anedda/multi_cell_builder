#include "robot_element/observers/rviz_panel.h"


RvizPanel::RvizPanel(std::string name, tf2::Transform tf, tf2::Vector3 size) 
: Panel(name, tf, size)
{
    marker_.header.frame_id = "map";
    marker_.header.stamp = ros::Time();
    marker_.ns = name_;
    marker_.id = 1;
    marker_.type = visualization_msgs::Marker::CUBE;
    marker_.action = visualization_msgs::Marker::ADD;
    marker_.pose.position.x = 0;
    marker_.pose.position.y = 0;
    marker_.pose.position.z = 0;
    marker_.pose.orientation.x = 0;
    marker_.pose.orientation.y = 0;
    marker_.pose.orientation.z = 0;
    marker_.pose.orientation.w = 1;
    marker_.scale.x = size_.getX();
    marker_.scale.y = size_.getY();
    marker_.scale.z = size_.getZ();
    marker_.color.r = 1.0;
    marker_.color.g = 1.0;
    marker_.color.b = 1.0;
    marker_.color.a = 0.0; 
};

void RvizPanel::update(tf2::Transform& tf){
    Panel::update(tf);

    marker_.header.frame_id = "map";
    marker_.header.stamp = ros::Time();
    marker_.ns = name_;
    marker_.id = 1;
    marker_.type = visualization_msgs::Marker::CUBE;
    marker_.action = visualization_msgs::Marker::MODIFY;
    marker_.pose.position.x = world_tf_.getOrigin().getX();
    marker_.pose.position.y = world_tf_.getOrigin().getY();
    marker_.pose.position.z = world_tf_.getOrigin().getZ() - size_.getZ()/2;
    marker_.pose.orientation.x = world_tf_.getRotation().getX();
    marker_.pose.orientation.y = world_tf_.getRotation().getY();
    marker_.pose.orientation.z = world_tf_.getRotation().getZ();
    marker_.pose.orientation.w = world_tf_.getRotation().getW();
    marker_.scale.x = size_.getX();
    marker_.scale.y = size_.getY();
    marker_.scale.z = size_.getZ();
    marker_.color.r = 1.0;
    marker_.color.g = 1.0;
    marker_.color.b = 1.0;
    // marker_.color.a = 1.0; 
}