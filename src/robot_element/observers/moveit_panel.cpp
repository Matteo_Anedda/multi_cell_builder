#include "robot_element/observers/moveit_panel.h"


MoveitPanel::MoveitPanel(std::string name, tf2::Transform tf, tf2::Vector3 size) 
: Panel(name, tf, size)
{
    /*
    marker_.id = name_;
    marker_.header.frame_id = "world";

    marker_.primitives.resize(1);
    marker_.primitives[0].type = marker_.primitives[0].BOX;
    marker_.primitives[0].dimensions.resize(3);
    marker_.primitives[0].dimensions[0] = size_.getX();
    marker_.primitives[0].dimensions[1] = size_.getY();
    marker_.primitives[0].dimensions[2] = size_.getZ();


    marker_.primitive_poses.resize(1);
    marker_.primitive_poses[0].position.x = world_tf_.getOrigin().getX();
    marker_.primitive_poses[0].position.y = world_tf_.getOrigin().getY();
    marker_.primitive_poses[0].position.z = world_tf_.getOrigin().getZ() - size_.getZ()/2;
    marker_.primitive_poses[0].orientation.x = world_tf_.getRotation().getX();
    marker_.primitive_poses[0].orientation.y = world_tf_.getRotation().getY();
    marker_.primitive_poses[0].orientation.z = world_tf_.getRotation().getZ();
    marker_.primitive_poses[0].orientation.w = world_tf_.getRotation().getW();

    marker_.operation = marker_.ADD;
    */
};

void MoveitPanel::update(tf2::Transform& tf){
    Panel::update(tf);

    marker_.id = name_;
    marker_.header.frame_id = "world";

    /*
    marker_.pose.position.x = world_tf_.getOrigin().getX();
    marker_.pose.position.y = world_tf_.getOrigin().getY();
    marker_.pose.position.x = world_tf_.getOrigin().getZ() - size_.getZ()/2;;

    marker_.pose.orientation.x = world_tf_.getRotation().getX();
    marker_.pose.orientation.x = world_tf_.getRotation().getY();
    marker_.pose.orientation.x = world_tf_.getRotation().getZ();
    marker_.pose.orientation.x = world_tf_.getRotation().getW();
    */

    marker_.primitives.resize(1);
    marker_.primitives[0].type = marker_.primitives[0].BOX;
    marker_.primitives[0].dimensions.resize(3);
    marker_.primitives[0].dimensions[0] = size_.getX();
    marker_.primitives[0].dimensions[1] = size_.getY();
    marker_.primitives[0].dimensions[2] = size_.getZ();

    marker_.primitive_poses.resize(1);
    marker_.primitive_poses[0].position.x = world_tf_.getOrigin().getX();
    marker_.primitive_poses[0].position.y = world_tf_.getOrigin().getY();
    marker_.primitive_poses[0].position.z = world_tf_.getOrigin().getZ() - size_.getZ()/2;
    marker_.primitive_poses[0].orientation.x = world_tf_.getRotation().getX();
    marker_.primitive_poses[0].orientation.y = world_tf_.getRotation().getY();
    marker_.primitive_poses[0].orientation.z = world_tf_.getRotation().getZ();
    marker_.primitive_poses[0].orientation.w = world_tf_.getRotation().getW();

    marker_.operation = marker_.ADD;
}

