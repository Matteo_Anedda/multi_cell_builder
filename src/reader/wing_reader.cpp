#include "reader/wing_reader.h"

void WingReader::read(){
    XmlRpc::XmlRpcValue resources;
    nh_->getParam("/objects", resources);

    std::regex rx("table([0-9]+)_table_top");
    std::smatch match;

    std::map<const std::string, std::pair<std::vector<object_data>, int>> map;

    ROS_INFO("--- WING_READER ---");
    try{
      for(int i = 0; i < resources.size(); i++){
          std::string str = resources[i]["id"];
          if(std::regex_match(str, match, rx)){
              std::stringstream ss;
              ss << "panda_arm" << match[1].str().c_str();
              std::vector<object_data> wd;
              wd.push_back({"", tf2::Transform(tf2::Quaternion(0,0,0,1), tf2::Vector3(0,0,0)), tf2::Vector3(0,0,0)});
              wd.push_back({"", tf2::Transform(tf2::Quaternion(0,0,0,1), tf2::Vector3(0,0,0)), tf2::Vector3(0,0,0)});
              wd.push_back({"", tf2::Transform(tf2::Quaternion(0,0,0,1), tf2::Vector3(0,0,0)), tf2::Vector3(0,0,0)});
              map.insert(std::pair<const std::string, std::pair<std::vector<object_data>, int>>(ss.str(), {wd, 0}));
          }
      }
    } catch (const XmlRpc::XmlRpcException& xre) {
      ROS_INFO("Iteration error in Blueprint initialization");
      ros::shutdown();
    }


    rx.assign("table([0-9]+)_right_panel");
    try{
      for(int i = 0; i < resources.size(); i++){
        std::string str = resources[i]["id"];
        if (std::regex_match(str, match, rx)){
          std::stringstream ss;
          ss << "panda_arm" << match[1].str().c_str();

          ROS_INFO("=> WING: description found, loading...");
          tf2::Vector3 pos;
          tf2::Vector3 size;
          tf2::Quaternion rot;

          (resources[i]["size"].hasMember("length")) ? size.setX(floatOf(resources[i]["size"]["length"])) :size.setX(0);
          (resources[i]["size"].hasMember("width")) ? size.setY(floatOf(resources[i]["size"]["width"])) :size.setY(0);
          (resources[i]["size"].hasMember("height")) ? size.setZ(floatOf(resources[i]["size"]["height"])) :size.setZ(0);

          (resources[i]["pos"].hasMember("x")) ? pos.setX(floatOf(resources[i]["pos"]["x"])) :pos.setX(0);
          (resources[i]["pos"].hasMember("y")) ? pos.setY(floatOf(resources[i]["pos"]["y"])) :pos.setY(0);
          (resources[i]["pos"].hasMember("z")) ? pos.setZ(floatOf(resources[i]["pos"]["z"]) + size.getZ()/2) :pos.setZ(0);
          (resources[i]["orientation"].hasMember("x")) ? rot.setX(floatOf(resources[i]["orientation"]["x"])) :rot.setX(0);
          (resources[i]["orientation"].hasMember("y")) ? rot.setY(floatOf(resources[i]["orientation"]["y"])) :rot.setY(0);
          (resources[i]["orientation"].hasMember("z")) ? rot.setZ(floatOf(resources[i]["orientation"]["z"])) :rot.setZ(0);
          (resources[i]["orientation"].hasMember("w")) ? rot.setW(floatOf(resources[i]["orientation"]["w"])) :rot.setW(0);

          ROS_INFO("--- Wing %s ---", str.c_str());
          ROS_INFO("=> WING: pos('%f', '%f', '%f')", pos.getX(), pos.getY(), pos.getZ());
          ROS_INFO("=> WING: orientation('%f', '%f', '%f', '%f')", rot.getX(), rot.getY(), rot.getZ(), rot.getW());
          ROS_INFO("=> WING: size('%f', '%f', '%f')", size.getX(), size.getY(), size.getZ());

          try {
            map.at(ss.str()).first[0].name_ = str.c_str();
            map.at(ss.str()).first[0].pose_ = tf2::Transform(rot.normalized(), pos);
            map.at(ss.str()).first[0].size_ = size;
            map.at(ss.str()).second += std::pow(2, 0);
          } catch (const std::out_of_range& oor) {
            ROS_INFO("Found panel for unknown robot");
          }
        }
      }
    } catch (const XmlRpc::XmlRpcException& xre){
      ROS_INFO("Iteration error in right panel search");
      ros::shutdown();
    }


    rx.assign("table([0-9]+)_front_panel");
    try {
      for(int i = 0; i < resources.size(); i++){
        std::string str = resources[i]["id"];
        if (std::regex_match(str, match, rx)){
          std::stringstream ss;
          ss << "panda_arm" << match[1].str().c_str();

          ROS_INFO("=> WING: description found, loading...");
          tf2::Vector3 pos;
          tf2::Vector3 size;
          tf2::Quaternion rot;

          
          (resources[i]["size"].hasMember("length")) ? size.setX(floatOf(resources[i]["size"]["length"])) :size.setX(0);
          (resources[i]["size"].hasMember("width")) ? size.setY(floatOf(resources[i]["size"]["width"])) :size.setY(0);
          (resources[i]["size"].hasMember("height")) ? size.setZ(floatOf(resources[i]["size"]["height"])) :size.setZ(0);

          (resources[i]["pos"].hasMember("x")) ? pos.setX(floatOf(resources[i]["pos"]["x"])) :pos.setX(0);
          (resources[i]["pos"].hasMember("y")) ? pos.setY(floatOf(resources[i]["pos"]["y"])) :pos.setY(0);
          (resources[i]["pos"].hasMember("z")) ? pos.setZ(floatOf(resources[i]["pos"]["z"]) + size.getZ()/2) :pos.setZ(0);
          (resources[i]["orientation"].hasMember("x")) ? rot.setX(floatOf(resources[i]["orientation"]["x"])) :rot.setX(0);
          (resources[i]["orientation"].hasMember("y")) ? rot.setY(floatOf(resources[i]["orientation"]["y"])) :rot.setY(0);
          (resources[i]["orientation"].hasMember("z")) ? rot.setZ(floatOf(resources[i]["orientation"]["z"])) :rot.setZ(0);
          (resources[i]["orientation"].hasMember("w")) ? rot.setW(floatOf(resources[i]["orientation"]["w"])) :rot.setW(0);

          ROS_INFO("--- Wing %s ---", str.c_str());
          ROS_INFO("=> WING: pos('%f', '%f', '%f')", pos.getX(), pos.getY(), pos.getZ());
          ROS_INFO("=> WING: orientation('%f', '%f', '%f', '%f')", rot.getX(), rot.getY(), rot.getZ(), rot.getW());
          ROS_INFO("=> WING: size('%f', '%f', '%f')", size.getX(), size.getY(), size.getZ());

          try {
            map.at(ss.str()).first[1].name_ = str.c_str();
            map.at(ss.str()).first[1].pose_ = tf2::Transform(rot.normalized(), pos);
            map.at(ss.str()).first[1].size_ = size;
            map.at(ss.str()).second += std::pow(2, 1);
          } catch (const std::out_of_range& oor) {
            ROS_INFO("Found panel for unknown robot");
          }
        }
      }
    } catch(const XmlRpc::XmlRpcException& xre){
      ROS_INFO("Iteration error in front panel search");
      ros::shutdown();
    }


    rx.assign("table([0-9]+)_left_panel");
    try{
      for(int i = 0; i < resources.size(); i++){
        std::string str = resources[i]["id"];
        if (std::regex_match(str, match, rx)){
          std::stringstream ss;
          ss << "panda_arm" << match[1].str().c_str();

          ROS_INFO("=> WING: description found, loading...");
          tf2::Vector3 pos;
          tf2::Vector3 size;
          tf2::Quaternion rot;

          
          (resources[i]["size"].hasMember("length")) ? size.setX(floatOf(resources[i]["size"]["length"])) :size.setX(0);
          (resources[i]["size"].hasMember("width")) ? size.setY(floatOf(resources[i]["size"]["width"])) :size.setY(0);
          (resources[i]["size"].hasMember("height")) ? size.setZ(floatOf(resources[i]["size"]["height"])) :size.setZ(0);

          (resources[i]["pos"].hasMember("x")) ? pos.setX(floatOf(resources[i]["pos"]["x"])) :pos.setX(0);
          (resources[i]["pos"].hasMember("y")) ? pos.setY(floatOf(resources[i]["pos"]["y"])) :pos.setY(0);
          (resources[i]["pos"].hasMember("z")) ? pos.setZ(floatOf(resources[i]["pos"]["z"]) + size.getZ()/2) :pos.setZ(0);
          (resources[i]["orientation"].hasMember("x")) ? rot.setX(floatOf(resources[i]["orientation"]["x"])) :rot.setX(0);
          (resources[i]["orientation"].hasMember("y")) ? rot.setY(floatOf(resources[i]["orientation"]["y"])) :rot.setY(0);
          (resources[i]["orientation"].hasMember("z")) ? rot.setZ(floatOf(resources[i]["orientation"]["z"])) :rot.setZ(0);
          (resources[i]["orientation"].hasMember("w")) ? rot.setW(floatOf(resources[i]["orientation"]["w"])) :rot.setW(0);

          ROS_INFO("--- Wing %s ---", str.c_str());
          ROS_INFO("=> WING: pos('%f', '%f', '%f')", pos.getX(), pos.getY(), pos.getZ());
          ROS_INFO("=> WING: orientation('%f', '%f', '%f', '%f')", rot.getX(), rot.getY(), rot.getZ(), rot.getW());
          ROS_INFO("=> WING: size('%f', '%f', '%f')", size.getX(), size.getY(), size.getZ());

          try {
            map.at(ss.str()).first[2].name_ = str.c_str();
            map.at(ss.str()).first[2].pose_ = tf2::Transform(rot.normalized(), pos);
            map.at(ss.str()).first[2].size_ = size;
            map.at(ss.str()).second += std::pow(2, 2);
          } catch (const std::out_of_range& oor) {
            ROS_INFO("Found panel for unknown robot");
          }
        }
      }
    } catch(const XmlRpc::XmlRpcException& xre){
      ROS_INFO("Iteration error in left panel search");
      ros::shutdown();
    }
    setWingData(map);
}
