#include "reader/map_reader.h"

void MapReader::read(){
    XmlRpc::XmlRpcValue resources;
    nh_->getParam("/data", resources);

    std::vector<tf2::Transform> map_data;
    ROS_INFO("--- MAP_READER ---");

    
    for(int i = 0; i < resources.size(); i++){
        tf2::Vector3 pos;
        tf2::Quaternion rot;

        (resources[i]["pos"].hasMember("x")) ? pos.setX(floatOf(resources[i]["pos"]["x"])) :pos.setX(0);
        (resources[i]["pos"].hasMember("y")) ? pos.setY(floatOf(resources[i]["pos"]["y"])) :pos.setY(0);
        (resources[i]["pos"].hasMember("z")) ? pos.setZ(floatOf(resources[i]["pos"]["z"])) :pos.setZ(0);
        (resources[i]["orientation"].hasMember("x")) ? rot.setX(floatOf(resources[i]["orientation"]["x"])) :rot.setX(0);
        (resources[i]["orientation"].hasMember("y")) ? rot.setY(floatOf(resources[i]["orientation"]["y"])) :rot.setY(0);
        (resources[i]["orientation"].hasMember("z")) ? rot.setZ(floatOf(resources[i]["orientation"]["z"])) :rot.setZ(0);
        (resources[i]["orientation"].hasMember("w")) ? rot.setW(floatOf(resources[i]["orientation"]["w"])) :rot.setW(0);
        map_data.push_back(tf2::Transform(rot, pos));
    }
    
    setMapData(map_data);
}
