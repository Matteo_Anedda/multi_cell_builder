#include "reader/cuboid_reader.h"

void CuboidReader::read(){
    ROS_INFO("--- CUBOID_READER ---");
    XmlRpc::XmlRpcValue resources;
    nh_->getParam("/objects", resources);
    std::smatch match;

    for (int i = 0; i < resources.size(); i++){
        std::string id = resources[i]["id"];
        if(resources[i]["type"] == "BOX"){
            tf2::Vector3 pos;
            tf2::Vector3 size;
            tf2::Quaternion rot;

            (resources[i]["size"].hasMember("length")) ? size.setX(floatOf(resources[i]["size"]["length"])) :size.setX(0);
            (resources[i]["size"].hasMember("width")) ? size.setY(floatOf(resources[i]["size"]["width"])) :size.setY(0);
            (resources[i]["size"].hasMember("height")) ? size.setZ(floatOf(resources[i]["size"]["height"])) :size.setZ(0);

            (resources[i]["pos"].hasMember("x")) ? pos.setX(floatOf(resources[i]["pos"]["x"])) :pos.setX(0);
            (resources[i]["pos"].hasMember("y")) ? pos.setY(floatOf(resources[i]["pos"]["y"])) :pos.setY(0);
            (resources[i]["pos"].hasMember("z")) ? pos.setZ(floatOf(resources[i]["pos"]["z"])) :pos.setZ(0);
            (resources[i]["orientation"].hasMember("x")) ? rot.setX(floatOf(resources[i]["orientation"]["x"])) :rot.setX(0);
            (resources[i]["orientation"].hasMember("y")) ? rot.setY(floatOf(resources[i]["orientation"]["y"])) :rot.setY(0);
            (resources[i]["orientation"].hasMember("z")) ? rot.setZ(floatOf(resources[i]["orientation"]["z"])) :rot.setZ(0);
            (resources[i]["orientation"].hasMember("w")) ? rot.setW(floatOf(resources[i]["orientation"]["w"])) :rot.setW(0);

            Cuboid o;
            o.Name = id.c_str();
            o.Pose.position.x = pos.getX();
            o.Pose.position.y = pos.getY();
            o.Pose.position.z = pos.getZ();
            o.Pose.orientation.x = rot.getX();
            o.Pose.orientation.y = rot.getY();
            o.Pose.orientation.z = rot.getZ();
            o.Pose.orientation.w = rot.getW();
            o.x_depth = size.getX();
            o.y_width = size.getY();
            o.z_heigth = size.getZ();

            
            cuboid_box_.push_back(o);
            ROS_INFO("--- Object: %s  --- Type: BOX  ---", o.Name.c_str());
            ROS_INFO("=> Object: pos('%f', '%f', '%f')", pos.getX(), pos.getY(), pos.getZ());
            ROS_INFO("=> Object: orientation('%f', '%f', '%f', '%f')", rot.getX(), rot.getY(), rot.getZ(), rot.getW());
            ROS_INFO("=> Object: size('%f', '%f', '%f')", size.getX(), size.getY(), size.getZ());
        }

        if(resources[i]["type"] == "BIN" || std::regex_match(id, match, std::regex("obstacle([0-9]+)"))){
            tf2::Vector3 pos;
            tf2::Vector3 size;
            tf2::Quaternion rot;

            (resources[i]["size"].hasMember("length")) ? size.setX(floatOf(resources[i]["size"]["length"])) :size.setX(0);
            (resources[i]["size"].hasMember("width")) ? size.setY(floatOf(resources[i]["size"]["width"])) :size.setY(0);
            (resources[i]["size"].hasMember("height")) ? size.setZ(floatOf(resources[i]["size"]["height"])) :size.setZ(0);

            (resources[i]["pos"].hasMember("x")) ? pos.setX(floatOf(resources[i]["pos"]["x"])) :pos.setX(0);
            (resources[i]["pos"].hasMember("y")) ? pos.setY(floatOf(resources[i]["pos"]["y"])) :pos.setY(0);
            (resources[i]["pos"].hasMember("z")) ? pos.setZ(floatOf(resources[i]["pos"]["z"])) :pos.setZ(0);
            (resources[i]["orientation"].hasMember("x")) ? rot.setX(floatOf(resources[i]["orientation"]["x"])) :rot.setX(0);
            (resources[i]["orientation"].hasMember("y")) ? rot.setY(floatOf(resources[i]["orientation"]["y"])) :rot.setY(0);
            (resources[i]["orientation"].hasMember("z")) ? rot.setZ(floatOf(resources[i]["orientation"]["z"])) :rot.setZ(0);
            (resources[i]["orientation"].hasMember("w")) ? rot.setW(floatOf(resources[i]["orientation"]["w"])) :rot.setW(0);

            Cuboid o;
            o.Name = id.c_str();
            o.Pose.position.x = pos.getX();
            o.Pose.position.y = pos.getY();
            o.Pose.position.z = pos.getZ();
            o.Pose.orientation.x = rot.getX();
            o.Pose.orientation.y = rot.getY();
            o.Pose.orientation.z = rot.getZ();
            o.Pose.orientation.w = rot.getW();
            o.x_depth = size.getX();
            o.y_width = size.getY();
            o.z_heigth = size.getZ();

            cuboid_obstacle_.push_back(o);
            ROS_INFO("--- Object: %s  --- Type: BIN  ---", o.Name.c_str());
            ROS_INFO("=> Object: pos('%f', '%f', '%f')", pos.getX(), pos.getY(), pos.getZ());
            ROS_INFO("=> Object: orientation('%f', '%f', '%f', '%f')", rot.getX(), rot.getY(), rot.getZ(), rot.getW());
            ROS_INFO("=> Object: size('%f', '%f', '%f')", size.getX(), size.getY(), size.getZ());

        }
    }
}