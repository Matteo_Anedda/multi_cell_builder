#include "reader/robot_reader.h"

void RobotReader::read(){
    ROS_INFO("--- ROBOT_READER ---");
    XmlRpc::XmlRpcValue resources;
    nh_->getParam("/objects", resources);
    std::regex rx("table([0-9]+)_table_top");
    std::smatch match;

    for (int i = 0; i < resources.size(); i++){
        std::string str = resources[i]["id"];
        if(std::regex_match(str, match, rx)){
            ROS_INFO("=> Robot: description found, loading...");
            std::stringstream ss;
            ss << "panda_arm" << match[1];
            tf2::Vector3 pos;
            tf2::Vector3 size;
            tf2::Quaternion rot;

            (resources[i]["size"].hasMember("length")) ? size.setX(floatOf(resources[i]["size"]["length"])) :size.setX(0);
            (resources[i]["size"].hasMember("width")) ? size.setY(floatOf(resources[i]["size"]["width"])) :size.setY(0);
            (resources[i]["size"].hasMember("height")) ? size.setZ(floatOf(resources[i]["size"]["height"])) :size.setZ(0);

            (resources[i]["pos"].hasMember("x")) ? pos.setX(floatOf(resources[i]["pos"]["x"])) :pos.setX(0);
            (resources[i]["pos"].hasMember("y")) ? pos.setY(floatOf(resources[i]["pos"]["y"])) :pos.setY(0);
            (resources[i]["pos"].hasMember("z")) ? pos.setZ((floatOf(resources[i]["pos"]["z"]) + size.getZ()/2) /2) :pos.setZ(0);
            (resources[i]["orientation"].hasMember("x")) ? rot.setX(floatOf(resources[i]["orientation"]["x"])) :rot.setX(0);
            (resources[i]["orientation"].hasMember("y")) ? rot.setY(floatOf(resources[i]["orientation"]["y"])) :rot.setY(0);
            (resources[i]["orientation"].hasMember("z")) ? rot.setZ(floatOf(resources[i]["orientation"]["z"])) :rot.setZ(0);
            (resources[i]["orientation"].hasMember("w")) ? rot.setW(floatOf(resources[i]["orientation"]["w"])) :rot.setW(0);

            ROS_INFO("--- Robot: %s ---", ss.str().c_str());
            ROS_INFO("=> Robot: pos('%f', '%f', '%f')", pos.getX(), pos.getY(), pos.getZ());
            ROS_INFO("=> Robot: orientation('%f', '%f', '%f', '%f')", rot.getX(), rot.getY(), rot.getZ(), rot.getW());
            ROS_INFO("=> Robot: size('%f', '%f', '%f')", size.getX(), size.getY(), size.getZ());

            robot_data_.push_back({ss.str().c_str(), tf2::Transform(rot,pos), size});
        }
    }
}