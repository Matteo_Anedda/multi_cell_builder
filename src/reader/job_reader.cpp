#include "reader/job_reader.h"
#include <queue>
#include "boost/circular_buffer.hpp"

void JobReader::read(){
    ROS_INFO("--- JOB_READER ---");
    XmlRpc::XmlRpcValue resources;
    nh_->getParam("/tasks/groups", resources);

    int count =0;
    for (int i = 0; i < resources.size(); i++){
        count+= resources[i]["jobs"].size();
    }

    boost::circular_buffer<std::pair<std::string, job_data>> cb_jd(count);

    for (int i = 0; i < resources.size(); i++){
        std::string robot = resources[i]["name"];
        //nh_->getParam("/tasks/groups/" + robot, jobs);
        for (int j = 0; j < resources[i]["jobs"].size(); j++){
            ROS_INFO("--- %s --- JOB_%i ---", robot.c_str(), j);
            job_data jd;
            for(int k = 0; k < resources[i]["jobs"][j].size(); k++){
                XmlRpc::XmlRpcValue job_data = resources[i]["jobs"][j][k];
                tf2::Vector3 pos;
                tf2::Quaternion rot;


                (job_data["pos"].hasMember("x")) ? pos.setX(floatOf(job_data["pos"]["x"])) :pos.setX(0);
                (job_data["pos"].hasMember("y")) ? pos.setY(floatOf(job_data["pos"]["y"])) :pos.setY(0);
                (job_data["pos"].hasMember("z")) ? pos.setZ(floatOf(job_data["pos"]["z"])) :pos.setZ(0);
                (job_data["orientation"].hasMember("x")) ? rot.setX(floatOf(job_data["orientation"]["x"])) :rot.setX(0);
                (job_data["orientation"].hasMember("y")) ? rot.setY(floatOf(job_data["orientation"]["y"])) :rot.setY(0);
                (job_data["orientation"].hasMember("z")) ? rot.setZ(floatOf(job_data["orientation"]["z"])) :rot.setZ(0);
                (job_data["orientation"].hasMember("w")) ? rot.setW(floatOf(job_data["orientation"]["w"])) :rot.setW(0);


                ROS_INFO("=> Grab: pos('%f', '%f', '%f')", pos.getX(), pos.getY(), pos.getZ());
                ROS_INFO("=> Grab: orientation('%f', '%f', '%f', '%f')", rot.getX(), rot.getY(), rot.getZ(), rot.getW());
                jd.jobs_.push_back(tf2::Transform(rot, pos));
            }
            cb_jd.push_back(std::pair<std::string, job_data>(robot, jd));
        }  
    }

    setJobData(cb_jd);


    // validate 
    for(auto& s_jd : job_data_){
        for(auto& pose : s_jd.second.jobs_) {
            ROS_INFO("%s %f %f %f %f %f %f %f", s_jd.first.c_str(), pose.getOrigin().getX(), pose.getOrigin().getY(), pose.getOrigin().getZ(), pose.getRotation().getX(), pose.getRotation().getY(), pose.getRotation().getZ(), pose.getRotation().getW());
        }
    }
}