#include "reader/ts_reader.h"

void TSReader::read(){
    XmlRpc::XmlRpcValue resources;
    nh_->getParam("/objects", resources);

    std::vector<object_data> robot_middle;
    std::vector<object_data>* robot_pointer = nullptr;


    std::regex rx("table([0-9]+)_table_top");
    std::smatch match;

    ROS_INFO("--- TASK_SPACE_READER ---");
    std::map<const std::string, std::vector<object_data>> map;

    for(int i = 0; i < resources.size(); i++){
      std::string str = resources[i]["id"];
      if (std::regex_match(str, match, rx)){
        std::stringstream ss;
        ss << "panda_arm" << match[1].str().c_str();
        map.insert(std::make_pair<const std::string, std::vector<object_data>>(ss.str().c_str(), std::vector<object_data>()));
      }
    }

    for (int i = 0; i < resources.size(); i++){
      if (resources[i]["type"] == "DROP_OFF_LOCATION"){
        std::string str = resources[i]["id"];
        if(str[0] == 'A')  robot_pointer = &map.at("panda_arm1");
        if(str[0] == 'B')  robot_pointer = &map.at("panda_arm1");
        if(str[0] == 'C')  robot_pointer = &map.at("panda_arm1");
        if(str[0] == 'D')  robot_pointer = &map.at("panda_arm1");
        if(str[0] == 'E')  robot_pointer = &robot_middle;
        if(str[0] == 'F')  robot_pointer = &robot_middle;
        if(str[0] == 'G')  robot_pointer = &map.at("panda_arm2");
        if(str[0] == 'H')  robot_pointer = &map.at("panda_arm2");
        if(str[0] == 'I')  robot_pointer = &map.at("panda_arm2");
        if(str[0] == 'J')  robot_pointer = &map.at("panda_arm2");
      
        if (robot_pointer){
          tf2::Vector3 pos(0,0,0);
          tf2::Quaternion rot(0,0,0,1);
          tf2::Vector3 size(0,0,0);

          (resources[i]["size"].hasMember("length")) ? size.setX(floatOf(resources[i]["size"]["length"])) :size.setX(0);
          (resources[i]["size"].hasMember("width")) ? size.setY(floatOf(resources[i]["size"]["width"])) :size.setY(0);
          (resources[i]["size"].hasMember("height")) ? size.setZ(floatOf(resources[i]["size"]["height"])) :size.setZ(0);
           
          (resources[i]["pos"].hasMember("x")) ? pos.setX(floatOf(resources[i]["pos"]["x"])) :pos.setX(0);
          (resources[i]["pos"].hasMember("y")) ? pos.setY(floatOf(resources[i]["pos"]["y"])) :pos.setY(0);
          (resources[i]["pos"].hasMember("z")) ? pos.setZ(floatOf(resources[i]["pos"]["z"])) :pos.setZ(0);

          (resources[i]["orientation"].hasMember("x")) ? rot.setX(floatOf(resources[i]["orientation"]["x"])) :rot.setX(0);
          (resources[i]["orientation"].hasMember("y")) ? rot.setY(floatOf(resources[i]["orientation"]["y"])) :rot.setY(0);
          (resources[i]["orientation"].hasMember("z")) ? rot.setZ(floatOf(resources[i]["orientation"]["z"])) :rot.setZ(0);
          (resources[i]["orientation"].hasMember("w")) ? rot.setW(floatOf(resources[i]["orientation"]["w"])) :rot.setW(0);
          
          robot_pointer->push_back({"", tf2::Transform(rot, pos), size});
        }
      }
      robot_pointer = nullptr;
    }

    for(auto& robot: map){
      for(object_data& tf : robot_middle){
        robot.second.push_back(tf);
      }
    }

    setDropOffData(map);
}