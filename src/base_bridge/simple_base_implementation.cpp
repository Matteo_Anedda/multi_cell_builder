#include "bridge/simple_base_implementation.h"

void SimpleBaseImplementation::setGraspOrientations(std::map<const std::string, std::vector<std::pair<object_data, std::vector<tf2::Quaternion>>>>& var){
    tf2::Quaternion x_rot(0,0,0,1);
    tf2::Quaternion y_rot(0,0,0.707,0.707);
    tf2::Quaternion z_rot(0,-0.707,0,0.707);
    std::vector<tf2::Quaternion> basic_rot{x_rot, x_rot.inverse(), y_rot, y_rot.inverse(), z_rot.inverse()};
    ROS_INFO("=> 5 Basic grasp roatations (x, -x, y, -y, -z) ");
    for(auto&s_od_q: var){
        for(auto&od_q: s_od_q.second){
            od_q.second = basic_rot;
        }
    }
}

void SimpleBaseImplementation::invMapCreation(std::vector<tf2::Transform>& map, std::vector<tf2::Transform>& inv_map, std::map<const std::string, std::vector<std::pair<object_data,std::vector<tf2::Quaternion>>>>& task_space){
    std::vector<tf2::Transform> trans;
    for(long unsigned int i = 0; i < map.size(); i++) {
        for(auto&s_od_q: task_space){
            for(auto&od_q: s_od_q.second){
                for(auto&q: od_q.second){
                    if (q.angle(map[i].getRotation()) < 0.349066f){
                        inv_map.push_back(tf2::Transform(q, map[i].getOrigin()).inverse());
                        break;
                    }
                }
            }
        }
    }

    ROS_INFO("caculated [inv_map] contains %li entrys...", inv_map.size());
}


void SimpleBaseImplementation::cloudCalculation(std::vector<tf2::Transform>& inv_map, std::map<const std::string, std::vector<std::pair<object_data,std::vector<tf2::Quaternion>>>>& task_space, std::map<const std::string, std::vector<pcl::PointCloud< pcl::PointXYZ >::Ptr>>& target_cloud){
    ROS_INFO("initialyze thread implementations...");
    
    for(auto& s_od_q: task_space) {
        std::vector<pcl::PointCloud< pcl::PointXYZ >::Ptr> clouds;
        for(auto& od_q: s_od_q.second) {
            clouds.push_back(pcl::PointCloud< pcl::PointXYZ >::Ptr(new pcl::PointCloud< pcl::PointXYZ >));
        }
        target_cloud.insert(std::pair<const std::string, std::vector<pcl::PointCloud< pcl::PointXYZ >::Ptr>>(s_od_q.first, clouds));
    }

    ROS_INFO("start [cloud] calculation...");
    // ------------------------------------------------------------
    tf2::Transform root(tf2::Quaternion(0,0,0,1), tf2::Vector3(-0.22f,0,0.445f));
    // ------------------------------------------------------------
    for(auto& s_c: target_cloud){
        for(int i = 0; i < s_c.second.size();i++){
            for(auto& itf : inv_map){
                for(auto& r: task_space.at(s_c.first)[i].second){
                    tf2::Transform target(r, task_space.at(s_c.first)[i].first.pose_.getOrigin());
                    tf2::Transform base = target * itf * root;
                    s_c.second[i]->push_back(pcl::PointXYZ(base.getOrigin().getX(), base.getOrigin().getY(), base.getOrigin().getZ()));
                }
            }
        }
    }
    
    ROS_INFO("[cloud]calculation done...");
    
}
