#include "bridge/simple_base.h"
#include "bridge/abstract_base_implementation.h"

SimpleBase::SimpleBase(std::shared_ptr<ros::NodeHandle> const& d)
    : AbstractBase(d)
    , task_space_reader_(std::make_unique<TSReader>(d))
    , map_reader_(std::make_unique<MapReader>(d))
    {
        map_= map_reader_->mapData();
        ROS_INFO("=> RM is known");

        for (auto& s_vod:task_space_reader_->dropOffData()){
            std::vector<std::pair<object_data, std::vector<tf2::Quaternion>>> vod_q;
            for(auto& od: s_vod.second) vod_q.push_back(std::pair<object_data, std::vector<tf2::Quaternion>>(od, std::vector<tf2::Quaternion>()));
            task_space_.insert(std::pair< const std::string, std::vector<std::pair<object_data, std::vector<tf2::Quaternion>>>>(s_vod.first,vod_q));
        }
        ROS_INFO("=> TS is known");
    }

void SimpleBase::baseCalculation(){

    implementation_->setGraspOrientations(task_space_);
    ROS_INFO("=> grasp rotations from implenentation object set...");


    implementation_->invMapCreation(map_, inv_map_, task_space_);
    ROS_INFO("=> inverse map is set...");

    implementation_->cloudCalculation(inv_map_, task_space_, target_cloud_);
    ROS_INFO("=> clouds are formed...");
    

    ROS_INFO("init voxel...");
    std::vector<pcl::PointXYZ> voxelization = this->createPCLBox();

    std::map<const std::string, std::vector<std::vector<int>>> base_target_map;
    for (auto& ts: task_space_){
        base_target_map.insert(std::pair<const std::string, std::vector<std::vector<int>>>(ts.first, std::vector<std::vector<int>>(voxelization.size())));
    }
    
    ROS_INFO("start cloud quantization...");
    for (auto& s_vc: target_cloud_){
        for(long unsigned int j = 0; j < target_cloud_.at(s_vc.first).size();j++){
            pcl::octree::OctreePointCloudSearch<pcl::PointXYZ> octree(0.2f);
            octree.setInputCloud(target_cloud_.at(s_vc.first)[j]);
            octree.addPointsFromInputCloud();
            double min_x, min_y, min_z, max_x, max_y, max_z;
            octree.getBoundingBox(min_x, min_y, min_z, max_x, max_y, max_z);
            for(long unsigned int k = 0; k < voxelization.size(); k++) {
                pcl::PointXYZ p = voxelization[k];
                bool isInBox = (p.x >= min_x && p.x <= max_x) && (p.y >= min_y && p.y <= max_y) && (p.z >= min_z && p.z <= max_z);
                if(isInBox && octree.isVoxelOccupiedAtPoint(p)) {
                    std::vector< int > pointIdxVec;
                    if(octree.voxelSearch(p, pointIdxVec)) if(!pointIdxVec.empty()) base_target_map.at(s_vc.first)[k].push_back(j);
                }
            }
        }
    }

    for(auto& s_bv: base_target_map){
        std::vector<pcl::PointXYZ> points_per_robot;
        for(int j = 0; j < base_target_map.at(s_bv.first).size(); j++){
            if (base_target_map.at(s_bv.first)[j].size() == task_space_.at(s_bv.first).size()) {
                points_per_robot.push_back(voxelization[j]);
            }
        }
        if (!points_per_robot.empty()) result_.insert(std::pair<const std::string, std::vector<pcl::PointXYZ>>(s_bv.first, points_per_robot));
    }

    

    for (auto& s_r: result_) {
        ROS_INFO("%s got %li base positions to ckeck", s_r.first.c_str(), result_.at(s_r.first).size());
    }
}


std::vector<pcl::PointXYZ> AbstractBase::createPCLBox(){
    tf2::Vector3 origin(0,0,0);
    float resolution = 0.4f;
    float diameter = 3.0f;
    unsigned char depth = 16;
    std::vector<pcl::PointXYZ> box; 
    octomap::OcTree* tree = new octomap::OcTree(resolution/2);
    for (float x = origin.getX() - diameter * 5 ; x <= origin.getX() + diameter * 5 ; x += resolution){
        for (float y = origin.getY() - diameter * 5 ; y <= origin.getY() + diameter * 5 ; y += resolution){
            for (float z = origin.getZ() - diameter * 1.5 ; z <= origin.getZ() + diameter * 1.5 ; z += resolution){
                octomap::point3d point(x,y,z);
                tree->updateNode(point, true);
            }
        }
    }

    for (octomap::OcTree::leaf_iterator it = tree->begin_leafs(depth), end = tree->end_leafs(); it != end; ++it){
        pcl::PointXYZ searchPoint(it.getCoordinate().x(), it.getCoordinate().y(), it.getCoordinate().z());
        box.push_back(searchPoint);
    }

    return box;
}
